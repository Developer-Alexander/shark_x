# shark_x

shark_x is a scripting extension for Wireshark (https://gitlab.com/wireshark/wireshark) for several scripting languages like Python or Javascript. It integrates into Wireshark as a plugin to make it usable with existing Wireshark installations. Primary design goal is to speed up and simplify custom analysis of network traces with scripts.

Supported scripting backends:

- Python via CPython -> will be called `python`

- Javascript via Nodejs -> will be called `nodejs`

- Lua via luaJIT -> will be called `luajit`

shark_x uses its own local instances of the scripting backends to increase portability and avoid potential conflicts with existing global installations.

Supported platforms:

- Windows

- Linux (Ubuntu)

Supported versions of Wireshark:

- Wireshark 3.6.x

# Features

shark_x API is available during runtime via a global object `shark_x`. (e.g. `shark_x.report_failure("Hello World!")`)

- Report messages to the user (`report_failure()`)

- Report warnings to the user (`report_warning()`)

- Callbacks for `late init` event (right after all registrations like dissectors have been done)

- Callbacks for `dissection start` event

- Callbacks for `shutdown` event (for cleanup and freeing resources)

- Listeners with callbacks for each `packet` and `finish` and `cleanup` event (for analysis scripts)

- List all fields (`get_field_descriptions()`)

For a detailed API description see `API.md`

## Roadmap

v0.3:

- Postdissectors

### Ideas

The following features might be supported in future releases:

- Dissectors

- heuristic Dissectors

- TCP reassembly

- Support for C# via .NET or Mono

Please tell me about your ideas or needs.

# Build

shark_x integrates itself into Wireshark CMake project to be built as a Wireshark plugin.

## Windows

### Requirements

1. git
2. CMake
3. Visual Studio 2019 (As of Feb. 2022 Wireshark requires Visual Studio 2019)
4. Python 3

For `Wireshark.exe` you need additionally (otherwise you will get only command line version `tshark.exe`):

5. Qt 5 (most recent LTS release is recommended)

### Steps

1. If you want to build with Qt for `Wireshark.exe` set environment variable `QT5_BASE_DIR` to the path of your Qt installation

   - e.g. `QT5_BASE_DIR=D:\Dev\wireshark_dependencies\qt_5.15\msvc2019_64`

2. Run `build.py` in the root directory

   - The CMake root for Wireshark will be `<OUTPUT_PREFIX>/libs/windows/wireshark/`.

   - The Visual Studio Solution for Wireshark can be found in `<OUTPUT_PREFIX>/libs/windows/wireshark/build/Wireshark.sln`. (Except you use a custom CMake output path)

3. Resulting binaries can be found in `<OUTPUT_PREFIX>/bin/windows/<RELEASE_NAME>/`

   - Another copy of the binaries will be placed in `<OUTPUT_PREFIX>/libs/windows/wireshark/build/run/<RELEASE_NAME>/`

### Notes

Supported builds:

- Debug

- Release

## Linux

### Requirements

Required packages:

`sudo add-apt-repository universe`

`sudo apt-get update`

`sudo apt-get install build-essential git cmake python3 gdb`

Wireshark dependencies:

`sudo apt-get install flex bison perl libglib2.0-dev libc-ares-dev libpcap-dev qttools5-dev qttools5-dev-tools libqt5svg5-dev qtmultimedia5-dev libgcrypt20-dev liblua5.2-dev libnghttp2-dev libpcre2-dev`

Note: Please check out documentation of Wireshark for information about optional packages.

Some optional Python dependencies:

`sudo apt install libssl-dev zlib1g-dev libncurses5-dev libncursesw5-dev libreadline-dev libsqlite3-dev libgdbm-dev libdb5.3-dev libbz2-dev libexpat1-dev liblzma-dev libffi-dev uuid-dev`

### Steps

1. Run `build.py` in the root directory

   - The CMake root for Wireshark will be `<OUTPUT_PREFIX>/libs/linux/wireshark/`.

   - The Makefile project for Wireshark can be found in `<OUTPUT_PREFIX>/libs/linux/wireshark/build/`. (Except you use a custom CMake output path)

2. Resulting binaries can be found in `<OUTPUT_PREFIX>/bin/linux/`

   - Another copy of the binaries will be placed in `<OUTPUT_PREFIX>/libs/linux/wireshark/build/run/`

# License

Source code of shark_x is licensed under MIT license.

SPDX identifier: MIT

**BUT BE AWARE:** Since shark_x is a plugin for Wireshark which is licensed under GPLv2(+) the the output of your build will be very likely licensed under GPLv2(+) aswell due to the copyleft effect of GPLv2(+) license.

The distribution obligations of GPLv2(+) license are the reason why there will be no no prebuilt binaries for now.

# Notes

## python

- You can use `tools/shark_x.pyi` for code completion in your favorite IDE.

- For simple debugging your scripts you can use `web-pdb` (https://github.com/romanvm/python-web-pdb).

- If you depend on `numpy`: `numpy` seems not to work with Debug build on windows. You should switch to Release build.

- If you want to install a package with pip you can use `pip` as fallback on an import error in your scripts. Example for `matplotlib`:

  ```
  try:
     import matplotlib.pyplot as plt
  except Exception as ex:
     print(ex)
     import pip, os, sys

     lib_path: str = f"{shark_x.EXECUTABLE_DIRECTORY}{os.sep}lib{os.sep}python{sys.version_info.major}.{sys.version_info.minor}{os.sep}site-packages"
     pip.main(
        [
           "install",
           "-t",
           lib_path,
           "matplotlib",
        ]
     )

     import matplotlib.pyplot as plt
  ```

  Note: Use option `-t` to specify an installation target. Without an explicit target the target will chosen by `pip`.

## luajit

API of `luajit` is not compatible to lua API built into Wireshark. To ensure that your lua script is executed by `luajit` and not by Wiresharks built in lua engine use a code barrier like this:

```
-- IS_SHARK_X_ENVIRONMENT is a boolean global that is defined by shark_x

if IS_SHARK_X_ENVIRONMENT == true then
	-- put your code here
end
```
