/*
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR
A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SPDX-License-Identifier: MIT
*/

#ifndef __SHARK_X_PYTHON_EVENT_HANDLER__
#define __SHARK_X_PYTHON_EVENT_HANDLER__

#ifdef SHARK_X_PYTHON_ENABLED

// shark_x includes
#include "shark_x.h"
#include "shark_x_event.hpp"
#include "shark_x_event_handler.hpp"
#include "shark_x_python_types.hpp"

// wireshark includes
#include "epan/tap.h"

// python includes
#define PY_SSIZE_T_CLEAN
#include "Python.h"

// std includes
#include <set>

namespace shark_x {
class PythonEventHandler {
  protected:
    // api implementation

    static PyObject* _get_current_phase(PyObject* self, PyObject* args);
    static PyObject* _report_failure(PyObject* self, PyObject* args);
    static PyObject* _report_warning(PyObject* self, PyObject* args);
    static PyObject* _register_late_init_callback(PyObject* self, PyObject* args);
    static PyObject* _deregister_late_init_callback(PyObject* self, PyObject* args);
    static PyObject* _register_shutdown_callback(PyObject* self, PyObject* args);
    static PyObject* _deregister_shutdown_callback(PyObject* self, PyObject* args);
    static PyObject* _register_dissection_start_callback(PyObject* self, PyObject* args);
    static PyObject* _deregister_dissection_start_callback(PyObject* self, PyObject* args);
    static PyObject* _register_packet_listener(PyObject* self, PyObject* args);
    static PyObject* _deregister_packet_listener(PyObject* self, PyObject* args);
    static PyObject* _get_field_descriptions(PyObject* self, PyObject* args);

    static PyObject* _create_packet_data(packet_info* packet_info, proto_tree* tree, tvbuff_t* packet_buffer);
    static void _create_packet_data_process_node(proto_node* node, gpointer data);
    static PyObject* _create_field_instance(header_field_info* current_header_field_info, field_info* current_field_info);
    static void _add_buffer_to_packet_data(tvbuff_t* buffer, PyObject* packet_data_py);

    // shark_x member

    static bool is_initialized;
    static std::set<PythonLateInitRegistrationInfo*> late_init_registration_infos;
    static std::set<PythonShutdownRegistrationInfo*> shutdown_registration_infos;
    static std::set<PythonDissectionStartRegistrationInfo*> dissection_start_registration_infos;
    static std::set<PacketListenerRegistrationInfo*> packet_listener_registration_infos;

    static PyRef main_module_py;
    static PyRef globals_py;
    static PyRef shark_x_module_py;
    static PyRef shark_x_base_type_py;

    // init functions

    static void Init();
    static PyObject* InitModule();
    static void ExecuteFiles();

    // protected functions

    static void OnLateInit();
    static void OnShutdown();
    static void OnDissectionStart();
    static tap_packet_status OnPacketListenerPacket(void* user_data, packet_info* packet_info, epan_dissect_t* epan_dissect, const void* _data);
    static void OnPacketListenerFinish(void* user_data);
    static void OnPacketListenerCleanup(void* user_data);

  public:
    // public functions

    static void Register();
    static void RegisterHandoff();
};
} // namespace shark_x

#endif // SHARK_X_PYTHON_ENABLED

#endif // __SHARK_X_PYTHON_EVENT_HANDLER__