/*
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR
A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SPDX-License-Identifier: MIT
*/

#ifndef __SHARK_X_PYTHON_TYPES__
#define __SHARK_X_PYTHON_TYPES__

#ifdef SHARK_X_PYTHON_ENABLED

// shark_x includes
#include "shark_x_python_util.hpp"

namespace shark_x {
class PythonLateInitRegistrationInfo {
  public:
    PyRef late_init_callback_py;
    PyRef user_data_py;
};

class PythonShutdownRegistrationInfo {
  public:
    PyRef shutdown_callback_py;
    PyRef user_data_py;
};

class PythonDissectionStartRegistrationInfo {
  public:
    PyRef dissection_start_callback_py;
    PyRef user_data_py;
};

class PythonPacketListenerRegistrationInfo {
  public:
    PyRef packet_listener_packet_callback_py;
    PyRef packet_listener_finish_callback_py;
    PyRef packet_listener_cleanup_callback_py;
    PyRef user_data_py;
};
} // namespace shark_x

#endif // SHARK_X_PYTHON_ENABLED

#endif // __SHARK_X_PYTHON_TYPES__