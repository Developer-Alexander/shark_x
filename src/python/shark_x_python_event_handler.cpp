/*
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

SPDX-License-Identifier: MIT
*/

#ifdef SHARK_X_PYTHON_ENABLED

// shark_x includes
#include "shark_x_python_event_handler.hpp"
#include "shark_x_python_types.hpp"
#include "shark_x_python_util.hpp"
#include "shark_x_util.hpp"

// wireshark includes
#include "epan/epan.h"
#include "epan/epan_dissect.h"
#include "epan/packet.h"
#include "epan/proto.h"
#include "wsutil/file_util.h"
#include "wsutil/filesystem.h"
#include "wsutil/report_message.h"
#include "wsutil/wslog.h"

// python includes
#define PY_SSIZE_T_CLEAN
#include "Python.h"
#include "frameobject.h"

// std includes
#include <charconv>
#include <string>
#include <vector>

using namespace std;

// api implementation
namespace shark_x {
PyObject* PythonEventHandler::_get_current_phase(PyObject* self, PyObject* args) {
    PyRef current_phase_py(PyLong_FromLongLong((int)EventHandler::current_phase), true);
    if (current_phase_py.is_null()) {
        python_handle_exception();
        return NULL;
    }
    return ++current_phase_py;
}

PyObject* PythonEventHandler::_report_failure(PyObject* self, PyObject* args) {
    const char* message = NULL;

    if (!PyArg_ParseTuple(args, "s", &message)) {
        return NULL;
    }

    report_failure("%s", message);

    Py_RETURN_NONE;
}

PyObject* PythonEventHandler::_report_warning(PyObject* self, PyObject* args) {
    const char* message = NULL;

    if (!PyArg_ParseTuple(args, "s", &message)) {
        return NULL;
    }

    report_warning("%s", message);

    Py_RETURN_NONE;
}

PyObject* PythonEventHandler::_register_late_init_callback(PyObject* self, PyObject* args) {

    PythonLateInitRegistrationInfo* late_init_registration_info = new PythonLateInitRegistrationInfo();

    PyRef late_init_callback_py;
    PyRef user_data_py;

    if (!PyArg_ParseTuple(args, "O|O", &late_init_callback_py.ref(), &user_data_py.ref())) {
        delete late_init_registration_info;
        return NULL;
    }

    ++late_init_callback_py;
    ++user_data_py;

    if (EventHandler::current_phase == Phase::SHUTDOWN) {
        PyErr_SetString(PyExc_Exception, "register_late_init_callback is not allowed during shutdown phase");
        delete late_init_registration_info;
        return NULL;
    }

    if (!PyCallable_Check(late_init_callback_py)) {
        PyErr_SetString(PyExc_Exception, "late_init_callback is not a function");
        delete late_init_registration_info;
        return NULL;
    }

    late_init_registration_info->late_init_callback_py = late_init_callback_py;
    late_init_registration_info->user_data_py = !user_data_py.is_null() ? user_data_py : PyRef(Py_None);

    late_init_registration_infos.insert(late_init_registration_info);

    string late_init_callback_id = to_string((uint64_t)late_init_registration_info);
    PyRef late_init_callback_id_py(PyUnicode_FromString(late_init_callback_id.c_str()), true);

    return ++late_init_callback_id_py;
}

PyObject* PythonEventHandler::_deregister_late_init_callback(PyObject* self, PyObject* args) {

    PyRef deregister_result_py = Py_False;
    const char* late_init_callback_id = NULL;

    if (!PyArg_ParseTuple(args, "s", &late_init_callback_id)) {
        return NULL;
    }

    if (EventHandler::current_phase == Phase::SHUTDOWN) {
        PyErr_SetString(PyExc_Exception, "deregister_late_init_callback is not allowed during shutdown phase");
        return NULL;
    }

    uint64_t late_init_callback_id_uint = 0;
    from_chars_result conversion_result = from_chars(late_init_callback_id, late_init_callback_id + strlen(late_init_callback_id), late_init_callback_id_uint);
    if (conversion_result.ec != errc()) {
        PyErr_SetString(PyExc_Exception, "late_init_callback_id is not valid");
        return NULL;
    }
    PythonLateInitRegistrationInfo* late_init_registration_info = (PythonLateInitRegistrationInfo*)late_init_callback_id_uint;
    set<PythonLateInitRegistrationInfo*>::iterator late_init_registration_info_found = late_init_registration_infos.find(late_init_registration_info);

    if (late_init_registration_info_found != late_init_registration_infos.end()) {
        late_init_registration_infos.erase(late_init_registration_info);
        delete late_init_registration_info;
        deregister_result_py = Py_True;
    }

    return ++deregister_result_py;
}

PyObject* PythonEventHandler::_register_shutdown_callback(PyObject* self, PyObject* args) {
    PythonShutdownRegistrationInfo* shutdown_registration_info = new PythonShutdownRegistrationInfo();

    PyRef shutdown_callback_py;
    PyRef user_data_py;

    if (!PyArg_ParseTuple(args, "O|O", &shutdown_callback_py.ref(), &user_data_py.ref())) {
        delete shutdown_registration_info;
        return NULL;
    }

    ++shutdown_callback_py;
    ++user_data_py;

    if (EventHandler::current_phase == Phase::SHUTDOWN) {
        PyErr_SetString(PyExc_Exception, "register_shutdown_callback is not allowed during shutdown phase");
        delete shutdown_registration_info;
        return NULL;
    }

    if (!PyCallable_Check(shutdown_callback_py)) {
        PyErr_SetString(PyExc_Exception, "shutdown_callback is not a function");
        delete shutdown_registration_info;
        return NULL;
    }

    shutdown_registration_info->shutdown_callback_py = shutdown_callback_py;
    shutdown_registration_info->user_data_py = !user_data_py.is_null() ? user_data_py : PyRef(Py_None);

    shutdown_registration_infos.insert(shutdown_registration_info);

    string shutdown_callback_id = to_string((uint64_t)shutdown_registration_info);
    PyRef shutdown_callback_id_py(PyUnicode_FromString(shutdown_callback_id.c_str()), true);

    return ++shutdown_callback_id_py;
}

PyObject* PythonEventHandler::_deregister_shutdown_callback(PyObject* self, PyObject* args) {
    PyRef deregister_result_py = Py_False;

    const char* shutdown_callback_id = NULL;

    if (!PyArg_ParseTuple(args, "s", &shutdown_callback_id)) {
        return NULL;
    }

    if (EventHandler::current_phase == Phase::SHUTDOWN) {
        PyErr_SetString(PyExc_Exception, "deregister_shutdown_callback is not allowed during shutdown phase");
        return NULL;
    }

    uint64_t shutdown_callback_id_uint = 0;
    from_chars_result conversion_result = from_chars(shutdown_callback_id, shutdown_callback_id + strlen(shutdown_callback_id), shutdown_callback_id_uint);
    if (conversion_result.ec != errc()) {
        PyErr_SetString(PyExc_Exception, "shutdown_callback_id is not valid");
        return NULL;
    }
    PythonShutdownRegistrationInfo* shutdown_registration_info = (PythonShutdownRegistrationInfo*)shutdown_callback_id_uint;

    set<PythonShutdownRegistrationInfo*>::iterator shutdown_registration_info_found = shutdown_registration_infos.find(shutdown_registration_info);

    if (shutdown_registration_info_found != shutdown_registration_infos.end()) {
        shutdown_registration_infos.erase(shutdown_registration_info);
        delete shutdown_registration_info;
        deregister_result_py = Py_True;
    }

    return ++deregister_result_py;
}

PyObject* PythonEventHandler::_register_dissection_start_callback(PyObject* self, PyObject* args) {
    PythonDissectionStartRegistrationInfo* dissection_start_registration_info = new PythonDissectionStartRegistrationInfo();

    PyRef dissection_start_callback_py;
    PyRef user_data_py;

    if (!PyArg_ParseTuple(args, "O|O", &dissection_start_callback_py.ref(), &user_data_py.ref())) {
        delete dissection_start_registration_info;
        return NULL;
    }

    ++dissection_start_callback_py;
    ++user_data_py;

    if (EventHandler::current_phase == Phase::SHUTDOWN) {
        PyErr_SetString(PyExc_Exception, "register_dissection_start_callback is not allowed during shutdown phase");
        delete dissection_start_registration_info;
        return NULL;
    }

    if (!PyCallable_Check(dissection_start_callback_py)) {
        PyErr_SetString(PyExc_Exception, "dissection_start_callback is not a function");
        delete dissection_start_registration_info;
        return NULL;
    }

    dissection_start_registration_info->dissection_start_callback_py = dissection_start_callback_py;
    dissection_start_registration_info->user_data_py = !user_data_py.is_null() ? user_data_py : PyRef(Py_None);

    dissection_start_registration_infos.insert(dissection_start_registration_info);

    string dissection_start_callback_id = to_string((uint64_t)dissection_start_registration_info);
    PyRef dissection_start_callback_id_py(PyUnicode_FromString(dissection_start_callback_id.c_str()), true);

    return ++dissection_start_callback_id_py;
}

PyObject* PythonEventHandler::_deregister_dissection_start_callback(PyObject* self, PyObject* args) {
    PyRef deregister_result_py = Py_False;
    const char* dissection_start_callback_id = NULL;

    if (!PyArg_ParseTuple(args, "s", &dissection_start_callback_id)) {
        return NULL;
    }

    if (EventHandler::current_phase == Phase::SHUTDOWN) {
        PyErr_SetString(PyExc_Exception, "deregister_dissection_start_callback is not allowed during shutdown phase");
        return NULL;
    }

    uint64_t dissection_start_callback_id_uint = 0;
    from_chars_result conversion_result =
        from_chars(dissection_start_callback_id, dissection_start_callback_id + strlen(dissection_start_callback_id), dissection_start_callback_id_uint);
    if (conversion_result.ec != errc()) {
        PyErr_SetString(PyExc_Exception, "dissection_start_callback_id is not valid");
        return NULL;
    }
    PythonDissectionStartRegistrationInfo* dissection_start_registration_info = (PythonDissectionStartRegistrationInfo*)dissection_start_callback_id_uint;
    set<PythonDissectionStartRegistrationInfo*>::iterator dissection_start_registration_info_found = dissection_start_registration_infos.find(dissection_start_registration_info);

    if (dissection_start_registration_info_found != dissection_start_registration_infos.end()) {
        dissection_start_registration_infos.erase(dissection_start_registration_info);
        delete dissection_start_registration_info;
        deregister_result_py = Py_True;
    }

    return ++deregister_result_py;
}

PyObject* PythonEventHandler::_register_packet_listener(PyObject* self, PyObject* args) {
    PacketListenerRegistrationInfo* packet_listener_registration_info = new PacketListenerRegistrationInfo();
    PythonPacketListenerRegistrationInfo* pythpacket_listener_registration_info = new PythonPacketListenerRegistrationInfo();
    packet_listener_registration_info->user_data = pythpacket_listener_registration_info;
    packet_listener_registration_info->packet_listener_packet_callback = OnPacketListenerPacket;
    packet_listener_registration_info->packet_listener_finish_callback = OnPacketListenerFinish;
    packet_listener_registration_info->packet_listener_cleanup_callback = OnPacketListenerCleanup;

    const char* tap_name = NULL;
    const char* filter_string = NULL;
    PyRef wanted_field_names_list_py;
    PyRef all_fields_requested_py;
    PyRef packet_listener_packet_callback_py;
    PyRef packet_listener_finish_callback_py;
    PyRef packet_listener_cleanup_callback_py;
    PyRef user_data_py;

    if (!PyArg_ParseTuple(args, "ssOOOOOO", &tap_name, &filter_string, &wanted_field_names_list_py.ref(), &all_fields_requested_py.ref(), &packet_listener_packet_callback_py.ref(),
                          &packet_listener_finish_callback_py.ref(), &packet_listener_cleanup_callback_py.ref(), &user_data_py.ref())) {
        delete pythpacket_listener_registration_info;
        delete packet_listener_registration_info;
        return NULL;
    }

    ++wanted_field_names_list_py;
    ++all_fields_requested_py;
    ++packet_listener_packet_callback_py;
    ++packet_listener_finish_callback_py;
    ++packet_listener_cleanup_callback_py;
    ++user_data_py;

    if (EventHandler::current_phase != Phase::NORMAL_OPERATION) {
        PyErr_SetString(PyExc_Exception, "register_packet_listener is only allowed during normal operation phase");
        delete pythpacket_listener_registration_info;
        delete packet_listener_registration_info;
        return NULL;
    }

    packet_listener_registration_info->tap_name = tap_name;
    packet_listener_registration_info->filter_string = filter_string;

    if (!PyList_Check(wanted_field_names_list_py)) {
        PyErr_SetString(PyExc_Exception, "wanted_field_names is not a list");
        delete pythpacket_listener_registration_info;
        delete packet_listener_registration_info;
        return NULL;
    }

    Py_ssize_t wanded_field_names_list_count = PyList_Size(wanted_field_names_list_py);
    for (Py_ssize_t i = 0; i < wanded_field_names_list_count; i++) {
        PyRef wanted_field_name_py = PyList_GetItem(wanted_field_names_list_py, i);
        if (wanted_field_name_py.is_null()) {
            delete pythpacket_listener_registration_info;
            delete packet_listener_registration_info;
            return NULL;
        }

        if (!PyUnicode_Check(wanted_field_name_py)) {
            PyErr_SetString(PyExc_Exception, "element in wanted_field_names is not a string");
            delete pythpacket_listener_registration_info;
            delete packet_listener_registration_info;
            return NULL;
        }

        const char* wanted_field_name = PyUnicode_AsUTF8(wanted_field_name_py);
        packet_listener_registration_info->wanted_field_names.insert(wanted_field_name);
    }

    if (!PyBool_Check(all_fields_requested_py)) {
        PyErr_SetString(PyExc_Exception, "all_fields_requested is not a bool");
        delete pythpacket_listener_registration_info;
        delete packet_listener_registration_info;
        return NULL;
    }
    packet_listener_registration_info->all_fields_requested = Py_IsTrue(all_fields_requested_py) ? true : false;

    if (!PyCallable_Check(packet_listener_packet_callback_py)) {
        PyErr_SetString(PyExc_Exception, "packet_listener_packet_callback is not a function");
        delete pythpacket_listener_registration_info;
        delete packet_listener_registration_info;
        return NULL;
    }

    pythpacket_listener_registration_info->packet_listener_packet_callback_py = packet_listener_packet_callback_py;

    if (!(PyCallable_Check(packet_listener_finish_callback_py) || Py_IsNone(packet_listener_finish_callback_py))) {
        PyErr_SetString(PyExc_Exception, "packet_listener_finish_callback is not a function or None");
        delete pythpacket_listener_registration_info;
        delete packet_listener_registration_info;
        return NULL;
    }

    pythpacket_listener_registration_info->packet_listener_finish_callback_py = packet_listener_finish_callback_py;

    if (!(PyCallable_Check(packet_listener_cleanup_callback_py) || Py_IsNone(packet_listener_cleanup_callback_py))) {
        PyErr_SetString(PyExc_Exception, "packet_listener_cleanup_callback is not a function or None");
        delete pythpacket_listener_registration_info;
        delete packet_listener_registration_info;
        return NULL;
    }

    pythpacket_listener_registration_info->packet_listener_cleanup_callback_py = packet_listener_cleanup_callback_py;

    pythpacket_listener_registration_info->user_data_py = user_data_py;

    string error_message;
    bool registration_result = EventHandler::RegisterPacketListener(packet_listener_registration_info, packet_listener_registration_infos, error_message);
    if (!registration_result) {
        PyErr_SetString(PyExc_Exception, error_message.c_str());
        delete pythpacket_listener_registration_info;
        delete packet_listener_registration_info;
        return NULL;
    }

    string packet_listener_id = to_string((uint64_t)packet_listener_registration_info);
    PyRef packet_listener_id_py(PyUnicode_FromString(packet_listener_id.c_str()), true);

    return ++packet_listener_id_py;
}

PyObject* PythonEventHandler::_deregister_packet_listener(PyObject* self, PyObject* args) {
    const char* packet_listener_id = NULL;

    if (!PyArg_ParseTuple(args, "s", &packet_listener_id)) {
        return NULL;
    }

    if (EventHandler::current_phase != Phase::NORMAL_OPERATION) {
        PyErr_SetString(PyExc_Exception, "deregister_packet_listener is only allowed during normal operation phase");
        return NULL;
    }

    uint64_t packet_listener_id_uint = 0;
    from_chars_result conversion_result = from_chars(packet_listener_id, packet_listener_id + strlen(packet_listener_id), packet_listener_id_uint);
    if (conversion_result.ec != errc()) {
        PyErr_SetString(PyExc_Exception, "packet_listener_id is not valid");
        return NULL;
    }
    PacketListenerRegistrationInfo* packet_listener_registration_info = (PacketListenerRegistrationInfo*)packet_listener_id_uint;
    bool deregister_result = EventHandler::DeregisterPacketListener(packet_listener_registration_info, packet_listener_registration_infos);

    PyRef deregister_result_py = deregister_result ? Py_True : Py_False;

    return ++deregister_result_py;
}

PyObject* PythonEventHandler::_get_field_descriptions(PyObject* self, PyObject* args) {

    if (EventHandler::current_phase != Phase::NORMAL_OPERATION) {
        PyErr_SetString(PyExc_Exception, "get_field_descriptions is only allowed during normal operation phase");
        return NULL;
    }

    void* protocol_cookie = NULL;
    void* field_cookie = NULL;
    header_field_info* current_header_field_info = NULL;

    int32_t field_count = 0;

    for (int protocol_id = proto_get_first_protocol(&protocol_cookie); protocol_id != -1; protocol_id = proto_get_next_protocol(&protocol_cookie)) {
        for (current_header_field_info = proto_get_first_protocol_field(protocol_id, &field_cookie); current_header_field_info != NULL;
             current_header_field_info = proto_get_next_protocol_field(protocol_id, &field_cookie)) {
            // ignore duplicate names
            if (current_header_field_info->same_name_prev_id != -1) {
                continue;
            }
            field_count++;
        }
    }

    PyRef field_descriptions_py(PyList_New(field_count), true);

    protocol_cookie = NULL;
    field_cookie = NULL;
    current_header_field_info = NULL;
    int32_t field_index = 0;

    for (int protocol_id = proto_get_first_protocol(&protocol_cookie); protocol_id != -1; protocol_id = proto_get_next_protocol(&protocol_cookie)) {
        for (current_header_field_info = proto_get_first_protocol_field(protocol_id, &field_cookie); current_header_field_info != NULL;
             current_header_field_info = proto_get_next_protocol_field(protocol_id, &field_cookie)) {
            // ignore duplicate names
            if (current_header_field_info->same_name_prev_id != -1) {
                continue;
            }

            PyRef field_description_py(_create_field_instance(current_header_field_info, NULL), true);
            PyList_SetItem(field_descriptions_py, field_index, ++field_description_py); // PyList_SetItem does not increment ref count, so we need to do it

            field_index++;
        }
    }

    return ++field_descriptions_py;
}

class CreatePacketDataProcessNodeData {
  public:
    vector<tvbuff_t*>* buffers = NULL;
    PyRef parent_protocol_tree_node_py;
};

PyObject* PythonEventHandler::_create_packet_data(packet_info* packet_info, proto_tree* tree, tvbuff_t* packet_buffer) {
    ws_assert(packet_info);
    ws_assert(packet_info->fd);
    ws_assert(tree);

    vector<tvbuff_t*> buffers;
    buffers.push_back(packet_buffer);

    PyRef packet_data_py(PyObject_CallObject(shark_x_base_type_py, NULL), true);

    PyObject_SetAttrString(packet_data_py, "packet_buffer_index", PyRef(PyLong_FromLongLong(0), true));

    PyRef packet_info_py(PyObject_CallObject(shark_x_base_type_py, NULL), true);
    PyObject_SetAttrString(packet_data_py, "packet_info", packet_info_py);

    PyObject_SetAttrString(packet_info_py, "number", PyRef(PyLong_FromLongLong(packet_info->num), true));
    PyObject_SetAttrString(packet_info_py, "absolute_timestamp_seconds", PyRef(PyLong_FromLongLong(packet_info->abs_ts.secs), true));
    PyObject_SetAttrString(packet_info_py, "absolute_timestamp_nanoseconds", PyRef(PyLong_FromLongLong(packet_info->abs_ts.nsecs), true));
    PyObject_SetAttrString(packet_info_py, "relative_timestamp_seconds", PyRef(PyLong_FromLongLong(packet_info->rel_ts.secs), true));
    PyObject_SetAttrString(packet_info_py, "relative_timestamp_nanoseconds", PyRef(PyLong_FromLongLong(packet_info->rel_ts.nsecs), true));
    PyObject_SetAttrString(packet_info_py, "length", PyRef(PyLong_FromLongLong(packet_info->fd->pkt_len), true));
    PyObject_SetAttrString(packet_info_py, "visited", PyRef(PyBool_FromLong(packet_info->fd->visited), true));
    PyObject_SetAttrString(packet_info_py, "visible", PyRef(PyBool_FromLong((tree != NULL && tree->tree_data->visible != FALSE) ? 1 : 0), true));

    const char* protocol_column_text = packet_info->cinfo != NULL ? col_get_text(packet_info->cinfo, COL_PROTOCOL) : NULL;
    protocol_column_text = protocol_column_text != NULL ? protocol_column_text : "";
    PyObject_SetAttrString(packet_info_py, "protocol_column", PyRef(PyUnicode_FromString(protocol_column_text), true));

    const char* info_column_text = packet_info->cinfo != NULL ? col_get_text(packet_info->cinfo, COL_INFO) : NULL;
    info_column_text = info_column_text != NULL ? info_column_text : "";
    PyObject_SetAttrString(packet_info_py, "info_column", PyRef(PyUnicode_FromString(info_column_text), true));

    PyRef protocol_tree_py(PyObject_CallObject(shark_x_base_type_py, NULL), true);
    PyObject_SetAttrString(packet_data_py, "protocol_tree", protocol_tree_py);

    PyObject_SetAttrString(protocol_tree_py, "children", PyRef(PyList_New(0), true));
    PyObject_SetAttrString(packet_data_py, "buffers", PyRef(PyList_New(0), true));

    CreatePacketDataProcessNodeData create_packet_data_process_node_data = CreatePacketDataProcessNodeData();
    create_packet_data_process_node_data.buffers = &buffers;
    create_packet_data_process_node_data.parent_protocol_tree_node_py = protocol_tree_py;

    proto_tree_children_foreach(tree, _create_packet_data_process_node, &create_packet_data_process_node_data);

    for (auto&& current_buffer : buffers) {
        _add_buffer_to_packet_data(current_buffer, packet_data_py);
    }

    return ++packet_data_py;
}

void PythonEventHandler::_create_packet_data_process_node(proto_node* node, gpointer data) {
    ws_assert(node);
    ws_assert(node->finfo);
    ws_assert(node->finfo->hfinfo);
    ws_assert(data);

    CreatePacketDataProcessNodeData* create_packet_data_process_node_data = (CreatePacketDataProcessNodeData*)data;
    header_field_info* current_header_field_info = node->finfo->hfinfo;

    tvbuff_t* current_buffer = node->finfo->ds_tvb;
    int64_t buffer_index = -1;

    if (current_buffer != NULL) {
        vector<tvbuff_t*>* buffers = create_packet_data_process_node_data->buffers;
        for (size_t i = 0; i < buffers->size(); i++) {
            if (buffers->at(i) == current_buffer) {
                buffer_index = (int64_t)i;
                break;
            }
        }
        if (buffer_index == -1) {
            buffer_index = (int64_t)buffers->size();
            buffers->push_back(current_buffer);
        }
    }

    int64_t start_position = -1;
    int64_t length = 0;

    if (current_buffer != NULL) {
        start_position = node->finfo->start;
        length = node->finfo->length;
    }

    PyRef parent_protocol_tree_node_py = create_packet_data_process_node_data->parent_protocol_tree_node_py;
    PyRef children_py(PyObject_GetAttrString(parent_protocol_tree_node_py, "children"), true);

    PyRef protocol_tree_node_py(PyObject_CallObject(shark_x_base_type_py, NULL), true);
    PyList_Append(children_py, protocol_tree_node_py);

    PyRef field_instance_py(_create_field_instance(current_header_field_info, node->finfo), true);
    PyObject_SetAttrString(protocol_tree_node_py, "field_instance", field_instance_py);

    PyObject_SetAttrString(field_instance_py, "start_position", PyRef(PyLong_FromLongLong(start_position), true));
    PyObject_SetAttrString(field_instance_py, "length", PyRef(PyLong_FromLongLong(length), true));
    PyObject_SetAttrString(field_instance_py, "buffer_index", PyRef(PyLong_FromLongLong(buffer_index), true));
    PyObject_SetAttrString(field_instance_py, "hidden", PyRef(PyBool_FromLong(node->finfo->flags & FI_HIDDEN), true));
    PyObject_SetAttrString(field_instance_py, "generated", PyRef(PyBool_FromLong(node->finfo->flags & FI_GENERATED), true));
    PyObject_SetAttrString(field_instance_py, "big_endian", PyRef(PyBool_FromLong(node->finfo->flags & FI_BIG_ENDIAN), true));

    const char* representation = node->finfo->rep->representation != NULL ? node->finfo->rep->representation : "";
    PyObject_SetAttrString(field_instance_py, "representation", PyRef(PyUnicode_FromString(representation), true));

    PyObject_SetAttrString(protocol_tree_node_py, "children", PyRef(PyList_New(0), true));

    create_packet_data_process_node_data->parent_protocol_tree_node_py = protocol_tree_node_py;
    proto_tree_children_foreach(node, _create_packet_data_process_node, create_packet_data_process_node_data);
    create_packet_data_process_node_data->parent_protocol_tree_node_py = parent_protocol_tree_node_py;

    return;
}

PyObject* PythonEventHandler::_create_field_instance(header_field_info* current_header_field_info, field_info* current_field_info) {
    ws_assert(current_header_field_info);

    PyRef field_instance_py(PyObject_CallObject(shark_x_base_type_py, NULL), true);

    PyObject_SetAttrString(field_instance_py, "name", PyRef(PyUnicode_FromString(current_header_field_info->abbrev), true));
    PyObject_SetAttrString(field_instance_py, "display_name", PyRef(PyUnicode_FromString(current_header_field_info->name), true));

    PyObject_SetAttrString(field_instance_py, "field_id", PyRef(PyLong_FromLongLong(current_header_field_info->id), true));

    ftenum field_type = current_header_field_info->type;
    if (!(field_type == ftenum::FT_INT8 || field_type == ftenum::FT_INT16 || field_type == ftenum::FT_INT32 || field_type == ftenum::FT_INT64 || field_type == ftenum::FT_UINT8 ||
          field_type == ftenum::FT_UINT16 || field_type == ftenum::FT_UINT32 || field_type == ftenum::FT_UINT64 || field_type == ftenum::FT_FLOAT ||
          field_type == ftenum::FT_DOUBLE || field_type == ftenum::FT_CHAR || field_type == ftenum::FT_BOOLEAN || field_type == ftenum::FT_BYTES || field_type == ftenum::FT_IPv4 ||
          field_type == ftenum::FT_IPv6 || field_type == ftenum::FT_ETHER || field_type == ftenum::FT_GUID || field_type == ftenum::FT_EUI64 || field_type == ftenum::FT_STRING ||
          field_type == ftenum::FT_STRINGZ || field_type == ftenum::FT_PROTOCOL || field_type == ftenum::FT_ABSOLUTE_TIME || field_type == ftenum::FT_RELATIVE_TIME ||
          field_type == ftenum::FT_FRAMENUM)) {
        field_type = ftenum::FT_NONE;
    }

    PyObject_SetAttrString(field_instance_py, "field_type", PyRef(PyLong_FromLongLong((int)field_type), true));

    if (current_field_info != NULL) {

        PyObject_SetAttrString(field_instance_py, "int_value", Py_None);
        PyObject_SetAttrString(field_instance_py, "uint_value", Py_None);
        PyObject_SetAttrString(field_instance_py, "double_value", Py_None);
        PyObject_SetAttrString(field_instance_py, "string_value", Py_None);
        PyObject_SetAttrString(field_instance_py, "bytes_value", Py_None);

        if (field_type == ftenum::FT_INT8 || field_type == ftenum::FT_INT16 || field_type == ftenum::FT_INT32) {
            int64_t int_value = fvalue_get_sinteger(&(current_field_info->value));
            PyObject_SetAttrString(field_instance_py, "int_value", PyRef(PyLong_FromLongLong(int_value), true));
        } else if (field_type == ftenum::FT_INT64) {
            int64_t int_value = fvalue_get_sinteger64(&(current_field_info->value));
            PyObject_SetAttrString(field_instance_py, "int_value", PyRef(PyLong_FromLongLong(int_value), true));
        } else if (field_type == ftenum::FT_UINT8 || field_type == ftenum::FT_UINT16 || field_type == ftenum::FT_UINT32) {
            uint64_t uint_value = fvalue_get_uinteger(&(current_field_info->value));
            PyObject_SetAttrString(field_instance_py, "uint_value", PyRef(PyLong_FromUnsignedLongLong(uint_value), true));
        } else if (field_type == ftenum::FT_UINT64) {
            uint64_t uint_value = fvalue_get_uinteger64(&(current_field_info->value));
            PyObject_SetAttrString(field_instance_py, "uint_value", PyRef(PyLong_FromUnsignedLongLong(uint_value), true));
        } else if (field_type == ftenum::FT_FLOAT || field_type == ftenum::FT_DOUBLE) {
            double double_value = fvalue_get_floating(&(current_field_info->value));
            PyObject_SetAttrString(field_instance_py, "double_value", PyRef(PyFloat_FromDouble(double_value), true));
        } else if (field_type == ftenum::FT_CHAR) {
            uint64_t uint_value = fvalue_get_uinteger(&(current_field_info->value));
            PyObject_SetAttrString(field_instance_py, "uint_value", PyRef(PyLong_FromUnsignedLongLong(uint_value), true));
        } else if (field_type == ftenum::FT_BOOLEAN) {
            uint64_t uint_value = fvalue_get_uinteger64(&(current_field_info->value));
            PyObject_SetAttrString(field_instance_py, "uint_value", PyRef(PyLong_FromUnsignedLongLong(uint_value), true));
        } else if (field_type == ftenum::FT_BYTES) {
            size_t buffer_length = (size_t)current_field_info->value.value.bytes->len;
            const char* data_pointer = (const char*)current_field_info->value.value.bytes->data;

            PyObject_SetAttrString(field_instance_py, "bytes_value", PyRef(PyBytes_FromStringAndSize(data_pointer, buffer_length), true));
        } else if (field_type == ftenum::FT_IPv4) {
            size_t buffer_length = 4;
            uint32_t int_value = fvalue_get_uinteger(&(current_field_info->value));

            string bytes_value;
            bytes_value.reserve(buffer_length);

            bytes_value.push_back((char)((int_value >> 24) & 0xFF));
            bytes_value.push_back((char)((int_value >> 16) & 0xFF));
            bytes_value.push_back((char)((int_value >> 8) & 0xFF));
            bytes_value.push_back((char)((int_value >> 0) & 0xFF));

            const char* data_pointer = bytes_value.c_str();

            PyObject_SetAttrString(field_instance_py, "bytes_value", PyRef(PyBytes_FromStringAndSize(data_pointer, buffer_length), true));
        } else if (field_type == ftenum::FT_IPv6) {
            size_t buffer_length = 16;
            const char* data_pointer = (const char*)current_field_info->value.value.ipv6.addr.bytes;

            PyObject_SetAttrString(field_instance_py, "bytes_value", PyRef(PyBytes_FromStringAndSize(data_pointer, buffer_length), true));
        } else if (field_type == ftenum::FT_ETHER) {
            size_t buffer_length = 6;
            const char* data_pointer = data_pointer = (const char*)current_field_info->value.value.bytes->data;

            PyObject_SetAttrString(field_instance_py, "bytes_value", PyRef(PyBytes_FromStringAndSize(data_pointer, buffer_length), true));
        } else if (field_type == ftenum::FT_GUID) {
            uint32_t buffer_length = 16;
            e_guid_t guid = current_field_info->value.value.guid;

            string bytes_value;
            bytes_value.reserve(buffer_length);

            bytes_value.push_back((char)((guid.data1 >> 24) & 0xFF));
            bytes_value.push_back((char)((guid.data1 >> 16) & 0xFF));
            bytes_value.push_back((char)((guid.data1 >> 8) & 0xFF));
            bytes_value.push_back((char)((guid.data1 >> 0) & 0xFF));

            bytes_value.push_back((char)((guid.data2 >> 8) & 0xFF));
            bytes_value.push_back((char)((guid.data2 >> 0) & 0xFF));
            bytes_value.push_back((char)((guid.data3 >> 8) & 0xFF));
            bytes_value.push_back((char)((guid.data3 >> 0) & 0xFF));

            bytes_value.push_back((char)guid.data4[0]);
            bytes_value.push_back((char)guid.data4[1]);
            bytes_value.push_back((char)guid.data4[2]);
            bytes_value.push_back((char)guid.data4[3]);
            bytes_value.push_back((char)guid.data4[4]);
            bytes_value.push_back((char)guid.data4[5]);
            bytes_value.push_back((char)guid.data4[6]);
            bytes_value.push_back((char)guid.data4[7]);

            const char* data_pointer = bytes_value.c_str();

            PyObject_SetAttrString(field_instance_py, "bytes_value", PyRef(PyBytes_FromStringAndSize(data_pointer, buffer_length), true));
        } else if (field_type == ftenum::FT_EUI64) {
            uint32_t buffer_length = 8;
            uint64_t uint_value = fvalue_get_uinteger64(&(current_field_info->value));

            string bytes_value;
            bytes_value.reserve(buffer_length);

            bytes_value.push_back((char)((uint_value >> 56) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 48) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 40) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 32) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 24) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 16) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 8) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 0) & 0xFFLL));

            const char* data_pointer = bytes_value.c_str();

            PyObject_SetAttrString(field_instance_py, "bytes_value", PyRef(PyBytes_FromStringAndSize(data_pointer, buffer_length), true));
        } else if (field_type == ftenum::FT_STRING || field_type == ftenum::FT_STRINGZ) {
            const char* data_pointer = current_field_info->value.value.string;
            PyObject_SetAttrString(field_instance_py, "string_value", PyRef(PyUnicode_FromString(data_pointer), true));
        } else if (field_type == ftenum::FT_PROTOCOL) {
            const char* data_pointer = current_field_info->value.value.protocol.proto_string;
            PyObject_SetAttrString(field_instance_py, "string_value", PyRef(PyUnicode_FromString(data_pointer), true));
        } else if (field_type == ftenum::FT_ABSOLUTE_TIME || field_type == ftenum::FT_RELATIVE_TIME) {
            double double_value = (double)(current_field_info->value.value.time.secs) + (double)(current_field_info->value.value.time.nsecs) / 1000000000.0;
            PyObject_SetAttrString(field_instance_py, "double_value", PyRef(PyFloat_FromDouble(double_value), true));
        } else if (field_type == ftenum::FT_FRAMENUM) {
            uint64_t uint_value = fvalue_get_uinteger(&(current_field_info->value));
            PyObject_SetAttrString(field_instance_py, "uint_value", PyRef(PyLong_FromUnsignedLongLong(uint_value), true));
        } else // FieldType::NONE and potentially others
        {
            // Do nothing
        }
    }

    return ++field_instance_py;
}

void PythonEventHandler::_add_buffer_to_packet_data(tvbuff_t* buffer, PyObject* packet_data_py) {
    int32_t buffer_length = tvb_captured_length(buffer);
    string current_buffer;
    current_buffer.reserve(buffer_length);

    for (int32_t i = 0; i < buffer_length; i++) {
        uint8_t current_byte = tvb_get_guint8(buffer, (gint)i);
        current_buffer.push_back(current_byte);
    }

    PyRef buffers_py(PyObject_GetAttrString(packet_data_py, "buffers"), true);

    PyRef buffer_py(PyBytes_FromStringAndSize(current_buffer.c_str(), buffer_length), true);
    PyList_Append(buffers_py, buffer_py);
}

} // namespace shark_x

// shark_x member
namespace shark_x {
bool PythonEventHandler::is_initialized = false;
set<PythonLateInitRegistrationInfo*> PythonEventHandler::late_init_registration_infos = set<PythonLateInitRegistrationInfo*>();
set<PythonShutdownRegistrationInfo*> PythonEventHandler::shutdown_registration_infos = set<PythonShutdownRegistrationInfo*>();
set<PythonDissectionStartRegistrationInfo*> PythonEventHandler::dissection_start_registration_infos = set<PythonDissectionStartRegistrationInfo*>();
set<PacketListenerRegistrationInfo*> PythonEventHandler::packet_listener_registration_infos = set<PacketListenerRegistrationInfo*>();

} // namespace shark_x

// python member
namespace shark_x {
PyRef PythonEventHandler::main_module_py = NULL;
PyRef PythonEventHandler::globals_py = NULL;
PyRef PythonEventHandler::shark_x_module_py = NULL;
PyRef PythonEventHandler::shark_x_base_type_py = NULL;
} // namespace shark_x

// init functions
namespace shark_x {

void PythonEventHandler::Init() {

    if (!Py_IsInitialized()) {
        const char* executable_directory = get_progfile_dir();

        wchar_t* home_directory_wide = Py_DecodeLocale(executable_directory, NULL);
        Py_SetPythonHome(home_directory_wide);
        PyMem_RawFree(home_directory_wide);

#ifdef _WIN32
        // char* lib_directory = g_strdup_printf("%s;%s;%s;%s;%s;%s;%s\\Lib;%s\\Lib\\site-packages", get_plugins_pers_dir_with_version(), get_plugins_pers_dir(),
        //                                       get_plugins_dir_with_version(), get_plugins_dir(), executable_directory, executable_directory, executable_directory);
        char* lib_directory = g_strdup_printf("%s;%s;%s;%s;%s;%s/lib/python%d.%d;%s/lib/python%d.%d/site-packages", get_plugins_pers_dir_with_version(), get_plugins_pers_dir(),
                                              get_plugins_dir_with_version(), get_plugins_dir(), executable_directory, executable_directory, PYTHON_MAJOR_VERSION,
                                              PYTHON_MINOR_VERSION, executable_directory, PYTHON_MAJOR_VERSION, PYTHON_MINOR_VERSION);
#else
        char* lib_directory = g_strdup_printf("%s:%s:%s:%s:%s:%s/lib/python%d.%d:%s/lib/python%d.%d/site-packages", get_plugins_pers_dir_with_version(), get_plugins_pers_dir(),
                                              get_plugins_dir_with_version(), get_plugins_dir(), executable_directory, executable_directory, PYTHON_MAJOR_VERSION,
                                              PYTHON_MINOR_VERSION, executable_directory, PYTHON_MAJOR_VERSION, PYTHON_MINOR_VERSION);

#endif // _WIN32
        wchar_t* lib_directory_wide = Py_DecodeLocale(lib_directory, NULL);
        Py_SetPath(lib_directory_wide);
        PyMem_RawFree(lib_directory_wide);
        g_free(lib_directory);

        if (PyImport_AppendInittab("shark_x", InitModule) != 0) {
            python_handle_exception();
            return;
        }

        Py_SetProgramName(L"shark_x");

        Py_Initialize();
        main_module_py = PyImport_AddModule("__main__");
        if (main_module_py.is_null()) {
            python_handle_exception();
            return;
        }

        globals_py = PyModule_GetDict(main_module_py);
        if (globals_py.is_null()) {
            python_handle_exception();
            return;
        }

        shark_x_module_py = InitModule();
        if (shark_x_module_py.is_null()) {
            python_handle_exception();
            return;
        }

        if (PyObject_SetItem(globals_py, PyUnicode_FromString("shark_x"), shark_x_module_py)) {
            python_handle_exception();
            return;
        }

        const char* shark_x_code =
            R"""(
class shark_x_base_type:
    def __repr__(self):
        import pprint
        return pprint.pformat(self.__dict__)
    def __str__(self):
        return __repr__(self)

import os, sys
if sys.stdout is None:
    sys.stdout = open(os.devnull, 'w')
if sys.stderr is None:
    sys.stderr = open(os.devnull, 'w')
            )""";

        PyRef run_string_result(PyRun_String(shark_x_code, Py_file_input, globals_py, globals_py), true);
        if (run_string_result.is_null()) {
            python_handle_exception();
            return;
        }

        shark_x_base_type_py = PyDict_GetItemString(globals_py, "shark_x_base_type");
        if (shark_x_base_type_py.is_null()) {
            python_handle_exception();
            return;
        }

        is_initialized = true;
    }

    ExecuteFiles();
}

PyObject* PythonEventHandler::InitModule() {
    static PyMethodDef shark_x_functions[] = {{"get_current_phase", _get_current_phase, METH_VARARGS, ""},
                                              {"report_failure", _report_failure, METH_VARARGS, ""},
                                              {"report_warning", _report_warning, METH_VARARGS, ""},
                                              {"register_late_init_callback", _register_late_init_callback, METH_VARARGS, ""},
                                              {"deregister_late_init_callback", _deregister_late_init_callback, METH_VARARGS, ""},
                                              {"register_shutdown_callback", _register_shutdown_callback, METH_VARARGS, ""},
                                              {"deregister_shutdown_callback", _deregister_shutdown_callback, METH_VARARGS, ""},
                                              {"register_dissection_start_callback", _register_dissection_start_callback, METH_VARARGS, ""},
                                              {"deregister_dissection_start_callback", _deregister_dissection_start_callback, METH_VARARGS, ""},
                                              {"register_packet_listener", _register_packet_listener, METH_VARARGS, ""},
                                              {"deregister_packet_listener", _deregister_packet_listener, METH_VARARGS, ""},
                                              {"get_field_descriptions", _get_field_descriptions, METH_VARARGS, ""},
                                              {NULL, NULL, 0, NULL}};

    static struct PyModuleDef shark_x_module_definition = {PyModuleDef_HEAD_INIT, "shark_x", NULL, -1, shark_x_functions};

    PyRef shark_x_module_py(PyModule_Create(&shark_x_module_definition), true);

    PyModule_AddIntConstant(shark_x_module_py, "SHARK_X_MAJOR_VERSION", SHARK_X_MAJOR_VERSION);
    PyModule_AddIntConstant(shark_x_module_py, "SHARK_X_MINOR_VERSION", SHARK_X_MINOR_VERSION);
    PyModule_AddIntConstant(shark_x_module_py, "SHARK_X_PATCH_VERSION", SHARK_X_PATCH_VERSION);

    int wireshark_major_version = 0;
    int wireshark_minor_version = 0;
    int wireshark_patch_version = 0;
    epan_get_version_number(&wireshark_major_version, &wireshark_minor_version, &wireshark_patch_version);

    PyModule_AddIntConstant(shark_x_module_py, "WIRESHARK_MAJOR_VERSION", wireshark_major_version);
    PyModule_AddIntConstant(shark_x_module_py, "WIRESHARK_MAJOR_VERSION", wireshark_minor_version);
    PyModule_AddIntConstant(shark_x_module_py, "WIRESHARK_MAJOR_VERSION", wireshark_patch_version);

    PyModule_AddStringConstant(shark_x_module_py, "PERSONAL_PLUGIN_DIRECTORY_WITH_VERSION", get_plugins_pers_dir_with_version());
    PyModule_AddStringConstant(shark_x_module_py, "PERSONAL_PLUGIN_DIRECTORY", get_plugins_pers_dir());
    PyModule_AddStringConstant(shark_x_module_py, "GLOBAL_PLUGIN_DIRECTORY_WITH_VERSION", get_plugins_dir_with_version());
    PyModule_AddStringConstant(shark_x_module_py, "GLOBAL_PLUGIN_DIRECTORY", get_plugins_dir());

    PyModule_AddStringConstant(shark_x_module_py, "EXECUTABLE_DIRECTORY", get_progfile_dir());

    PyModule_AddIntConstant(shark_x_module_py, "PHASE_REGISTRATION", (int)Phase::REGISTRATION);
    PyModule_AddIntConstant(shark_x_module_py, "PHASE_NORMAL", (int)Phase::NORMAL_OPERATION);
    PyModule_AddIntConstant(shark_x_module_py, "PHASE_SHUTDOWN", (int)Phase::SHUTDOWN);

    PyModule_AddIntConstant(shark_x_module_py, "FT_INT8", (int)ftenum::FT_INT8);
    PyModule_AddIntConstant(shark_x_module_py, "FT_INT16", (int)ftenum::FT_INT16);
    PyModule_AddIntConstant(shark_x_module_py, "FT_INT32", (int)ftenum::FT_INT32);
    PyModule_AddIntConstant(shark_x_module_py, "FT_INT64", (int)ftenum::FT_INT64);
    PyModule_AddIntConstant(shark_x_module_py, "FT_UINT8", (int)ftenum::FT_UINT8);
    PyModule_AddIntConstant(shark_x_module_py, "FT_UINT16", (int)ftenum::FT_UINT16);
    PyModule_AddIntConstant(shark_x_module_py, "FT_UINT32", (int)ftenum::FT_UINT32);
    PyModule_AddIntConstant(shark_x_module_py, "FT_UINT64", (int)ftenum::FT_UINT64);
    PyModule_AddIntConstant(shark_x_module_py, "FT_FLOAT", (int)ftenum::FT_FLOAT);
    PyModule_AddIntConstant(shark_x_module_py, "FT_DOUBLE", (int)ftenum::FT_DOUBLE);
    PyModule_AddIntConstant(shark_x_module_py, "FT_CHAR", (int)ftenum::FT_CHAR);
    PyModule_AddIntConstant(shark_x_module_py, "FT_BOOL", (int)ftenum::FT_BOOLEAN);
    PyModule_AddIntConstant(shark_x_module_py, "FT_BYTES", (int)ftenum::FT_BYTES);
    PyModule_AddIntConstant(shark_x_module_py, "FT_IPV4_ADDRESS", (int)ftenum::FT_IPv4);
    PyModule_AddIntConstant(shark_x_module_py, "FT_IPV6_ADDRESS", (int)ftenum::FT_IPv6);
    PyModule_AddIntConstant(shark_x_module_py, "FT_ETHERNET_ADDRESS", (int)ftenum::FT_ETHER);
    PyModule_AddIntConstant(shark_x_module_py, "FT_GUID", (int)ftenum::FT_GUID);
    PyModule_AddIntConstant(shark_x_module_py, "FT_EUI64", (int)ftenum::FT_EUI64);
    PyModule_AddIntConstant(shark_x_module_py, "FT_STRING", (int)ftenum::FT_STRING);
    PyModule_AddIntConstant(shark_x_module_py, "FT_STRINGZ", (int)ftenum::FT_STRINGZ);
    PyModule_AddIntConstant(shark_x_module_py, "FT_PROTOCOL", (int)ftenum::FT_PROTOCOL);
    PyModule_AddIntConstant(shark_x_module_py, "FT_ABSOLUTE_TIME", (int)ftenum::FT_ABSOLUTE_TIME);
    PyModule_AddIntConstant(shark_x_module_py, "FT_RELATIVE_TIME", (int)ftenum::FT_RELATIVE_TIME);
    PyModule_AddIntConstant(shark_x_module_py, "FT_FRAME_NUMBER", (int)ftenum::FT_FRAMENUM);
    PyModule_AddIntConstant(shark_x_module_py, "FT_NONE", (int)ftenum::FT_NONE);

    return ++shark_x_module_py;
}

void PythonEventHandler::ExecuteFiles() {
    if (!is_initialized) {
        return;
    }

    vector<string> file_paths = get_file_paths_of_plugin_directories(".py");

    for (auto&& file_path : file_paths) {
        PyRef file_path_py(PyUnicode_FromString(file_path.c_str()), true);
        if (file_path_py.is_null()) {
            python_handle_exception();
            continue;
        }
        FILE* file = _Py_fopen_obj(file_path_py, "r+");
        if (file == NULL) {
            PyErr_Format(PyExc_Exception, "Can't open file: %s", file_path.c_str());
            python_handle_exception();
            continue;
        }

        PyRef run_file_result_py(PyRun_File(file, file_path.c_str(), Py_file_input, globals_py, globals_py), true);
        if (run_file_result_py.is_null()) {
            fclose(file);
            python_handle_exception();
            continue;
        }

        fclose(file);
    }
}

} // namespace shark_x

// protected functions
namespace shark_x {
void PythonEventHandler::OnLateInit() {
    for (auto&& late_init_registration_info : late_init_registration_infos) {

        if (!PyCallable_Check(late_init_registration_info->late_init_callback_py)) {
            PyErr_SetString(PyExc_Exception, "late_init_callback is not a function");
            python_handle_exception();
            continue;
        }
        PyRef args_py(Py_BuildValue("(O)", late_init_registration_info->user_data_py), true);

        PyRef result_py(PyObject_CallObject(late_init_registration_info->late_init_callback_py, args_py), true);
        if (result_py.is_null()) {
            python_handle_exception();
            continue;
        }
    }
}

void PythonEventHandler::OnShutdown() {
    if (!is_initialized) {
        return;
    }

    // clear late init registrations
    for (auto&& late_init_registration_info : late_init_registration_infos) {
        delete late_init_registration_info;
    }
    late_init_registration_infos.clear();

    // clear dissection start registrations
    for (auto&& dissection_start_registration_info : dissection_start_registration_infos) {
        delete dissection_start_registration_info;
    }
    dissection_start_registration_infos.clear();

    // clear packet listener registrations (this will trigger a packet listener
    // finish events)
    vector<PacketListenerRegistrationInfo*> temp_packet_listener_registration_infos;
    for (auto&& packet_listener_registration_info : packet_listener_registration_infos) {
        temp_packet_listener_registration_infos.push_back(packet_listener_registration_info);
    }
    for (auto&& packet_listener_registration_info : temp_packet_listener_registration_infos) {
        EventHandler::DeregisterPacketListener(packet_listener_registration_info, packet_listener_registration_infos);
    }
    temp_packet_listener_registration_infos.clear();

    // execute registered on_shutdown callbacks
    for (auto&& shutdown_registration_info : shutdown_registration_infos) {

        if (!PyCallable_Check(shutdown_registration_info->shutdown_callback_py)) {
            PyErr_SetString(PyExc_Exception, "shutdown_callback is not a function");
            python_handle_exception();
            continue;
        }
        PyRef args_py(Py_BuildValue("(O)", shutdown_registration_info->user_data_py), true);

        PyRef result_py(PyObject_CallObject(shutdown_registration_info->shutdown_callback_py, args_py), true);
        if (result_py.is_null()) {
            python_handle_exception();
            continue;
        }
    }

    // clear shutdown registrations
    for (auto&& shutdown_registration_info : shutdown_registration_infos) {
        delete shutdown_registration_info;
    }
    shutdown_registration_infos.clear();

    shark_x_base_type_py = NULL;
    shark_x_module_py = NULL;
    globals_py = NULL;
    main_module_py = NULL;

    Py_Finalize();
    is_initialized = false;
}

void PythonEventHandler::OnDissectionStart() {
    for (auto&& dissection_start_registration_info : dissection_start_registration_infos) {
        if (!PyCallable_Check(dissection_start_registration_info->dissection_start_callback_py)) {
            PyErr_SetString(PyExc_Exception, "dissection_start_callback is not a function");
            python_handle_exception();
            continue;
        }
        PyRef args_py(Py_BuildValue("(O)", dissection_start_registration_info->user_data_py), true);

        PyRef result_py(PyObject_CallObject(dissection_start_registration_info->dissection_start_callback_py, args_py), true);
        if (result_py.is_null()) {
            python_handle_exception();
            continue;
        }
    }
}

tap_packet_status PythonEventHandler::OnPacketListenerPacket(void* user_data, packet_info* packet_info, epan_dissect_t* epan_dissect, const void* _data) {
    PacketListenerRegistrationInfo* packet_listener_registration_info = (PacketListenerRegistrationInfo*)user_data;
    PythonPacketListenerRegistrationInfo* pythpacket_listener_registration_info = (PythonPacketListenerRegistrationInfo*)packet_listener_registration_info->user_data;

    if (!PyCallable_Check(pythpacket_listener_registration_info->packet_listener_packet_callback_py)) {
        PyErr_SetString(PyExc_Exception, "packet_listener_packet_callback is not a function");
        python_handle_exception();
        return tap_packet_status::TAP_PACKET_DONT_REDRAW;
    }

    PyRef packet_data_py(_create_packet_data(packet_info, epan_dissect->tree, epan_dissect->tvb), true);

    PyRef args_py(Py_BuildValue("(OO)", packet_data_py, pythpacket_listener_registration_info->user_data_py), true);

    PyRef result_py(PyObject_CallObject(pythpacket_listener_registration_info->packet_listener_packet_callback_py, args_py), true);
    if (result_py.is_null()) {
        python_handle_exception();
        return tap_packet_status::TAP_PACKET_DONT_REDRAW;
    }

    return tap_packet_status::TAP_PACKET_DONT_REDRAW;
}

void PythonEventHandler::OnPacketListenerFinish(void* user_data) {
    PacketListenerRegistrationInfo* packet_listener_registration_info = (PacketListenerRegistrationInfo*)user_data;
    PythonPacketListenerRegistrationInfo* pythpacket_listener_registration_info = (PythonPacketListenerRegistrationInfo*)packet_listener_registration_info->user_data;

    if (!PyCallable_Check(pythpacket_listener_registration_info->packet_listener_finish_callback_py)) { // packet_listener_cleanup_callback is optional
        return;
    }
    PyRef args_py(Py_BuildValue("(O)", pythpacket_listener_registration_info->user_data_py), true);

    PyRef result_py(PyObject_CallObject(pythpacket_listener_registration_info->packet_listener_finish_callback_py, args_py), true);
    if (result_py.is_null()) {
        python_handle_exception();
        return;
    }
}

void PythonEventHandler::OnPacketListenerCleanup(void* user_data) {
    PacketListenerRegistrationInfo* packet_listener_registration_info = (PacketListenerRegistrationInfo*)user_data;
    PythonPacketListenerRegistrationInfo* pythpacket_listener_registration_info = (PythonPacketListenerRegistrationInfo*)packet_listener_registration_info->user_data;

    if (!PyCallable_Check(pythpacket_listener_registration_info->packet_listener_cleanup_callback_py)) { // packet_listener_cleanup_callback is optional
        return;
    }
    PyRef args_py(Py_BuildValue("(O)", pythpacket_listener_registration_info->user_data_py), true);

    PyRef result_py(PyObject_CallObject(pythpacket_listener_registration_info->packet_listener_cleanup_callback_py, args_py), true);
    if (result_py.is_null()) {
        python_handle_exception();
        return;
    }
}
} // namespace shark_x

// public functions
namespace shark_x {
void PythonEventHandler::Register() {
    Init();

    register_final_registration_routine(OnLateInit);

    register_shutdown_routine(OnShutdown);

    register_init_routine(OnDissectionStart);
}

void PythonEventHandler::RegisterHandoff() {}
} // namespace shark_x

#endif // SHARK_X_PYTHON_ENABLED