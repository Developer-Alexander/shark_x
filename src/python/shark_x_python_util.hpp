/*
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR
A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SPDX-License-Identifier: MIT
*/

#ifndef __SHARK_X_PYTHON_UTIL__
#define __SHARK_X_PYTHON_UTIL__

#ifdef SHARK_X_PYTHON_ENABLED

// shark_x includes
#include "shark_x_util.hpp"

// wireshark includes
#include "wsutil/report_message.h"

// python includes
#define PY_SSIZE_T_CLEAN
#include "Python.h"

// std includes
#include <string>

namespace shark_x {
class PyRef {

  private:
    PyObject* ref_py = NULL;

  public:
    PyObject*& ref() { return ref_py; }

    void set_ref(PyObject*& new_ref_py) {
        Py_XDECREF(ref_py);
        ref_py = new_ref_py;
        Py_XINCREF(ref_py);
    }

    bool is_null() { return ref_py == NULL; }

    PyRef& inc_ref_count() {
        Py_XINCREF(ref_py);
        return *this;
    }

    PyRef& dec_ref_count() {
        Py_XDECREF(ref_py);
        return *this;
    }

    int64_t get_ref_count() {
        if (ref_py == NULL) {
            return 0;
        }
        return ref_py->ob_refcnt;
    }

    PyRef() {}

    PyRef(const PyRef& other) {
        PyObject* other_ref = other.ref_py;
        set_ref(other_ref);
    }

    PyRef(PyObject* new_ref_py, bool is_new_ref = false) {
        if (is_new_ref) {
            ref_py = new_ref_py;

        } else {
            set_ref(new_ref_py);
        }
    }

    ~PyRef() {
        Py_XDECREF(ref_py);
        ref_py = NULL;
    }

    // assign from PyObject*
    PyRef& operator=(PyObject* new_ref_py) {
        set_ref(new_ref_py);
        return *this;
    }

    // assign to PyObject*
    operator PyObject*&() { return ref_py; }

    PyRef& operator=(const PyRef& other) {
        PyObject* other_ref = other.ref_py;
        set_ref(other_ref);
        return *this;
    }

    // increment ref count
    PyRef& operator++() {
        Py_XINCREF(ref_py);
        return *this;
    }

    // decrement ref count
    PyRef& operator--() {
        Py_XDECREF(ref_py);
        return *this;
    }
};

static void python_handle_exception() {
    if (PyErr_Occurred() != NULL) {
        std::string error_message = "PYTHON ERROR: ";

        // Fetch the type, value and traceback of the exception
        PyRef type_py;
        PyRef value_py;
        PyRef traceback_py;

        PyErr_Fetch(&type_py.ref(), &value_py.ref(), &traceback_py.ref());
        PyErr_NormalizeException(&type_py.ref(), &value_py.ref(), &traceback_py.ref());

        ++type_py;
        ++value_py;
        ++traceback_py;

        if (!type_py.is_null()) {
            PyRef type_string_py(PyObject_GetAttrString(type_py, "__name__"), true);
            const char* type = PyUnicode_AsUTF8(type_string_py);
            string_append_format(error_message, "%s: ", type);
        }
        if (!value_py.is_null()) {
            PyRef value_string_py(PyObject_Str(value_py));
            const char* value_string = PyUnicode_AsUTF8(value_string_py);
            error_message += value_string;
        }

        if (!traceback_py.is_null()) {
            string_append_format(error_message, "\nTraceback: ");
            PyTracebackObject* traceback = (PyTracebackObject*)traceback_py.ref();
            while (traceback != NULL) {
                int line_number = PyFrame_GetLineNumber(traceback->tb_frame);
                PyCodeObject* code_py = PyFrame_GetCode(traceback->tb_frame);

                const char* file_name = PyUnicode_AsUTF8(code_py->co_filename);
                const char* function_name = PyUnicode_AsUTF8(code_py->co_name);

                string_append_format(error_message, "\n%s:%i in %s", file_name, line_number, function_name);

                traceback = traceback->tb_next;
            }
        }

        report_failure("%s", error_message.c_str());

        PyErr_Clear();
    }
}

} // namespace shark_x

#endif // SHARK_X_PYTHON_ENABLED

#endif // __SHARK_X_PYTHON_UTIL__