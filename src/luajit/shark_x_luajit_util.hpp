/*
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR
A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SPDX-License-Identifier: MIT
*/

#ifndef __SHARK_X_LUAJIT_UTIL__
#define __SHARK_X_LUAJIT_UTIL__

#ifdef SHARK_X_LUAJIT_ENABLED

// shark_x includes
#include "shark_x_util.hpp"

// wireshark includes
#include "wsutil/report_message.h"

// luajit includes
#include "lua.hpp"

// std includes
#include <string>

namespace shark_x {

static void luajit_print_stack(lsx_State* L) {
    std::string stack_trace;

    int top_index = lsx_gettop(L);

    for (int i = top_index; i >= 1; i--) {
        int relative_index = -top_index + i - 1;

        string_append_format(stack_trace, "%i (%i): %s", i, relative_index, lsx_typename(L, lsx_type(L, i)));

        if (lsx_isboolean(L, i)) {
            string_append_format(stack_trace, " [%s]\n", lsx_toboolean(L, -1) != 0 ? "true" : "false");

        } else if (lsx_isnumber(L, i)) {
            string_append_format(stack_trace, " [%f]\n", lsx_tonumber(L, -1));

        } else if (lsx_isstring(L, i)) {
            string_append_format(stack_trace, " [%s]\n", lsx_tostring(L, -1));

        } else if (lsx_isnil(L, i)) {
            string_append_format(stack_trace, "\n");

        } else if (lsx_istable(L, i)) {
            uint32_t member_count = 0;
            std::string member = "";

            lsx_pushnil(L);
            while (lsx_next(L, i)) {
                member_count++;

                if (lsx_isboolean(L, -2)) {
                    string_append_format(member, "(%s - ", lsx_toboolean(L, -2) != 0 ? "true" : "false");
                } else if (lsx_isnumber(L, -2)) {
                    string_append_format(member, "(%d - ", lsx_tonumber(L, -2));

                } else if (lsx_isstring(L, -2)) {
                    string_append_format(member, "(\"%s\" - ", lsx_tostring(L, -2));

                } else {
                    string_append_format(member, "(%s - ", lsx_typename(L, lsx_type(L, -2)));
                }

                if (lsx_isboolean(L, -1)) {
                    string_append_format(member, "%s) ", lsx_toboolean(L, -1) != 0 ? "true" : "false");
                } else if (lsx_isnumber(L, -1)) {
                    string_append_format(member, "%d) ", lsx_tonumber(L, -1));

                } else if (lsx_isstring(L, -1)) {
                    string_append_format(member, "\"%s\") ", lsx_tostring(L, -1));

                } else {
                    string_append_format(member, "%s) ", lsx_typename(L, lsx_type(L, -1)));
                }

                lsx_pop(L, 1); // pop value, keep key as last key
            }

            string_append_format(stack_trace, " [%d member] %s\n", member_count, member.c_str());

        } else {
            string_append_format(stack_trace, "\n");
        }
    }

    report_failure("%s", stack_trace.c_str());
}

#define luajit_set_bool_attribute(attribute_name, value)                                                                                                                           \
    lsx_pushboolean(L, (value) ? 1 : 0);                                                                                                                                           \
    lsx_setfield(L, -2, attribute_name);

#define luajit_set_number_attribute(attribute_name, value)                                                                                                                         \
    lsx_pushinteger(L, value);                                                                                                                                                     \
    lsx_setfield(L, -2, attribute_name);

#define luajit_set_string_attribute(attribute_name, value)                                                                                                                         \
    lsx_pushstring(L, value);                                                                                                                                                      \
    lsx_setfield(L, -2, attribute_name);

#define luajit_set_string_with_length_attribute(attribute_name, value, length)                                                                                                     \
    lsx_pushlstring(L, value, length);                                                                                                                                             \
    lsx_setfield(L, -2, attribute_name);

#define luajit_set_function_attribute(attribute_name, value)                                                                                                                       \
    lsx_pushcfunction(L, value);                                                                                                                                                   \
    lsx_setfield(L, -2, attribute_name);

#define luajit_set_table_attribute(attribute_name)                                                                                                                                 \
    lsx_newtable(L);                                                                                                                                                               \
    lsx_setfield(L, -2, attribute_name);

#define luajit_set_light_user_data_attribute(attribute_name, value)                                                                                                                \
    lsx_pushlightuserdata(L, value);                                                                                                                                               \
    lsx_setfield(L, -2, attribute_name);

#define luajit_set_nil_attribute(attribute_name)                                                                                                                                   \
    lsx_pushnil(L);                                                                                                                                                                \
    lsx_setfield(L, -2, attribute_name);

#define luajit_set_attribute(attribute_name, stack_position)                                                                                                                       \
    lsx_pushvalue(L, stack_position);                                                                                                                                              \
    lsx_setfield(L, -2, attribute_name);

#define luajit_set_attribute_with_light_user_data_key(key)                                                                                                                         \
    lsx_pushlightuserdata(L, key);                                                                                                                                                 \
    lsx_pushvalue(L, -2);                                                                                                                                                          \
    lsx_settable(L, -4);                                                                                                                                                           \
    lsx_pop(L, 1);

#define luajit_set_nil_attribute_with_light_user_data_key(key)                                                                                                                     \
    lsx_pushlightuserdata(L, key);                                                                                                                                                 \
    lsx_pushnil(L);                                                                                                                                                                \
    lsx_settable(L, -3);

#define luajit_get_attribute_with_light_user_data_key(key)                                                                                                                         \
    lsx_pushlightuserdata(L, key);                                                                                                                                                 \
    lsx_gettable(L, -2);

} // namespace shark_x

#endif // SHARK_X_LUAJIT_ENABLED

#endif // __SHARK_X_LUAJIT_UTIL__
