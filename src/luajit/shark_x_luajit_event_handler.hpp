/*
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR
A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SPDX-License-Identifier: MIT
*/

#ifndef __SHARK_X_LUAJIT_EVENT_HANDLER__
#define __SHARK_X_LUAJIT_EVENT_HANDLER__

#ifdef SHARK_X_LUAJIT_ENABLED

// shark_x includes
#include "shark_x.h"
#include "shark_x_luajit_types.hpp"
#include "shark_x_types.hpp"

struct lsx_State;

namespace shark_x {
class LuajitEventHandler {
  protected:
    // api implementation

    static int _get_current_phase(lsx_State* L);
    static int _report_failure(lsx_State* L);
    static int _report_warning(lsx_State* L);
    static int _register_late_init_callback(lsx_State* L);
    static int _deregister_late_init_callback(lsx_State* L);
    static int _register_shutdown_callback(lsx_State* L);
    static int _deregister_shutdown_callback(lsx_State* L);
    static int _register_dissection_start_callback(lsx_State* L);
    static int _deregister_dissection_start_callback(lsx_State* L);
    static int _register_packet_listener(lsx_State* L);
    static int _deregister_packet_listener(lsx_State* L);
    static int _get_field_descriptions(lsx_State* L);

    static void _create_packet_data(packet_info* packet_info, proto_tree* tree, tvbuff_t* packet_buffer);
    static void _create_packet_data_process_node(proto_node* node, gpointer data);
    static void _create_field_instance(header_field_info* current_header_field_info, field_info* current_field_info);
    static void _add_buffer_to_packet_data(tvbuff_t* buffer);

    // shark_x member

    static bool is_initialized;
    static std::set<LuajitLateInitRegistrationInfo*> late_init_registration_infos;
    static std::set<LuajitShutdownRegistrationInfo*> shutdown_registration_infos;
    static std::set<LuajitDissectionStartRegistrationInfo*> dissection_start_registration_infos;
    static std::set<PacketListenerRegistrationInfo*> packet_listener_registration_infos;

    static lsx_State* L;

    // init functions

    static void Init();
    static void InitModule();
    static void ExecuteFiles();
    static int AtPanic(lsx_State* L);

    // protected functions

    static void OnLateInit();
    static void OnShutdown();
    static void OnDissectionStart();
    static tap_packet_status OnPacketListenerPacket(void* user_data, packet_info* packet_info, epan_dissect_t* epan_dissect, const void* _data);
    static void OnPacketListenerFinish(void* user_data);
    static void OnPacketListenerCleanup(void* user_data);

  public:
    // public functions

    static void Register();
    static void RegisterHandoff();
};
} // namespace shark_x

#endif // SHARK_X_LUAJIT_ENABLED

#endif // __SHARK_X_LUAJIT_EVENT_HANDLER__