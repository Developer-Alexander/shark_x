/*
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR
A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SPDX-License-Identifier: MIT
*/

#ifdef SHARK_X_LUAJIT_ENABLED

// shark_x includes
#include "shark_x_luajit_event_handler.hpp"
#include "shark_x_event_handler.hpp"
#include "shark_x_luajit_types.hpp"
#include "shark_x_luajit_util.hpp"
#include "shark_x_util.hpp"

// wireshark includes
#include "epan/epan.h"
#include "epan/epan_dissect.h"
#include "epan/packet.h"
#include "epan/proto.h"
#include "wsutil/file_util.h"
#include "wsutil/filesystem.h"
#include "wsutil/report_message.h"
#include "wsutil/wslog.h"

// luajit includes
#include "lua.hpp"

// std includes
#include <charconv>
#include <string>
#include <vector>

using namespace std;

// api implementation
namespace shark_x {

int LuajitEventHandler::_get_current_phase(lsx_State* L) {
    int number_of_arguments = lsx_gettop(L);
    if (number_of_arguments != 0) {
        lsxL_error(L, "number of arguments is not 0");
        return 0;
    }

    lsx_pushinteger(L, (int)EventHandler::current_phase);
    return 1;
}

int LuajitEventHandler::_report_failure(lsx_State* L) {
    int number_of_arguments = lsx_gettop(L);
    if (number_of_arguments != 1) {
        lsxL_error(L, "number of arguments is not 1");
        return 0;
    }

    const char* message = lsxL_checkstring(L, -1);
    report_failure("%s", message);
    return 0;
}

int LuajitEventHandler::_report_warning(lsx_State* L) {
    int number_of_arguments = lsx_gettop(L);
    if (number_of_arguments != 1) {
        lsxL_error(L, "number of arguments is not 1");
        return 0;
    }
    const char* message = lsxL_checkstring(L, -1);
    report_warning("%s", message);
    return 0;
}

int LuajitEventHandler::_register_late_init_callback(lsx_State* L) {

    LuajitLateInitRegistrationInfo* late_init_registration_info = new LuajitLateInitRegistrationInfo();

    if (EventHandler::current_phase == Phase::SHUTDOWN) {
        delete late_init_registration_info;
        lsxL_error(L, "register_late_init_callback is not allowed during shutdown phase");
        return 0;
    }

    int number_of_arguments = lsx_gettop(L);
    if (!(number_of_arguments >= 1 && number_of_arguments <= 2)) {
        delete late_init_registration_info;
        lsxL_error(L, "number of arguments is not between 1 and 2");
        return 0;
    }
    if (!lsx_isfunction(L, 1)) {
        delete late_init_registration_info;
        lsxL_error(L, "late_init_callback is not a function");
        return 0;
    }

    lsx_getregistry(L);
    lsx_getfield(L, -1, "late_init_registration_infos");

    lsx_newtable(L); // late_init_registration_info
    luajit_set_attribute("late_init_callback", 1);
    luajit_set_attribute("user_data", 2);

    luajit_set_attribute_with_light_user_data_key(late_init_registration_info);

    lsx_pop(L, 1); // pop late_init_registration_infos
    lsx_pop(L, 1); // pop registry

    late_init_registration_infos.insert(late_init_registration_info);

    string late_init_callback_id = to_string((uint64_t)late_init_registration_info);
    lsx_pushstring(L, late_init_callback_id.c_str());

    return 1;
}
int LuajitEventHandler::_deregister_late_init_callback(lsx_State* L) {

    if (EventHandler::current_phase == Phase::SHUTDOWN) {
        lsxL_error(L, "deregister_late_init_callback is not allowed during shutdown phase");
        return 0;
    }

    int number_of_arguments = lsx_gettop(L);
    if (number_of_arguments != 1) {
        lsxL_error(L, "number of arguments is not 1");
        return 0;
    }

    const char* late_init_callback_id = lsxL_checkstring(L, -1);

    uint64_t late_init_callback_id_uint = 0;
    from_chars_result conversion_result = from_chars(late_init_callback_id, late_init_callback_id + strlen(late_init_callback_id), late_init_callback_id_uint);
    if (conversion_result.ec != errc()) {
        lsxL_error(L, "late_init_callback_id is not valid");
        return 0;
    }
    LuajitLateInitRegistrationInfo* late_init_registration_info = (LuajitLateInitRegistrationInfo*)late_init_callback_id_uint;

    bool result = false;

    set<LuajitLateInitRegistrationInfo*>::iterator late_init_registration_info_found = late_init_registration_infos.find(late_init_registration_info);
    if (late_init_registration_info_found != late_init_registration_infos.end()) {

        lsx_getregistry(L);
        lsx_getfield(L, -1, "late_init_registration_infos");

        luajit_set_nil_attribute_with_light_user_data_key(late_init_registration_info);

        lsx_pop(L, 1); // pop late_init_registration_infos
        lsx_pop(L, 1); // pop registry

        late_init_registration_infos.erase(late_init_registration_info);
        delete late_init_registration_info;
        result = true;
    }

    lsx_pushboolean(L, result ? 1 : 0);

    return 1;
}
int LuajitEventHandler::_register_shutdown_callback(lsx_State* L) {

    LuajitShutdownRegistrationInfo* shutdown_registration_info = new LuajitShutdownRegistrationInfo();

    if (EventHandler::current_phase == Phase::SHUTDOWN) {
        delete shutdown_registration_info;
        lsxL_error(L, "register_shutdown_callback is not allowed during shutdown phase");
        return 0;
    }

    int number_of_arguments = lsx_gettop(L);
    if (!(number_of_arguments >= 1 && number_of_arguments <= 2)) {
        delete shutdown_registration_info;
        lsxL_error(L, "number of arguments is not between 1 and 2");
        return 0;
    }
    if (!lsx_isfunction(L, 1)) {
        delete shutdown_registration_info;
        lsxL_error(L, "shutdown_callback is not a function");
        return 0;
    }

    lsx_getregistry(L);
    lsx_getfield(L, -1, "shutdown_registration_infos");

    lsx_newtable(L); // shutdown_registration_info
    luajit_set_attribute("shutdown_callback", 1);
    luajit_set_attribute("user_data", 2);

    luajit_set_attribute_with_light_user_data_key(shutdown_registration_info);

    lsx_pop(L, 1); // pop shutdown_registration_infos
    lsx_pop(L, 1); // pop registry

    shutdown_registration_infos.insert(shutdown_registration_info);

    string shutdown_callback_id = to_string((uint64_t)shutdown_registration_info);
    lsx_pushstring(L, shutdown_callback_id.c_str());

    return 1;
}
int LuajitEventHandler::_deregister_shutdown_callback(lsx_State* L) {
    if (EventHandler::current_phase == Phase::SHUTDOWN) {
        lsxL_error(L, "deregister_shutdown_callback is not allowed during shutdown phase");
        return 0;
    }

    int number_of_arguments = lsx_gettop(L);
    if (number_of_arguments != 1) {
        lsxL_error(L, "number of arguments is not 1");
        return 0;
    }

    const char* shutdown_callback_id = lsxL_checkstring(L, -1);

    uint64_t shutdown_callback_id_uint = 0;
    from_chars_result conversion_result = from_chars(shutdown_callback_id, shutdown_callback_id + strlen(shutdown_callback_id), shutdown_callback_id_uint);
    if (conversion_result.ec != errc()) {
        lsxL_error(L, "shutdown_callback_id is not valid");
        return 0;
    }
    LuajitShutdownRegistrationInfo* shutdown_registration_info = (LuajitShutdownRegistrationInfo*)shutdown_callback_id_uint;

    bool result = false;

    set<LuajitShutdownRegistrationInfo*>::iterator shutdown_registration_info_found = shutdown_registration_infos.find(shutdown_registration_info);
    if (shutdown_registration_info_found != shutdown_registration_infos.end()) {

        lsx_getregistry(L);
        lsx_getfield(L, -1, "shutdown_registration_infos");

        luajit_set_nil_attribute_with_light_user_data_key(shutdown_registration_info);

        lsx_pop(L, 1); // pop shutdown_registration_infos
        lsx_pop(L, 1); // pop registry

        shutdown_registration_infos.erase(shutdown_registration_info);
        delete shutdown_registration_info;
        result = true;
    }

    lsx_pushboolean(L, result ? 1 : 0);

    return 1;
}
int LuajitEventHandler::_register_dissection_start_callback(lsx_State* L) {

    LuajitDissectionStartRegistrationInfo* dissection_start_registration_info = new LuajitDissectionStartRegistrationInfo();

    if (EventHandler::current_phase == Phase::SHUTDOWN) {
        delete dissection_start_registration_info;
        lsxL_error(L, "register_dissection_start_callback is not allowed during shutdown phase");
        return 0;
    }

    int number_of_arguments = lsx_gettop(L);
    if (!(number_of_arguments >= 1 && number_of_arguments <= 2)) {
        delete dissection_start_registration_info;
        lsxL_error(L, "number of arguments is not between 1 and 2");
        return 0;
    }
    if (!lsx_isfunction(L, 1)) {
        delete dissection_start_registration_info;
        lsxL_error(L, "dissection_start_callback is not a function");
        return 0;
    }

    lsx_getregistry(L);
    lsx_getfield(L, -1, "dissection_start_registration_infos");

    lsx_newtable(L); // dissection_start_registration_info
    luajit_set_attribute("dissection_start_callback", 1);
    luajit_set_attribute("user_data", 2);

    luajit_set_attribute_with_light_user_data_key(dissection_start_registration_info);

    lsx_pop(L, 1); // pop dissection_start_registration_infos
    lsx_pop(L, 1); // pop registry

    dissection_start_registration_infos.insert(dissection_start_registration_info);

    string dissection_start_callback_id = to_string((uint64_t)dissection_start_registration_info);
    lsx_pushstring(L, dissection_start_callback_id.c_str());

    return 1;
}
int LuajitEventHandler::_deregister_dissection_start_callback(lsx_State* L) {
    if (EventHandler::current_phase == Phase::SHUTDOWN) {
        lsxL_error(L, "deregister_dissection_start_callback is not allowed during shutdown phase");
        return 0;
    }

    int number_of_arguments = lsx_gettop(L);
    if (number_of_arguments != 1) {
        lsxL_error(L, "number of arguments is not 1");
        return 0;
    }

    const char* dissection_start_callback_id = lsxL_checkstring(L, -1);

    uint64_t dissection_start_callback_id_uint = 0;
    from_chars_result conversion_result =
        from_chars(dissection_start_callback_id, dissection_start_callback_id + strlen(dissection_start_callback_id), dissection_start_callback_id_uint);
    if (conversion_result.ec != errc()) {
        lsxL_error(L, "dissection_start_callback_id is not valid");
        return 0;
    }
    LuajitDissectionStartRegistrationInfo* dissection_start_registration_info = (LuajitDissectionStartRegistrationInfo*)dissection_start_callback_id_uint;

    bool result = false;

    set<LuajitDissectionStartRegistrationInfo*>::iterator dissection_start_registration_info_found = dissection_start_registration_infos.find(dissection_start_registration_info);
    if (dissection_start_registration_info_found != dissection_start_registration_infos.end()) {

        lsx_getregistry(L);
        lsx_getfield(L, -1, "dissection_start_registration_infos");

        luajit_set_nil_attribute_with_light_user_data_key(dissection_start_registration_info);

        lsx_pop(L, 1); // pop dissection_start_registration_infos
        lsx_pop(L, 1); // pop registry

        dissection_start_registration_infos.erase(dissection_start_registration_info);
        delete dissection_start_registration_info;
        result = true;
    }

    lsx_pushboolean(L, result ? 1 : 0);

    return 1;
}

int LuajitEventHandler::_register_packet_listener(lsx_State* L) {

    PacketListenerRegistrationInfo* packet_listener_registration_info = new PacketListenerRegistrationInfo();
    LuajitPacketListenerRegistrationInfo* luajit_packet_listener_registration_info = new LuajitPacketListenerRegistrationInfo();
    packet_listener_registration_info->user_data = luajit_packet_listener_registration_info;
    packet_listener_registration_info->packet_listener_packet_callback = OnPacketListenerPacket;
    packet_listener_registration_info->packet_listener_finish_callback = OnPacketListenerFinish;
    packet_listener_registration_info->packet_listener_cleanup_callback = OnPacketListenerCleanup;

    if (EventHandler::current_phase != Phase::NORMAL_OPERATION) {

        delete luajit_packet_listener_registration_info;
        delete packet_listener_registration_info;
        lsxL_error(L, "register_packet_listener is only allowed during normal operation phase");
        return 0;
    }

    int number_of_arguments = lsx_gettop(L);
    if (number_of_arguments != 8) {
        delete luajit_packet_listener_registration_info;
        delete packet_listener_registration_info;
        lsxL_error(L, "number of arguments is not 8");
        return 0;
    }

    if (!lsx_isstring(L, 1)) {

        delete luajit_packet_listener_registration_info;
        delete packet_listener_registration_info;
        lsxL_error(L, "tap_name is not a string");
        return 0;
    }

    const char* tap_name = lsxL_checkstring(L, 1);
    packet_listener_registration_info->tap_name = tap_name;

    if (!lsx_isstring(L, 2)) {

        delete luajit_packet_listener_registration_info;
        delete packet_listener_registration_info;
        lsxL_error(L, "filter_string is not a string");
        return 0;
    }

    const char* filter_string = lsxL_checkstring(L, 2);
    packet_listener_registration_info->tap_name = filter_string;

    if (!lsx_istable(L, 3)) {

        delete luajit_packet_listener_registration_info;
        delete packet_listener_registration_info;
        lsxL_error(L, "wanted_field_names is not a array like table");
        return 0;
    }

    size_t wanded_field_names_list_count = lsx_objlen(L, 3);
    for (size_t i = 1; i < wanded_field_names_list_count + 1; i++) {
        lsx_rawgeti(L, 3, i);
        if (!lsx_isstring(L, -1)) {

            delete luajit_packet_listener_registration_info;
            delete packet_listener_registration_info;
            lsxL_error(L, "element in wanted_field_names is not a string");
            return 0;
        }

        const char* wanted_field_name = lsxL_checkstring(L, -1);
        packet_listener_registration_info->wanted_field_names.insert(wanted_field_name);

        lsx_pop(L, 1); // pop wanted_field_name
    }

    if (!lsx_isboolean(L, 4)) {

        delete luajit_packet_listener_registration_info;
        delete packet_listener_registration_info;
        lsxL_error(L, "all_fields_requested is not a bool");
        return 0;
    }

    packet_listener_registration_info->all_fields_requested = lsx_toboolean(L, 4) != 0 ? true : false;

    if (!lsx_isfunction(L, 5)) {

        delete luajit_packet_listener_registration_info;
        delete packet_listener_registration_info;
        lsxL_error(L, "packet_listener_packet_callback is not a function");
        return 0;
    }

    if (!(lsx_isfunction(L, 6) || lsx_isnil(L, 6))) {

        delete luajit_packet_listener_registration_info;
        delete packet_listener_registration_info;
        lsxL_error(L, "packet_listener_finish_callback is not a function or nil");
        return 0;
    }

    if (!(lsx_isfunction(L, 7) || lsx_isnil(L, 7))) {

        delete luajit_packet_listener_registration_info;
        delete packet_listener_registration_info;
        lsxL_error(L, "packet_listener_cleanup_callback is not a function or nil");
        return 0;
    }

    string error_message;
    bool registration_result = EventHandler::RegisterPacketListener(packet_listener_registration_info, packet_listener_registration_infos, error_message);
    if (!registration_result) {

        delete luajit_packet_listener_registration_info;
        delete packet_listener_registration_info;
        lsxL_error(L, error_message.c_str());
        return 0;
    }

    lsx_getregistry(L);
    lsx_getfield(L, -1, "packet_listener_registration_infos");

    lsx_newtable(L); // packet_listener_registration_info
    luajit_set_attribute("packet_listener_packet_callback", 5);
    luajit_set_attribute("packet_listener_finish_callback", 6);
    luajit_set_attribute("packet_listener_cleanup_callback", 7);
    luajit_set_attribute("user_data", 8);

    luajit_set_attribute_with_light_user_data_key(packet_listener_registration_info);

    lsx_pop(L, 1); // pop packet_listener_registration_infos
    lsx_pop(L, 1); // pop registry

    string packet_listener_id = to_string((uint64_t)packet_listener_registration_info);

    lsx_pushstring(L, packet_listener_id.c_str());

    return 1;
}

int LuajitEventHandler::_deregister_packet_listener(lsx_State* L) {
    if (EventHandler::current_phase != Phase::NORMAL_OPERATION) {
        lsxL_error(L, "deregister_packet_listener is only allowed during normal operation phase");
        return 0;
    }

    int number_of_arguments = lsx_gettop(L);
    if (number_of_arguments != 1) {
        lsxL_error(L, "number of arguments is not 1");
        return 0;
    }

    const char* packet_listener_id = lsxL_checkstring(L, -1);

    uint64_t packet_listener_id_uint = 0;
    from_chars_result conversion_result = from_chars(packet_listener_id, packet_listener_id + strlen(packet_listener_id), packet_listener_id_uint);
    if (conversion_result.ec != errc()) {
        lsxL_error(L, "packet_listener_id is not valid");
        return 0;
    }

    PacketListenerRegistrationInfo* packet_listener_registration_info = (PacketListenerRegistrationInfo*)packet_listener_id_uint;
    bool deregister_result = EventHandler::DeregisterPacketListener(packet_listener_registration_info, packet_listener_registration_infos);
    if (deregister_result) {
        lsx_getregistry(L);
        lsx_getfield(L, -1, "packet_listener_registration_infos");

        luajit_set_nil_attribute_with_light_user_data_key((PacketListenerRegistrationInfo*)packet_listener_id_uint);

        lsx_pop(L, 1); // pop packet_listener_registration_infos
        lsx_pop(L, 1); // pop registry
    }

    lsx_pushboolean(L, deregister_result ? 1 : 0);

    return 1;
}

int LuajitEventHandler::_get_field_descriptions(lsx_State* L) {
    if (EventHandler::current_phase != Phase::NORMAL_OPERATION) {
        lsxL_error(L, "get_field_descriptions is only allowed during normal operation phase");
    }

    void* protocol_cookie = NULL;
    void* field_cookie = NULL;
    header_field_info* current_header_field_info = NULL;

    int32_t field_count = 0;

    for (int protocol_id = proto_get_first_protocol(&protocol_cookie); protocol_id != -1; protocol_id = proto_get_next_protocol(&protocol_cookie)) {
        for (current_header_field_info = proto_get_first_protocol_field(protocol_id, &field_cookie); current_header_field_info != NULL;
             current_header_field_info = proto_get_next_protocol_field(protocol_id, &field_cookie)) {
            // ignore duplicate names
            if (current_header_field_info->same_name_prev_id != -1) {
                continue;
            }
            field_count++;
        }
    }

    lsx_newtable(L);

    protocol_cookie = NULL;
    field_cookie = NULL;
    current_header_field_info = NULL;
    int32_t field_index = 0;

    for (int protocol_id = proto_get_first_protocol(&protocol_cookie); protocol_id != -1; protocol_id = proto_get_next_protocol(&protocol_cookie)) {
        for (current_header_field_info = proto_get_first_protocol_field(protocol_id, &field_cookie); current_header_field_info != NULL;
             current_header_field_info = proto_get_next_protocol_field(protocol_id, &field_cookie)) {
            // ignore duplicate names
            if (current_header_field_info->same_name_prev_id != -1) {
                continue;
            }

            _create_field_instance(current_header_field_info, NULL);
            lsx_rawseti(L, -2, field_index);

            field_index++;
        }
    }

    return 1;
}

class CreatePacketDataProcessNodeData {
  public:
    vector<tvbuff_t*>* buffers = NULL;
};

void LuajitEventHandler::_create_packet_data(packet_info* packet_info, proto_tree* tree, tvbuff_t* packet_buffer) {
    ws_assert(packet_info);
    ws_assert(packet_info->fd);
    ws_assert(tree);

    vector<tvbuff_t*> buffers;
    buffers.push_back(packet_buffer);

    lsx_newtable(L); // packet_data

    luajit_set_number_attribute("packet_buffer_index", 0);

    lsx_newtable(L); // packet_info

    luajit_set_number_attribute("number", packet_info->num);
    luajit_set_number_attribute("absolute_timestamp_seconds", packet_info->abs_ts.secs);
    luajit_set_number_attribute("absolute_timestamp_nanoseconds", packet_info->abs_ts.nsecs);
    luajit_set_number_attribute("relative_timestamp_seconds", packet_info->rel_ts.secs);
    luajit_set_number_attribute("relative_timestamp_nanoseconds", packet_info->rel_ts.nsecs);
    luajit_set_number_attribute("length", packet_info->fd->pkt_len);
    luajit_set_bool_attribute("visited", packet_info->fd->visited);
    luajit_set_bool_attribute("visible", (tree != NULL && tree->tree_data->visible != FALSE));

    const char* protocol_column_text = packet_info->cinfo != NULL ? col_get_text(packet_info->cinfo, COL_PROTOCOL) : NULL;
    protocol_column_text = protocol_column_text != NULL ? protocol_column_text : "";
    luajit_set_string_attribute("protocol_column", protocol_column_text);

    const char* info_column_text = packet_info->cinfo != NULL ? col_get_text(packet_info->cinfo, COL_INFO) : NULL;
    info_column_text = info_column_text != NULL ? info_column_text : "";
    luajit_set_string_attribute("info_column", info_column_text);

    lsx_setfield(L, -2, "packet_info");

    lsx_newtable(L); // protocol_tree
    luajit_set_table_attribute("children");

    CreatePacketDataProcessNodeData create_packet_data_process_node_data = CreatePacketDataProcessNodeData();
    create_packet_data_process_node_data.buffers = &buffers;

    proto_tree_children_foreach(tree, _create_packet_data_process_node, &create_packet_data_process_node_data);

    lsx_setfield(L, -2, "protocol_tree");

    luajit_set_table_attribute("buffers");

    for (auto&& current_buffer : buffers) {
        _add_buffer_to_packet_data(current_buffer);
    }

    return;
}

void LuajitEventHandler::_create_packet_data_process_node(proto_node* node, gpointer data) {
    ws_assert(node);
    ws_assert(node->finfo);
    ws_assert(node->finfo->hfinfo);
    ws_assert(data);

    CreatePacketDataProcessNodeData* create_packet_data_process_node_data = (CreatePacketDataProcessNodeData*)data;
    header_field_info* current_header_field_info = node->finfo->hfinfo;

    tvbuff_t* current_buffer = node->finfo->ds_tvb;
    int64_t buffer_index = -1;

    if (current_buffer != NULL) {
        vector<tvbuff_t*>* buffers = create_packet_data_process_node_data->buffers;
        for (size_t i = 0; i < buffers->size(); i++) {
            if (buffers->at(i) == current_buffer) {
                buffer_index = (int64_t)i;
                break;
            }
        }
        if (buffer_index == -1) {
            buffer_index = (int64_t)buffers->size();
            buffers->push_back(current_buffer);
        }
    }

    int64_t start_position = -1;
    int64_t length = 0;

    if (current_buffer != NULL) {
        start_position = node->finfo->start;
        length = node->finfo->length;
    }

    lsx_getfield(L, -1, "children");

    lsx_newtable(L); // protocol_tree_node

    _create_field_instance(current_header_field_info, node->finfo);

    luajit_set_number_attribute("start_position", start_position);
    luajit_set_number_attribute("length", length);
    luajit_set_number_attribute("buffer_index", buffer_index);
    luajit_set_bool_attribute("hidden", (node->finfo->flags & FI_HIDDEN) > 0);
    luajit_set_bool_attribute("generated", (node->finfo->flags & FI_GENERATED) > 0);
    luajit_set_bool_attribute("big_endian", (node->finfo->flags & FI_BIG_ENDIAN) > 0);

    lsx_setfield(L, -2, "field_instance");

    const char* representation = node->finfo->rep->representation != NULL ? node->finfo->rep->representation : "";
    luajit_set_string_attribute("representation", representation);

    luajit_set_table_attribute("children");

    proto_tree_children_foreach(node, _create_packet_data_process_node, create_packet_data_process_node_data);

    size_t children_count = lsx_objlen(L, -2);
    lsx_rawseti(L, -2, children_count + 1); // append protocol_tree_node

    lsx_pop(L, 1); // pop children
    return;
}

void LuajitEventHandler::_create_field_instance(header_field_info* current_header_field_info, field_info* current_field_info) {
    lsx_newtable(L);
    luajit_set_string_attribute("name", current_header_field_info->abbrev);
    luajit_set_string_attribute("display_name", current_header_field_info->name);
    luajit_set_number_attribute("field_id", current_header_field_info->id);

    ftenum field_type = current_header_field_info->type;
    if (!(field_type == ftenum::FT_INT8 || field_type == ftenum::FT_INT16 || field_type == ftenum::FT_INT32 || field_type == ftenum::FT_INT64 || field_type == ftenum::FT_UINT8 ||
          field_type == ftenum::FT_UINT16 || field_type == ftenum::FT_UINT32 || field_type == ftenum::FT_UINT64 || field_type == ftenum::FT_FLOAT ||
          field_type == ftenum::FT_DOUBLE || field_type == ftenum::FT_CHAR || field_type == ftenum::FT_BOOLEAN || field_type == ftenum::FT_BYTES || field_type == ftenum::FT_IPv4 ||
          field_type == ftenum::FT_IPv6 || field_type == ftenum::FT_ETHER || field_type == ftenum::FT_GUID || field_type == ftenum::FT_EUI64 || field_type == ftenum::FT_STRING ||
          field_type == ftenum::FT_STRINGZ || field_type == ftenum::FT_PROTOCOL || field_type == ftenum::FT_ABSOLUTE_TIME || field_type == ftenum::FT_RELATIVE_TIME ||
          field_type == ftenum::FT_FRAMENUM)) {
        field_type = ftenum::FT_NONE;
    }

    luajit_set_number_attribute("field_type", (int)field_type);

    if (current_field_info != NULL) {

        if (field_type == ftenum::FT_INT8 || field_type == ftenum::FT_INT16 || field_type == ftenum::FT_INT32) {

            int64_t int_value = fvalue_get_sinteger(&(current_field_info->value));
            luajit_set_number_attribute("int_value", int_value);
        } else if (field_type == ftenum::FT_INT64) {
            int64_t int_value = fvalue_get_sinteger64(&(current_field_info->value));
            string int_value_string = to_string(int_value);
            luajit_set_string_attribute("int_value", int_value_string.c_str());
        } else if (field_type == ftenum::FT_UINT8 || field_type == ftenum::FT_UINT16 || field_type == ftenum::FT_UINT32) {
            uint64_t uint_value = fvalue_get_uinteger(&(current_field_info->value));
            luajit_set_number_attribute("uint_value", uint_value);
        } else if (field_type == ftenum::FT_UINT64) {
            uint64_t uint_value = fvalue_get_uinteger64(&(current_field_info->value));
            string uint_value_string = to_string(uint_value);
            luajit_set_string_attribute("uint_value", uint_value_string.c_str());
        } else if (field_type == ftenum::FT_FLOAT || field_type == ftenum::FT_DOUBLE) {
            double double_value = fvalue_get_floating(&(current_field_info->value));
            luajit_set_number_attribute("double_value", double_value);
        } else if (field_type == ftenum::FT_CHAR) {
            uint64_t uint_value = fvalue_get_uinteger(&(current_field_info->value));
            luajit_set_number_attribute("uint_value", uint_value);
        } else if (field_type == ftenum::FT_BOOLEAN) {
            uint64_t uint_value = fvalue_get_uinteger64(&(current_field_info->value));
            luajit_set_number_attribute("uint_value", uint_value);
        } else if (field_type == ftenum::FT_BYTES) {
            size_t buffer_length = (size_t)current_field_info->value.value.bytes->len;
            const char* data_pointer = (const char*)current_field_info->value.value.bytes->data;

            luajit_set_string_with_length_attribute("bytes_value", data_pointer, buffer_length);
        } else if (field_type == ftenum::FT_IPv4) {
            size_t buffer_length = 4;
            uint32_t int_value = fvalue_get_uinteger(&(current_field_info->value));

            string bytes_value;
            bytes_value.reserve(buffer_length);

            bytes_value.push_back((char)((int_value >> 24) & 0xFF));
            bytes_value.push_back((char)((int_value >> 16) & 0xFF));
            bytes_value.push_back((char)((int_value >> 8) & 0xFF));
            bytes_value.push_back((char)((int_value >> 0) & 0xFF));

            const char* data_pointer = bytes_value.c_str();

            luajit_set_string_with_length_attribute("bytes_value", data_pointer, buffer_length);
        } else if (field_type == ftenum::FT_IPv6) {
            size_t buffer_length = 16;
            const char* data_pointer = (const char*)current_field_info->value.value.ipv6.addr.bytes;

            luajit_set_string_with_length_attribute("bytes_value", data_pointer, buffer_length);
        } else if (field_type == ftenum::FT_ETHER) {
            size_t buffer_length = 6;
            const char* data_pointer = data_pointer = (const char*)current_field_info->value.value.bytes->data;

            luajit_set_string_with_length_attribute("bytes_value", data_pointer, buffer_length);
        } else if (field_type == ftenum::FT_GUID) {
            uint32_t buffer_length = 16;
            e_guid_t guid = current_field_info->value.value.guid;

            string bytes_value;
            bytes_value.reserve(buffer_length);

            bytes_value.push_back((char)((guid.data1 >> 24) & 0xFF));
            bytes_value.push_back((char)((guid.data1 >> 16) & 0xFF));
            bytes_value.push_back((char)((guid.data1 >> 8) & 0xFF));
            bytes_value.push_back((char)((guid.data1 >> 0) & 0xFF));

            bytes_value.push_back((char)((guid.data2 >> 8) & 0xFF));
            bytes_value.push_back((char)((guid.data2 >> 0) & 0xFF));
            bytes_value.push_back((char)((guid.data3 >> 8) & 0xFF));
            bytes_value.push_back((char)((guid.data3 >> 0) & 0xFF));

            bytes_value.push_back((char)guid.data4[0]);
            bytes_value.push_back((char)guid.data4[1]);
            bytes_value.push_back((char)guid.data4[2]);
            bytes_value.push_back((char)guid.data4[3]);
            bytes_value.push_back((char)guid.data4[4]);
            bytes_value.push_back((char)guid.data4[5]);
            bytes_value.push_back((char)guid.data4[6]);
            bytes_value.push_back((char)guid.data4[7]);

            const char* data_pointer = bytes_value.c_str();

            luajit_set_string_with_length_attribute("bytes_value", data_pointer, buffer_length);
        } else if (field_type == ftenum::FT_EUI64) {
            uint32_t buffer_length = 8;
            uint64_t uint_value = fvalue_get_uinteger64(&(current_field_info->value));

            string bytes_value;
            bytes_value.reserve(buffer_length);

            bytes_value.push_back((char)((uint_value >> 56) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 48) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 40) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 32) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 24) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 16) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 8) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 0) & 0xFFLL));

            const char* data_pointer = bytes_value.c_str();

            luajit_set_string_with_length_attribute("bytes_value", data_pointer, buffer_length);
        } else if (field_type == ftenum::FT_STRING || field_type == ftenum::FT_STRINGZ) {
            const char* data_pointer = current_field_info->value.value.string;
            luajit_set_string_attribute("string_value", data_pointer);
        } else if (field_type == ftenum::FT_PROTOCOL) {
            const char* data_pointer = current_field_info->value.value.protocol.proto_string;
            luajit_set_string_attribute("string_value", data_pointer);
        } else if (field_type == ftenum::FT_ABSOLUTE_TIME || field_type == ftenum::FT_RELATIVE_TIME) {
            double double_value = (double)(current_field_info->value.value.time.secs) + (double)(current_field_info->value.value.time.nsecs) / 1000000000.0;
            luajit_set_number_attribute("double_value", double_value);
        } else if (field_type == ftenum::FT_FRAMENUM) {
            uint64_t uint_value = fvalue_get_uinteger(&(current_field_info->value));
            luajit_set_number_attribute("uint_value", uint_value);
        } else // FieldType::NONE and potentially others
        {
            // Do nothing
        }
    }

    return;
}

void LuajitEventHandler::_add_buffer_to_packet_data(tvbuff_t* buffer) {
    int32_t buffer_length = tvb_captured_length(buffer);

    string current_buffer;
    current_buffer.reserve(buffer_length);

    for (int32_t i = 0; i < buffer_length; i++) {
        uint8_t current_byte = tvb_get_guint8(buffer, (gint)i);
        current_buffer.push_back(current_byte);
    }

    lsx_getfield(L, -1, "buffers");
    size_t buffer_count = lsx_objlen(L, -1);
    lsx_pushlstring(L, current_buffer.c_str(), buffer_length);

    lsx_rawseti(L, -2, buffer_count + 1);

    lsx_pop(L, 1); // pop buffers
}
} // namespace shark_x

// shark_x member
namespace shark_x {
bool LuajitEventHandler::is_initialized = false;
set<LuajitLateInitRegistrationInfo*> LuajitEventHandler::late_init_registration_infos;
set<LuajitShutdownRegistrationInfo*> LuajitEventHandler::shutdown_registration_infos;
set<LuajitDissectionStartRegistrationInfo*> LuajitEventHandler::dissection_start_registration_infos;
set<PacketListenerRegistrationInfo*> LuajitEventHandler::packet_listener_registration_infos;

lsx_State* LuajitEventHandler::L = NULL;
} // namespace shark_x

// init functions
namespace shark_x {

static void InitPackagePaths(lsx_State* L) {
    // Set package.path
    lsx_getglobal(L, "package");

    string package_path;
    package_path += get_plugins_pers_dir_with_version();
    package_path += G_DIR_SEPARATOR_S;
    package_path += "?.lua;";

    package_path += get_plugins_pers_dir();
    package_path += G_DIR_SEPARATOR_S;
    package_path += "?.lua;";

    package_path += get_plugins_dir_with_version();
    package_path += G_DIR_SEPARATOR_S;
    package_path += "?.lua;";

    package_path += get_plugins_dir();
    package_path += G_DIR_SEPARATOR_S;
    package_path += "?.lua;";

    package_path += get_progfile_dir();
    package_path += G_DIR_SEPARATOR_S;
    package_path += "?.lua;";

    package_path += ".";
    package_path += G_DIR_SEPARATOR_S;
    package_path += "?.lua;";

    luajit_set_string_attribute("path", package_path.c_str());

    // Set package.cpath

    string package_cpath;
    package_cpath += get_plugins_pers_dir_with_version();
    package_cpath += G_DIR_SEPARATOR_S;
    package_cpath += "?.dll;";

    package_cpath += get_plugins_pers_dir();
    package_cpath += G_DIR_SEPARATOR_S;
    package_cpath += "?.dll;";

    package_cpath += get_plugins_dir_with_version();
    package_cpath += G_DIR_SEPARATOR_S;
    package_cpath += "?.dll;";

    package_cpath += get_plugins_dir();
    package_cpath += G_DIR_SEPARATOR_S;
    package_cpath += "?.dll;";

    package_path += get_progfile_dir();
    package_path += G_DIR_SEPARATOR_S;
    package_path += "?.dll;";

    package_cpath += ".";
    package_cpath += G_DIR_SEPARATOR_S;
    package_cpath += "?.dll;";

    luajit_set_string_attribute("cpath", package_cpath.c_str());

    lsx_pop(L, 1); // pop package
}

void LuajitEventHandler::Init() {
    L = lsxL_newstate();
    lsx_atpanic(L, AtPanic);
    lsxL_openlibs(L);

    lsx_newtable(L);
    lsx_setglobal(L, "shark_x");

    InitModule();

    is_initialized = true;

    ExecuteFiles();
}

void LuajitEventHandler::InitModule() {
    // Init constants
    lsx_pushboolean(L, 1);
    lsx_setglobal(L, "IS_SHARK_X_ENVIRONMENT");

    lsx_getglobal(L, "shark_x");

    luajit_set_bool_attribute("IS_SHARK_X_ENVIRONMENT", true);

    luajit_set_number_attribute("SHARK_X_MAJOR_VERSION", SHARK_X_MAJOR_VERSION);
    luajit_set_number_attribute("SHARK_X_MINOR_VERSION", SHARK_X_MINOR_VERSION);
    luajit_set_number_attribute("SHARK_X_PATCH_VERSION", SHARK_X_PATCH_VERSION);

    int wireshark_major_version = 0;
    int wireshark_minor_version = 0;
    int wireshark_patch_version = 0;
    epan_get_version_number(&wireshark_major_version, &wireshark_minor_version, &wireshark_patch_version);

    luajit_set_number_attribute("WIRESHARK_MAJOR_VERSION", wireshark_major_version);
    luajit_set_number_attribute("WIRESHARK_MINOR_VERSION", wireshark_minor_version);
    luajit_set_number_attribute("WIRESHARK_PATCH_VERSION", wireshark_patch_version);

    luajit_set_string_attribute("PERSONAL_PLUGIN_DIRECTORY_WITH_VERSION", get_plugins_pers_dir_with_version());
    luajit_set_string_attribute("PERSONAL_PLUGIN_DIRECTORY", get_plugins_pers_dir());
    luajit_set_string_attribute("GLOBAL_PLUGIN_DIRECTORY_WITH_VERSION", get_plugins_dir_with_version());
    luajit_set_string_attribute("GLOBAL_PLUGIN_DIRECTORY", get_plugins_dir());
    luajit_set_string_attribute("EXECUTABLE_DIRECTORY", get_progfile_dir());

    luajit_set_number_attribute("PHASE_REGISTRATION", (int)Phase::REGISTRATION);
    luajit_set_number_attribute("PHASE_NORMAL", (int)Phase::NORMAL_OPERATION);
    luajit_set_number_attribute("PHASE_SHUTDOWN", (int)Phase::SHUTDOWN);

    luajit_set_number_attribute("FT_INT8", (int)ftenum::FT_INT8);
    luajit_set_number_attribute("FT_INT16", (int)ftenum::FT_INT16);
    luajit_set_number_attribute("FT_INT32", (int)ftenum::FT_INT32);
    luajit_set_number_attribute("FT_INT64", (int)ftenum::FT_INT64);
    luajit_set_number_attribute("FT_UINT8", (int)ftenum::FT_UINT8);
    luajit_set_number_attribute("FT_UINT16", (int)ftenum::FT_UINT16);
    luajit_set_number_attribute("FT_UINT32", (int)ftenum::FT_UINT32);
    luajit_set_number_attribute("FT_UINT64", (int)ftenum::FT_UINT64);
    luajit_set_number_attribute("FT_FLOAT", (int)ftenum::FT_FLOAT);
    luajit_set_number_attribute("FT_DOUBLE", (int)ftenum::FT_DOUBLE);
    luajit_set_number_attribute("FT_CHAR", (int)ftenum::FT_CHAR);
    luajit_set_number_attribute("FT_BOOL", (int)ftenum::FT_BOOLEAN);
    luajit_set_number_attribute("FT_BYTES", (int)ftenum::FT_BYTES);
    luajit_set_number_attribute("FT_IPV4_ADDRESS", (int)ftenum::FT_IPv4);
    luajit_set_number_attribute("FT_IPV6_ADDRESS", (int)ftenum::FT_IPv6);
    luajit_set_number_attribute("FT_ETHERNET_ADDRESS", (int)ftenum::FT_ETHER);
    luajit_set_number_attribute("FT_GUID", (int)ftenum::FT_GUID);
    luajit_set_number_attribute("FT_EUI64", (int)ftenum::FT_EUI64);
    luajit_set_number_attribute("FT_STRING", (int)ftenum::FT_STRING);
    luajit_set_number_attribute("FT_STRINGZ", (int)ftenum::FT_STRINGZ);
    luajit_set_number_attribute("FT_PROTOCOL", (int)ftenum::FT_PROTOCOL);
    luajit_set_number_attribute("FT_ABSOLUTE_TIME", (int)ftenum::FT_ABSOLUTE_TIME);
    luajit_set_number_attribute("FT_RELATIVE_TIME", (int)ftenum::FT_RELATIVE_TIME);
    luajit_set_number_attribute("FT_FRAME_NUMBER", (int)ftenum::FT_FRAMENUM);
    luajit_set_number_attribute("FT_NONE", (int)ftenum::FT_NONE);

    lsx_pop(L, 1); // pop shark_x

    // Init functions
    lsx_getglobal(L, "shark_x");

    luajit_set_function_attribute("get_current_phase", _get_current_phase);
    luajit_set_function_attribute("report_failure", _report_failure);
    luajit_set_function_attribute("report_warning", _report_warning);
    luajit_set_function_attribute("register_late_init_callback", _register_late_init_callback);
    luajit_set_function_attribute("deregister_late_init_callback", _deregister_late_init_callback);
    luajit_set_function_attribute("register_shutdown_callback", _register_shutdown_callback);
    luajit_set_function_attribute("deregister_shutdown_callback", _deregister_shutdown_callback);
    luajit_set_function_attribute("register_dissection_start_callback", _register_dissection_start_callback);
    luajit_set_function_attribute("deregister_dissection_start_callback", _deregister_dissection_start_callback);
    luajit_set_function_attribute("register_packet_listener", _register_packet_listener);
    luajit_set_function_attribute("deregister_packet_listener", _deregister_packet_listener);
    luajit_set_function_attribute("get_field_descriptions", _get_field_descriptions);

    lsx_pop(L, 1); // pop shark_x

    InitPackagePaths(L);

    // shark_x memeber
    lsx_getregistry(L);
    luajit_set_table_attribute("late_init_registration_infos");
    luajit_set_table_attribute("shutdown_registration_infos");
    luajit_set_table_attribute("dissection_start_registration_infos");
    luajit_set_table_attribute("packet_listener_registration_infos");

    lsx_pop(L, 1); // pop registry
}

void LuajitEventHandler::ExecuteFiles() {
    if (!is_initialized) {
        return;
    }

    vector<string> file_paths = get_file_paths_of_plugin_directories(".lua");

    for (auto&& file_path : file_paths) {
        lsx_pop(L, lsx_gettop(L)); // Clear Stack
        if (LUA_OK != lsxL_dofile(L, file_path.c_str())) {
            report_failure(lsx_tostring(L, -1));
            lsx_pop(L, 1); // pop error message
        }
        lsx_pop(L, lsx_gettop(L)); // Clear Stack
    }
}

int LuajitEventHandler::AtPanic(lsx_State* L) {
    ws_error("LUAJIT ERROR: %s", lsx_tostring(L, -1));
    return 0;
}

} // namespace shark_x

// protected functions
namespace shark_x {

void LuajitEventHandler::OnLateInit() {
    for (auto&& late_init_registration_info : late_init_registration_infos) {
        lsx_pop(L, lsx_gettop(L)); // Clear Stack
        lsx_getregistry(L);
        lsx_getfield(L, -1, "late_init_registration_infos");

        luajit_get_attribute_with_light_user_data_key(late_init_registration_info);
        lsx_getfield(L, -1, "late_init_callback");
        if (!lsx_isfunction(L, -1)) {
            report_failure("late_init_callback is not a function");
            lsx_pop(L, 2); // pop late_init_callback, late_init_registration_info
            continue;
        }

        lsx_getfield(L, -2, "user_data");

        if (LUA_OK != lsx_pcall(L, 1, LUA_MULTRET, 0)) {
            report_failure(lsx_tostring(L, -1));
            lsx_pop(L, 1); // pop error message
        }

        lsx_pop(L, lsx_gettop(L)); // Clear Stack
    }
}

void LuajitEventHandler::OnShutdown() {
    if (!is_initialized) {
        return;
    }

    lsx_pop(L, lsx_gettop(L)); // Clear Stack

    // clear late init registrations
    for (auto&& late_init_registration_info : late_init_registration_infos) {
        lsx_pop(L, lsx_gettop(L)); // Clear Stack
        lsx_getregistry(L);
        lsx_getfield(L, -1, "late_init_registration_infos");

        luajit_set_nil_attribute_with_light_user_data_key(late_init_registration_info);

        lsx_pop(L, lsx_gettop(L)); // Clear Stack

        delete late_init_registration_info;
    }
    late_init_registration_infos.clear();

    // clear dissection start registrations
    for (auto&& dissection_start_registration_info : dissection_start_registration_infos) {
        lsx_pop(L, lsx_gettop(L)); // Clear Stack
        lsx_getregistry(L);
        lsx_getfield(L, -1, "dissection_start_registration_infos");

        luajit_set_nil_attribute_with_light_user_data_key(dissection_start_registration_info);

        lsx_pop(L, lsx_gettop(L)); // Clear Stack

        delete dissection_start_registration_info;
    }
    dissection_start_registration_infos.clear();

    // clear packet listener registrations (this will trigger a packet listener
    // finish events)
    vector<PacketListenerRegistrationInfo*> temp_packet_listener_registration_infos;
    for (auto&& packet_listener_registration_info : packet_listener_registration_infos) {
        temp_packet_listener_registration_infos.push_back(packet_listener_registration_info);
    }
    for (auto&& packet_listener_registration_info : temp_packet_listener_registration_infos) {
        EventHandler::DeregisterPacketListener(packet_listener_registration_info, packet_listener_registration_infos);

        lsx_pop(L, lsx_gettop(L)); // Clear Stack
        lsx_getregistry(L);
        lsx_getfield(L, -1, "packet_listener_registration_infos");

        luajit_set_nil_attribute_with_light_user_data_key(packet_listener_registration_info);

        lsx_pop(L, lsx_gettop(L)); // Clear Stack
    }
    temp_packet_listener_registration_infos.clear();

    for (auto&& shutdown_registration_info : shutdown_registration_infos) {
        lsx_pop(L, lsx_gettop(L)); // Clear Stack
        lsx_getregistry(L);
        lsx_getfield(L, -1, "shutdown_registration_infos");

        luajit_get_attribute_with_light_user_data_key(shutdown_registration_info);
        lsx_getfield(L, -1, "shutdown_callback");
        if (!lsx_isfunction(L, -1)) {
            report_failure("shutdown_callback is not a function");
            lsx_pop(L, 2); // pop shutdown_callback, shutdown_registration_info
            continue;
        }

        lsx_getfield(L, -2, "user_data");

        int stack_count_before_call = lsx_gettop(L) - 2;
        if (LUA_OK != lsx_pcall(L, 1, LUA_MULTRET, 0)) {
            report_failure(lsx_tostring(L, -1));
            lsx_pop(L, 1); // pop error message
        }

        lsx_pop(L, lsx_gettop(L)); // Clear Stack
    }

    // clear shutdown registrations
    for (auto&& shutdown_registration_info : shutdown_registration_infos) {
        lsx_pop(L, lsx_gettop(L)); // Clear Stack

        lsx_getregistry(L);
        lsx_getfield(L, -1, "shutdown_registration_infos");

        luajit_set_nil_attribute_with_light_user_data_key(shutdown_registration_info);

        lsx_pop(L, lsx_gettop(L)); // Clear Stack

        delete shutdown_registration_info;
    }
    shutdown_registration_infos.clear();

    lsx_close(L);
    L = NULL;

    is_initialized = false;
}

void LuajitEventHandler::OnDissectionStart() {
    for (auto&& dissection_start_registration_info : dissection_start_registration_infos) {
        lsx_pop(L, lsx_gettop(L)); // Clear Stack
        lsx_getregistry(L);
        lsx_getfield(L, -1, "dissection_start_registration_infos");

        luajit_get_attribute_with_light_user_data_key(dissection_start_registration_info);
        lsx_getfield(L, -1, "dissection_start_callback");
        if (!lsx_isfunction(L, -1)) {
            report_failure("dissection_start_callback is not a function");
            lsx_pop(L, 2); // pop dissection_start_callback, dissection_start_registration_info
            continue;
        }

        lsx_getfield(L, -2, "user_data");

        int stack_count_before_call = lsx_gettop(L) - 2;
        if (LUA_OK != lsx_pcall(L, 1, LUA_MULTRET, 0)) {
            report_failure(lsx_tostring(L, -1));
            lsx_pop(L, 1); // pop error message
        }

        lsx_pop(L, lsx_gettop(L)); // Clear Stack
    }
}

tap_packet_status LuajitEventHandler::OnPacketListenerPacket(void* user_data, packet_info* packet_info, epan_dissect_t* epan_dissect, const void* _data) {

    lsx_pop(L, lsx_gettop(L)); // Clear Stack
    lsx_getregistry(L);
    lsx_getfield(L, -1, "packet_listener_registration_infos");

    luajit_get_attribute_with_light_user_data_key(user_data);
    lsx_getfield(L, -1, "packet_listener_packet_callback");
    if (!lsx_isfunction(L, -1)) {
        report_failure("packet_listener_packet_callback is not a function");
        lsx_pop(L, lsx_gettop(L)); // Clear Stack
        return tap_packet_status::TAP_PACKET_DONT_REDRAW;
    }

    _create_packet_data(packet_info, epan_dissect->tree, epan_dissect->tvb);

    lsx_getfield(L, -3, "user_data");

    if (LUA_OK != lsx_pcall(L, 2, LUA_MULTRET, 0)) {
        report_failure(lsx_tostring(L, -1));
        lsx_pop(L, 1); // pop error message
    }

    lsx_pop(L, lsx_gettop(L)); // Clear Stack

    return tap_packet_status::TAP_PACKET_DONT_REDRAW;
}

void LuajitEventHandler::OnPacketListenerFinish(void* user_data) {

    lsx_pop(L, lsx_gettop(L)); // Clear Stack
    lsx_getregistry(L);
    lsx_getfield(L, -1, "packet_listener_registration_infos");

    luajit_get_attribute_with_light_user_data_key(user_data);
    lsx_getfield(L, -1, "packet_listener_finish_callback");
    if (!lsx_isfunction(L, -1)) { // packet_listener_finish_callback is optional
        return;
    }

    lsx_getfield(L, -2, "user_data");

    if (LUA_OK != lsx_pcall(L, 1, LUA_MULTRET, 0)) {
        report_failure(lsx_tostring(L, -1));
        lsx_pop(L, 1); // pop error message
    }

    lsx_pop(L, lsx_gettop(L)); // Clear Stack
}

void LuajitEventHandler::OnPacketListenerCleanup(void* user_data) {

    lsx_pop(L, lsx_gettop(L)); // Clear Stack
    lsx_getregistry(L);
    lsx_getfield(L, -1, "packet_listener_registration_infos");

    luajit_get_attribute_with_light_user_data_key(user_data);
    lsx_getfield(L, -1, "packet_listener_cleanup_callback");
    if (!lsx_isfunction(L, -1)) { // packet_listener_cleanup_callback is optional
        return;
    }

    lsx_getfield(L, -2, "user_data");

    if (LUA_OK != lsx_pcall(L, 1, LUA_MULTRET, 0)) {
        report_failure(lsx_tostring(L, -1));
        lsx_pop(L, 1); // pop error message
    }

    lsx_pop(L, lsx_gettop(L)); // Clear Stack
}

} // namespace shark_x

// public functions
namespace shark_x {

void LuajitEventHandler::Register() {
    Init();

    register_final_registration_routine(OnLateInit);

    register_shutdown_routine(OnShutdown);

    register_init_routine(OnDissectionStart);
}

void LuajitEventHandler::RegisterHandoff() {}
} // namespace shark_x

#endif // SHARK_X_LUAJIT_ENABLED