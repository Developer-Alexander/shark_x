/*
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR
A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SPDX-License-Identifier: MIT
*/

#ifdef SHARK_X_NODEJS_ENABLED

// shark_x includes
#include "shark_x_nodejs_event_handler.hpp"
#include "shark_x_event_handler.hpp"
#include "shark_x_nodejs_types.hpp"
#include "shark_x_nodejs_util.hpp"

// wireshark includes
#include "epan/epan.h"
#include "epan/epan_dissect.h"
#include "epan/packet.h"
#include "epan/proto.h"
#include "wsutil/file_util.h"
#include "wsutil/filesystem.h"
#include "wsutil/report_message.h"
#include "wsutil/wslog.h"

// nodejs includes
#define BUILDING_NODE_EXTENSION
#include "node.h"
#include "uv.h"

using namespace node;
using namespace v8;

// std includes
#include <charconv>
#include <string>
#include <vector>

using namespace std;

// api implementation
namespace shark_x {

void NodejsEventHandler::_get_current_phase(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    HandleScope scope(isolate);

    if (args.Length() != 0) {
        isolate->ThrowException(Exception::Error(nodejs_new_string(isolate, "number of arguments is not 0")));
        return;
    }

    Local<Number> current_phase = Number::New(isolate, (int)EventHandler::current_phase);
    args.GetReturnValue().Set(current_phase);
    return;
}

void NodejsEventHandler::_report_failure(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    HandleScope scope(isolate);

    if (args.Length() != 1) {
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "number of arguments is not 1")));
        return;
    }

    if (!args[0]->IsString()) {
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "message is not a string")));
        return;
    }

    String::Utf8Value message(isolate, args[0]);

    report_failure("%s", *message);
}

void NodejsEventHandler::_report_warning(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    HandleScope scope(isolate);

    if (args.Length() != 1) {
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "number of arguments is not 1")));
        return;
    }

    if (!args[0]->IsString()) {
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "message is not a string")));
        return;
    }

    String::Utf8Value message(isolate, args[0]);

    report_warning("%s", *message);
}

void NodejsEventHandler::_register_late_init_callback(const FunctionCallbackInfo<Value>& args) {
    NodejsLateInitRegistrationInfo* late_init_registration_info = new NodejsLateInitRegistrationInfo();

    Isolate* isolate = args.GetIsolate();
    HandleScope scope(isolate);

    if (EventHandler::current_phase == Phase::SHUTDOWN) {
        delete late_init_registration_info;
        isolate->ThrowException(Exception::Error(nodejs_new_string(isolate, "register_late_init_callback is not allowed during shutdown phase")));
        return;
    }

    if (!(args.Length() >= 1 && args.Length() <= 2)) {
        delete late_init_registration_info;
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "number of arguments is not between 1 and 2")));
        return;
    }

    if (!args[0]->IsFunction()) {
        delete late_init_registration_info;
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "late_init_callback is not a function")));
        return;
    }

    Local<Function> late_init_callback_v8 = args[0].As<Function>();
    Local<Value> user_data_v8 = args.Length() == 2 ? args[1] : Undefined(isolate).As<Value>();

    late_init_registration_info->late_init_callback_v8.Reset(isolate, late_init_callback_v8);
    late_init_registration_info->user_data_v8.Reset(isolate, user_data_v8);

    late_init_registration_infos.insert(late_init_registration_info);

    string late_init_callback_id = to_string((uint64_t)late_init_registration_info);
    Local<String> late_init_callback_id_v8 = nodejs_new_string(isolate, late_init_callback_id.c_str());
    args.GetReturnValue().Set(late_init_callback_id_v8);
    return;
}

void NodejsEventHandler::_deregister_late_init_callback(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    HandleScope scope(isolate);

    if (EventHandler::current_phase == Phase::SHUTDOWN) {
        isolate->ThrowException(Exception::Error(nodejs_new_string(isolate, "deregister_late_init_callback is not allowed during shutdown phase")));
        return;
    }

    if (!(args.Length() != 1)) {
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "number of arguments is not 1")));
        return;
    }

    if (!args[0]->IsString()) {
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "late_init_callback_id is not a string")));
        return;
    }

    String::Utf8Value late_init_callback_id = String::Utf8Value(isolate, args[0]);

    uint64_t late_init_callback_id_uint = 0;
    from_chars_result conversion_result = from_chars(*late_init_callback_id, *late_init_callback_id + strlen(*late_init_callback_id), late_init_callback_id_uint);
    if (conversion_result.ec != errc()) {
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "late_init_callback_id is not valid")));
        return;
    }
    NodejsLateInitRegistrationInfo* late_init_registration_info = (NodejsLateInitRegistrationInfo*)late_init_callback_id_uint;
    set<NodejsLateInitRegistrationInfo*>::iterator late_init_registration_info_found = late_init_registration_infos.find(late_init_registration_info);

    Local<Value> result_v8 = False(isolate);
    if (late_init_registration_info_found != late_init_registration_infos.end()) {
        late_init_registration_infos.erase(late_init_registration_info);
        delete late_init_registration_info;
        result_v8 = True(isolate);
    }

    args.GetReturnValue().Set(result_v8);
    return;
}

void NodejsEventHandler::_register_shutdown_callback(const FunctionCallbackInfo<Value>& args) {
    NodejsShutdownRegistrationInfo* shutdown_registration_info = new NodejsShutdownRegistrationInfo();

    Isolate* isolate = args.GetIsolate();
    HandleScope scope(isolate);

    if (EventHandler::current_phase == Phase::SHUTDOWN) {
        delete shutdown_registration_info;
        isolate->ThrowException(Exception::Error(nodejs_new_string(isolate, "register_shutdown_callback is not allowed during shutdown phase")));
        return;
    }

    if (!(args.Length() >= 1 && args.Length() <= 2)) {
        delete shutdown_registration_info;
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "number of arguments is not between 1 and 2")));
        return;
    }

    if (!args[0]->IsFunction()) {
        delete shutdown_registration_info;
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "shutdown_callback is not a function")));
        return;
    }

    Local<Function> shutdown_callback_v8 = args[0].As<Function>();
    Local<Value> user_data_v8 = args.Length() == 2 ? args[1] : Undefined(isolate).As<Value>();

    shutdown_registration_info->shutdown_callback_v8.Reset(isolate, shutdown_callback_v8);
    shutdown_registration_info->user_data_v8.Reset(isolate, user_data_v8);

    shutdown_registration_infos.insert(shutdown_registration_info);

    string shutdown_callback_id = to_string((uint64_t)shutdown_registration_info);
    Local<String> shutdown_callback_id_v8 = nodejs_new_string(isolate, shutdown_callback_id.c_str());
    args.GetReturnValue().Set(shutdown_callback_id_v8);
    return;
}

void NodejsEventHandler::_deregister_shutdown_callback(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    HandleScope scope(isolate);

    if (EventHandler::current_phase == Phase::SHUTDOWN) {
        isolate->ThrowException(Exception::Error(nodejs_new_string(isolate, "deregister_shutdown_callback is not allowed during shutdown phase")));
        return;
    }

    if (!(args.Length() != 1)) {
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "number of arguments is not 1")));
        return;
    }

    if (!args[0]->IsString()) {
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "shutdown_callback_id is not a string")));
        return;
    }

    String::Utf8Value shutdown_callback_id = String::Utf8Value(isolate, args[0]);

    uint64_t shutdown_callback_id_uint = 0;
    from_chars_result conversion_result = from_chars(*shutdown_callback_id, *shutdown_callback_id + strlen(*shutdown_callback_id), shutdown_callback_id_uint);
    if (conversion_result.ec != errc()) {
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "shutdown_callback_id is not valid")));
        return;
    }
    NodejsShutdownRegistrationInfo* shutdown_registration_info = (NodejsShutdownRegistrationInfo*)shutdown_callback_id_uint;
    set<NodejsShutdownRegistrationInfo*>::iterator shutdown_registration_info_found = shutdown_registration_infos.find(shutdown_registration_info);

    Local<Value> result_v8 = False(isolate);
    if (shutdown_registration_info_found != shutdown_registration_infos.end()) {
        shutdown_registration_infos.erase(shutdown_registration_info);
        delete shutdown_registration_info;
        result_v8 = True(isolate);
    }

    args.GetReturnValue().Set(result_v8);
    return;
}

void NodejsEventHandler::_register_dissection_start_callback(const FunctionCallbackInfo<Value>& args) {
    NodejsDissectionStartRegistrationInfo* dissection_start_registration_info = new NodejsDissectionStartRegistrationInfo();

    Isolate* isolate = args.GetIsolate();
    HandleScope scope(isolate);

    if (EventHandler::current_phase == Phase::SHUTDOWN) {
        delete dissection_start_registration_info;
        isolate->ThrowException(Exception::Error(nodejs_new_string(isolate, "register_dissection_start_callback is not allowed during shutdown phase")));
        return;
    }

    if (!(args.Length() >= 1 && args.Length() <= 2)) {
        delete dissection_start_registration_info;
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "number of arguments is not between 1 and 2")));
        return;
    }

    if (!args[0]->IsFunction()) {
        delete dissection_start_registration_info;
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "dissection_start_callback is not a function")));
        return;
    }

    Local<Function> dissection_start_callback_v8 = args[0].As<Function>();
    Local<Value> user_data_v8 = args.Length() == 2 ? args[1] : Undefined(isolate).As<Value>();

    dissection_start_registration_info->dissection_start_callback_v8.Reset(isolate, dissection_start_callback_v8);
    dissection_start_registration_info->user_data_v8.Reset(isolate, user_data_v8);

    dissection_start_registration_infos.insert(dissection_start_registration_info);

    string dissection_start_callback_id = to_string((uint64_t)dissection_start_registration_info);
    Local<String> dissection_start_callback_id_v8 = nodejs_new_string(isolate, dissection_start_callback_id.c_str());
    args.GetReturnValue().Set(dissection_start_callback_id_v8);
    return;
}

void NodejsEventHandler::_deregister_dissection_start_callback(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    HandleScope scope(isolate);

    if (EventHandler::current_phase == Phase::SHUTDOWN) {
        isolate->ThrowException(Exception::Error(nodejs_new_string(isolate, "deregister_dissection_start_callback is not allowed during shutdown phase")));
        return;
    }

    if (!(args.Length() != 1)) {
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "number of arguments is not 1")));
        return;
    }

    if (!args[0]->IsString()) {
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "dissection_start_callback_id is not a string")));
        return;
    }

    String::Utf8Value dissection_start_callback_id = String::Utf8Value(isolate, args[0]);

    uint64_t dissection_start_callback_id_uint = 0;
    from_chars_result conversion_result =
        from_chars(*dissection_start_callback_id, *dissection_start_callback_id + strlen(*dissection_start_callback_id), dissection_start_callback_id_uint);
    if (conversion_result.ec != errc()) {
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "dissection_start_callback_id is not valid")));
        return;
    }
    NodejsDissectionStartRegistrationInfo* dissection_start_registration_info = (NodejsDissectionStartRegistrationInfo*)dissection_start_callback_id_uint;
    set<NodejsDissectionStartRegistrationInfo*>::iterator dissection_start_registration_info_found = dissection_start_registration_infos.find(dissection_start_registration_info);

    Local<Value> result_v8 = False(isolate);
    if (dissection_start_registration_info_found != dissection_start_registration_infos.end()) {
        dissection_start_registration_infos.erase(dissection_start_registration_info);
        delete dissection_start_registration_info;
        result_v8 = True(isolate);
    }

    args.GetReturnValue().Set(result_v8);
    return;
}

void NodejsEventHandler::_register_packet_listener(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    HandleScope scope(isolate);
    Local<Context> context = isolate->GetCurrentContext();

    PacketListenerRegistrationInfo* packet_listener_registration_info = new PacketListenerRegistrationInfo();
    NodejsPacketListenerRegistrationInfo* nodejs_packet_listener_registration_info = new NodejsPacketListenerRegistrationInfo();
    packet_listener_registration_info->user_data = nodejs_packet_listener_registration_info;
    packet_listener_registration_info->packet_listener_packet_callback = OnPacketListenerPacket;
    packet_listener_registration_info->packet_listener_finish_callback = OnPacketListenerFinish;
    packet_listener_registration_info->packet_listener_cleanup_callback = OnPacketListenerCleanup;

    if (EventHandler::current_phase != Phase::NORMAL_OPERATION) {
        delete nodejs_packet_listener_registration_info;
        delete packet_listener_registration_info;
        isolate->ThrowException(Exception::Error(nodejs_new_string(isolate, "register_packet_listener is only allowed during normal operation phase")));

        return;
    }

    if (args.Length() != 8) {
        delete nodejs_packet_listener_registration_info;
        delete packet_listener_registration_info;
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "number of arguments is not 8")));

        return;
    }

    if (!args[0]->IsString()) {
        delete nodejs_packet_listener_registration_info;
        delete packet_listener_registration_info;
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "tap_name is not a string")));
        return;
    }

    String::Utf8Value tap_name = String::Utf8Value(isolate, args[0]);
    packet_listener_registration_info->tap_name = *tap_name;

    if (!args[1]->IsString()) {
        delete nodejs_packet_listener_registration_info;
        delete packet_listener_registration_info;
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "filter_string is not a string")));

        return;
    }

    String::Utf8Value filter_string = String::Utf8Value(isolate, args[1]);
    packet_listener_registration_info->filter_string = *filter_string;

    if (!args[2]->IsArray()) {
        delete nodejs_packet_listener_registration_info;
        delete packet_listener_registration_info;
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "wanted_field_names is not an array")));

        return;
    }

    Local<Array> wanded_field_names_list_v8 = args[2].As<Array>();
    uint32_t wanded_field_names_list_count = wanded_field_names_list_v8->Length();
    for (uint32_t i = 0; i < wanded_field_names_list_count; i++) {
        Local<Value> wanted_field_name_v8 = wanded_field_names_list_v8->Get(context, i).ToLocalChecked();
        if (!wanted_field_name_v8->IsString()) {
            delete nodejs_packet_listener_registration_info;
            delete packet_listener_registration_info;
            isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "element in wanted_field_names is not a string")));

            return;
        }
        String::Utf8Value wanted_field_name = String::Utf8Value(isolate, wanted_field_name_v8);
        packet_listener_registration_info->wanted_field_names.insert(*wanted_field_name);
    }

    if (!args[3]->IsBoolean()) {
        delete nodejs_packet_listener_registration_info;
        delete packet_listener_registration_info;
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "all_fields_requested is not a bool")));

        return;
    }
    Local<Boolean> all_fields_requested_v8 = args[3].As<Boolean>();
    packet_listener_registration_info->all_fields_requested = all_fields_requested_v8->IsTrue() ? true : false;

    if (!args[4]->IsFunction()) {
        delete nodejs_packet_listener_registration_info;
        delete packet_listener_registration_info;
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "packet_listener_packet_callback is not a function")));

        return;
    }

    Local<Function> packet_listener_packet_callback_v8 = args[4].As<Function>();
    nodejs_packet_listener_registration_info->packet_listener_packet_callback_v8.Reset(isolate, packet_listener_packet_callback_v8);

    if (!(args[5]->IsFunction() || args[5]->IsUndefined())) {
        delete nodejs_packet_listener_registration_info;
        delete packet_listener_registration_info;
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "packet_listener_finish_callback is not a function or undefined")));

        return;
    }

    Local<Value> packet_listener_finish_callback_v8 = args[5];
    nodejs_packet_listener_registration_info->packet_listener_finish_callback_v8.Reset(isolate, packet_listener_finish_callback_v8);

    if (!(args[6]->IsFunction() || args[6]->IsUndefined())) {
        delete nodejs_packet_listener_registration_info;
        delete packet_listener_registration_info;
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "packet_listener_cleanup_callback is not a function or undefined")));

        return;
    }

    Local<Value> packet_listener_cleanup_callback_v8 = args[6];
    nodejs_packet_listener_registration_info->packet_listener_cleanup_callback_v8.Reset(isolate, packet_listener_cleanup_callback_v8);

    Local<Value> user_data_v8 = args[7];
    nodejs_packet_listener_registration_info->user_data_v8.Reset(isolate, user_data_v8);

    string error_message;
    bool registration_result = EventHandler::RegisterPacketListener(packet_listener_registration_info, packet_listener_registration_infos, error_message);
    if (!registration_result) {
        delete nodejs_packet_listener_registration_info;
        delete packet_listener_registration_info;
        isolate->ThrowException(Exception::Error(nodejs_new_string(isolate, error_message.c_str())));

        return;
    }

    string packet_listener_id = to_string((uint64_t)packet_listener_registration_info);
    Local<String> packet_listener_id_v8 = nodejs_new_string(isolate, packet_listener_id.c_str());

    args.GetReturnValue().Set(packet_listener_id_v8);
    return;
}

void NodejsEventHandler::_deregister_packet_listener(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    HandleScope scope(isolate);

    if (EventHandler::current_phase != Phase::NORMAL_OPERATION) {
        isolate->ThrowException(Exception::Error(nodejs_new_string(isolate, "deregister_packet_listener is only allowed during normal operation phase")));
        return;
    }

    if (!(args.Length() != 1)) {
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "number of arguments is not 1")));
        return;
    }

    if (!args[0]->IsString()) {
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "packet_listener_id is not a string")));
        return;
    }

    String::Utf8Value packet_listener_id = String::Utf8Value(isolate, args[0]);

    uint64_t packet_listener_id_uint = 0;
    from_chars_result conversion_result = from_chars(*packet_listener_id, *packet_listener_id + strlen(*packet_listener_id), packet_listener_id_uint);
    if (conversion_result.ec != errc()) {
        isolate->ThrowException(Exception::TypeError(nodejs_new_string(isolate, "packet_listener_id is not valid")));
        return;
    }

    PacketListenerRegistrationInfo* packet_listener_registration_info = (PacketListenerRegistrationInfo*)packet_listener_id_uint;
    bool deregister_result = EventHandler::DeregisterPacketListener(packet_listener_registration_info, packet_listener_registration_infos);

    Local<Boolean> result_v8 = deregister_result ? True(isolate) : True(isolate);
    args.GetReturnValue().Set(result_v8);
    return;
}

void NodejsEventHandler::_get_field_descriptions(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    HandleScope scope(isolate);
    Local<Context> context = environment_setup->context();

    if (EventHandler::current_phase != Phase::NORMAL_OPERATION) {
        isolate->ThrowException(Exception::Error(nodejs_new_string(isolate, "get_field_descriptions is only allowed during normal operation phase")));
        return;
    }

    void* protocol_cookie = NULL;
    void* field_cookie = NULL;
    header_field_info* current_header_field_info = NULL;

    int32_t field_count = 0;

    for (int protocol_id = proto_get_first_protocol(&protocol_cookie); protocol_id != -1; protocol_id = proto_get_next_protocol(&protocol_cookie)) {
        for (current_header_field_info = proto_get_first_protocol_field(protocol_id, &field_cookie); current_header_field_info != NULL;
             current_header_field_info = proto_get_next_protocol_field(protocol_id, &field_cookie)) {
            // ignore duplicate names
            if (current_header_field_info->same_name_prev_id != -1) {
                continue;
            }
            field_count++;
        }
    }

    Local<Array> field_descriptions_v8 = Array::New(isolate, field_count);

    protocol_cookie = NULL;
    field_cookie = NULL;
    current_header_field_info = NULL;
    int32_t field_index = 0;

    for (int protocol_id = proto_get_first_protocol(&protocol_cookie); protocol_id != -1; protocol_id = proto_get_next_protocol(&protocol_cookie)) {
        for (current_header_field_info = proto_get_first_protocol_field(protocol_id, &field_cookie); current_header_field_info != NULL;
             current_header_field_info = proto_get_next_protocol_field(protocol_id, &field_cookie)) {
            // ignore duplicate names
            if (current_header_field_info->same_name_prev_id != -1) {
                continue;
            }

            Local<Object> field_description_v8 = _create_field_instance(current_header_field_info, NULL);

            field_index++;
        }
    }

    args.GetReturnValue().Set(field_descriptions_v8);
    return;
}

class CreatePacketDataProcessNodeData {
  public:
    vector<tvbuff_t*>* buffers = NULL;
    Local<Object> parent_protocol_tree_node_v8;
};

Local<Object> NodejsEventHandler::_create_packet_data(packet_info* packet_info, proto_tree* tree, tvbuff_t* packet_buffer) {
    ws_assert(packet_info);
    ws_assert(packet_info->fd);
    ws_assert(tree);

    vector<tvbuff_t*> buffers;
    buffers.push_back(packet_buffer);

    Isolate* isolate = environment_setup->isolate();
    EscapableHandleScope scope(isolate);
    Local<Context> context = isolate->GetCurrentContext();

    Local<Object> packet_data_v8 = Object::New(isolate);

    nodejs_set_number_attribute(isolate, context, packet_data_v8, "packet_buffer_index", 0);

    Local<Object> packet_info_v8 = Object::New(isolate);
    packet_data_v8->Set(context, nodejs_new_string(isolate, "packet_info"), packet_info_v8);

    nodejs_set_number_attribute(isolate, context, packet_info_v8, "number", packet_info->num);
    nodejs_set_number_attribute(isolate, context, packet_info_v8, "absolute_timestamp_seconds", packet_info->abs_ts.secs);
    nodejs_set_number_attribute(isolate, context, packet_info_v8, "absolute_timestamp_nanoseconds", packet_info->abs_ts.nsecs);
    nodejs_set_number_attribute(isolate, context, packet_info_v8, "relative_timestamp_seconds", packet_info->rel_ts.secs);
    nodejs_set_number_attribute(isolate, context, packet_info_v8, "relative_timestamp_nanoseconds", packet_info->rel_ts.nsecs);
    nodejs_set_number_attribute(isolate, context, packet_info_v8, "length", packet_info->fd->pkt_len);
    nodejs_set_bool_attribute(isolate, context, packet_info_v8, "visited", packet_info->fd->visited);
    nodejs_set_bool_attribute(isolate, context, packet_info_v8, "visible", (tree != NULL && tree->tree_data->visible != FALSE));

    const char* protocol_column_text = packet_info->cinfo != NULL ? col_get_text(packet_info->cinfo, COL_PROTOCOL) : NULL;
    protocol_column_text = protocol_column_text != NULL ? protocol_column_text : "";
    nodejs_set_string_attribute(isolate, context, packet_info_v8, "protocol_column", protocol_column_text);

    const char* info_column_text = packet_info->cinfo != NULL ? col_get_text(packet_info->cinfo, COL_INFO) : NULL;
    info_column_text = info_column_text != NULL ? info_column_text : "";
    nodejs_set_string_attribute(isolate, context, packet_info_v8, "info_column", info_column_text);

    Local<Object> protocol_tree_v8 = Object::New(isolate);
    packet_data_v8->Set(context, nodejs_new_string(isolate, "protocol_tree"), protocol_tree_v8);

    Local<Array> protocol_tree_children_v8 = Array::New(isolate);
    protocol_tree_v8->Set(context, nodejs_new_string(isolate, "children"), protocol_tree_children_v8);

    Local<Array> buffers_v8 = Array::New(isolate);
    packet_data_v8->Set(context, nodejs_new_string(isolate, "buffers"), buffers_v8);

    CreatePacketDataProcessNodeData create_packet_data_process_node_data = CreatePacketDataProcessNodeData();
    create_packet_data_process_node_data.buffers = &buffers;
    create_packet_data_process_node_data.parent_protocol_tree_node_v8 = protocol_tree_v8;

    proto_tree_children_foreach(tree, _create_packet_data_process_node, &create_packet_data_process_node_data);

    for (auto&& current_buffer : buffers) {
        _add_buffer_to_packet_data(current_buffer, packet_data_v8);
    }

    return scope.Escape(packet_data_v8);
}

void NodejsEventHandler::_create_packet_data_process_node(proto_node* node, gpointer data) {
    ws_assert(node);
    ws_assert(node->finfo);
    ws_assert(node->finfo->hfinfo);
    ws_assert(data);

    CreatePacketDataProcessNodeData* create_packet_data_process_node_data = (CreatePacketDataProcessNodeData*)data;
    header_field_info* current_header_field_info = node->finfo->hfinfo;

    tvbuff_t* current_buffer = node->finfo->ds_tvb;
    int64_t buffer_index = -1;

    if (current_buffer != NULL) {
        vector<tvbuff_t*>* buffers = create_packet_data_process_node_data->buffers;
        for (size_t i = 0; i < buffers->size(); i++) {
            if (buffers->at(i) == current_buffer) {
                buffer_index = (int64_t)i;
                break;
            }
        }
        if (buffer_index == -1) {
            buffer_index = (int64_t)buffers->size();
            buffers->push_back(current_buffer);
        }
    }

    int64_t start_position = -1;
    int64_t length = 0;

    if (current_buffer != NULL) {
        start_position = node->finfo->start;
        length = node->finfo->length;
    }

    Isolate* isolate = environment_setup->isolate();
    HandleScope scope(isolate);
    Local<Context> context = isolate->GetCurrentContext();

    Local<Object> parent_protocol_tree_node_v8 = create_packet_data_process_node_data->parent_protocol_tree_node_v8;
    Local<Array> children_v8 = nodejs_get_attribute(isolate, context, parent_protocol_tree_node_v8, "children").As<Array>();

    Local<Object> protocol_tree_node_v8 = Object::New(isolate);
    children_v8->Set(context, children_v8->Length(), protocol_tree_node_v8);

    Local<Object> field_instance_v8 = _create_field_instance(current_header_field_info, node->finfo);
    protocol_tree_node_v8->Set(context, nodejs_new_string(isolate, "field_instance"), field_instance_v8);

    nodejs_set_number_attribute(isolate, context, field_instance_v8, "start_position", start_position);
    nodejs_set_number_attribute(isolate, context, field_instance_v8, "length", length);
    nodejs_set_number_attribute(isolate, context, field_instance_v8, "buffer_index", buffer_index);
    nodejs_set_bool_attribute(isolate, context, field_instance_v8, "hidden", (node->finfo->flags & FI_HIDDEN) > 0);
    nodejs_set_bool_attribute(isolate, context, field_instance_v8, "generated", (node->finfo->flags & FI_GENERATED) > 0);
    nodejs_set_bool_attribute(isolate, context, field_instance_v8, "big_endian", (node->finfo->flags & FI_BIG_ENDIAN) > 0);

    const char* representation = node->finfo->rep->representation != NULL ? node->finfo->rep->representation : "";
    nodejs_set_string_attribute(isolate, context, field_instance_v8, "representation", representation);

    Local<Array> protocol_tree_children_v8 = Array::New(isolate);
    protocol_tree_node_v8->Set(context, nodejs_new_string(isolate, "children"), protocol_tree_children_v8);

    create_packet_data_process_node_data->parent_protocol_tree_node_v8 = protocol_tree_node_v8;
    proto_tree_children_foreach(node, _create_packet_data_process_node, create_packet_data_process_node_data);
    create_packet_data_process_node_data->parent_protocol_tree_node_v8 = parent_protocol_tree_node_v8;

    return;
}

Local<Object> NodejsEventHandler::_create_field_instance(header_field_info* current_header_field_info, field_info* current_field_info) {
    ws_assert(current_header_field_info);

    Isolate* isolate = environment_setup->isolate();
    EscapableHandleScope scope(isolate);
    Local<Context> context = isolate->GetCurrentContext();

    Local<Object> field_instance_v8 = Object::New(isolate);
    nodejs_set_string_attribute(isolate, context, field_instance_v8, "name", current_header_field_info->abbrev);
    nodejs_set_string_attribute(isolate, context, field_instance_v8, "display_name", current_header_field_info->name);
    nodejs_set_number_attribute(isolate, context, field_instance_v8, "field_id", current_header_field_info->id);

    ftenum field_type = current_header_field_info->type;
    if (!(field_type == ftenum::FT_INT8 || field_type == ftenum::FT_INT16 || field_type == ftenum::FT_INT32 || field_type == ftenum::FT_INT64 || field_type == ftenum::FT_UINT8 ||
          field_type == ftenum::FT_UINT16 || field_type == ftenum::FT_UINT32 || field_type == ftenum::FT_UINT64 || field_type == ftenum::FT_FLOAT ||
          field_type == ftenum::FT_DOUBLE || field_type == ftenum::FT_CHAR || field_type == ftenum::FT_BOOLEAN || field_type == ftenum::FT_BYTES || field_type == ftenum::FT_IPv4 ||
          field_type == ftenum::FT_IPv6 || field_type == ftenum::FT_ETHER || field_type == ftenum::FT_GUID || field_type == ftenum::FT_EUI64 || field_type == ftenum::FT_STRING ||
          field_type == ftenum::FT_STRINGZ || field_type == ftenum::FT_PROTOCOL || field_type == ftenum::FT_ABSOLUTE_TIME || field_type == ftenum::FT_RELATIVE_TIME ||
          field_type == ftenum::FT_FRAMENUM)) {
        field_type = ftenum::FT_NONE;
    }

    nodejs_set_number_attribute(isolate, context, field_instance_v8, "field_type", (int)field_type);

    if (current_field_info != NULL) {
        nodejs_set_undefined_attribute(isolate, context, field_instance_v8, "int_value");
        nodejs_set_undefined_attribute(isolate, context, field_instance_v8, "uint_value");
        nodejs_set_undefined_attribute(isolate, context, field_instance_v8, "double_value");
        nodejs_set_undefined_attribute(isolate, context, field_instance_v8, "string_value");
        nodejs_set_undefined_attribute(isolate, context, field_instance_v8, "bytes_value");

        if (field_type == ftenum::FT_INT8 || field_type == ftenum::FT_INT16 || field_type == ftenum::FT_INT32) {

            int64_t int_value = fvalue_get_sinteger(&(current_field_info->value));
            nodejs_set_number_attribute(isolate, context, field_instance_v8, "int_value", int_value);
        } else if (field_type == ftenum::FT_INT64) {
            int64_t int_value = fvalue_get_sinteger64(&(current_field_info->value));
            Local<BigInt> int_value_v8 = BigInt::New(isolate, int_value);
            field_instance_v8->Set(context, nodejs_new_string(isolate, "int_value"), int_value_v8);
        } else if (field_type == ftenum::FT_UINT8 || field_type == ftenum::FT_UINT16 || field_type == ftenum::FT_UINT32) {
            uint64_t uint_value = fvalue_get_uinteger(&(current_field_info->value));
            nodejs_set_number_attribute(isolate, context, field_instance_v8, "uint_value", uint_value);
        } else if (field_type == ftenum::FT_UINT64) {
            uint64_t uint_value = fvalue_get_uinteger64(&(current_field_info->value));
            Local<BigInt> uint_value_v8 = BigInt::New(isolate, uint_value);
            field_instance_v8->Set(context, nodejs_new_string(isolate, "uint_value"), uint_value_v8);
        } else if (field_type == ftenum::FT_FLOAT || field_type == ftenum::FT_DOUBLE) {
            double double_value = fvalue_get_floating(&(current_field_info->value));
            nodejs_set_number_attribute(isolate, context, field_instance_v8, "double_value", double_value);
        } else if (field_type == ftenum::FT_CHAR) {
            uint64_t uint_value = fvalue_get_uinteger(&(current_field_info->value));
            nodejs_set_number_attribute(isolate, context, field_instance_v8, "uint_value", uint_value);
        } else if (field_type == ftenum::FT_BOOLEAN) {
            uint64_t uint_value = fvalue_get_uinteger64(&(current_field_info->value));
            nodejs_set_number_attribute(isolate, context, field_instance_v8, "uint_value", uint_value);
        } else if (field_type == ftenum::FT_BYTES) {
            size_t buffer_length = (size_t)current_field_info->value.value.bytes->len;
            const char* data_pointer = (const char*)current_field_info->value.value.bytes->data;

            Local<ArrayBuffer> bytes_value_v8 = ArrayBuffer::New(isolate, buffer_length);
            memcpy(bytes_value_v8->GetContents().Data(), data_pointer, buffer_length);
            field_instance_v8->Set(context, nodejs_new_string(isolate, "bytes_value"), bytes_value_v8);
        } else if (field_type == ftenum::FT_IPv4) {
            size_t buffer_length = 4;
            uint32_t int_value = fvalue_get_uinteger(&(current_field_info->value));

            string bytes_value;
            bytes_value.reserve(buffer_length);

            bytes_value.push_back((char)((int_value >> 24) & 0xFF));
            bytes_value.push_back((char)((int_value >> 16) & 0xFF));
            bytes_value.push_back((char)((int_value >> 8) & 0xFF));
            bytes_value.push_back((char)((int_value >> 0) & 0xFF));

            const char* data_pointer = bytes_value.c_str();

            Local<ArrayBuffer> bytes_value_v8 = ArrayBuffer::New(isolate, buffer_length);
            memcpy(bytes_value_v8->GetContents().Data(), data_pointer, buffer_length);
            field_instance_v8->Set(context, nodejs_new_string(isolate, "bytes_value"), bytes_value_v8);
        } else if (field_type == ftenum::FT_IPv6) {
            size_t buffer_length = 16;
            const char* data_pointer = (const char*)current_field_info->value.value.ipv6.addr.bytes;

            Local<ArrayBuffer> bytes_value_v8 = ArrayBuffer::New(isolate, buffer_length);
            memcpy(bytes_value_v8->GetContents().Data(), data_pointer, buffer_length);
            field_instance_v8->Set(context, nodejs_new_string(isolate, "bytes_value"), bytes_value_v8);
        } else if (field_type == ftenum::FT_ETHER) {
            size_t buffer_length = 6;
            const char* data_pointer = data_pointer = (const char*)current_field_info->value.value.bytes->data;

            Local<ArrayBuffer> bytes_value_v8 = ArrayBuffer::New(isolate, buffer_length);
            memcpy(bytes_value_v8->GetContents().Data(), data_pointer, buffer_length);
            field_instance_v8->Set(context, nodejs_new_string(isolate, "bytes_value"), bytes_value_v8);
        } else if (field_type == ftenum::FT_GUID) {
            uint32_t buffer_length = 16;
            e_guid_t guid = current_field_info->value.value.guid;

            string bytes_value;
            bytes_value.reserve(buffer_length);

            bytes_value.push_back((char)((guid.data1 >> 24) & 0xFF));
            bytes_value.push_back((char)((guid.data1 >> 16) & 0xFF));
            bytes_value.push_back((char)((guid.data1 >> 8) & 0xFF));
            bytes_value.push_back((char)((guid.data1 >> 0) & 0xFF));

            bytes_value.push_back((char)((guid.data2 >> 8) & 0xFF));
            bytes_value.push_back((char)((guid.data2 >> 0) & 0xFF));
            bytes_value.push_back((char)((guid.data3 >> 8) & 0xFF));
            bytes_value.push_back((char)((guid.data3 >> 0) & 0xFF));

            bytes_value.push_back((char)guid.data4[0]);
            bytes_value.push_back((char)guid.data4[1]);
            bytes_value.push_back((char)guid.data4[2]);
            bytes_value.push_back((char)guid.data4[3]);
            bytes_value.push_back((char)guid.data4[4]);
            bytes_value.push_back((char)guid.data4[5]);
            bytes_value.push_back((char)guid.data4[6]);
            bytes_value.push_back((char)guid.data4[7]);

            const char* data_pointer = bytes_value.c_str();

            Local<ArrayBuffer> bytes_value_v8 = ArrayBuffer::New(isolate, buffer_length);
            memcpy(bytes_value_v8->GetContents().Data(), data_pointer, buffer_length);
            field_instance_v8->Set(context, nodejs_new_string(isolate, "bytes_value"), bytes_value_v8);
        } else if (field_type == ftenum::FT_EUI64) {
            uint32_t buffer_length = 8;
            uint64_t uint_value = fvalue_get_uinteger64(&(current_field_info->value));

            string bytes_value;
            bytes_value.reserve(buffer_length);

            bytes_value.push_back((char)((uint_value >> 56) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 48) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 40) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 32) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 24) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 16) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 8) & 0xFFLL));
            bytes_value.push_back((char)((uint_value >> 0) & 0xFFLL));

            const char* data_pointer = bytes_value.c_str();

            Local<ArrayBuffer> bytes_value_v8 = ArrayBuffer::New(isolate, buffer_length);
            memcpy(bytes_value_v8->GetContents().Data(), data_pointer, buffer_length);
            field_instance_v8->Set(context, nodejs_new_string(isolate, "bytes_value"), bytes_value_v8);
        } else if (field_type == ftenum::FT_STRING || field_type == ftenum::FT_STRINGZ) {
            const char* data_pointer = current_field_info->value.value.string;
            nodejs_set_string_attribute(isolate, context, field_instance_v8, "string_value", data_pointer);
        } else if (field_type == ftenum::FT_PROTOCOL) {
            const char* data_pointer = current_field_info->value.value.protocol.proto_string;
            nodejs_set_string_attribute(isolate, context, field_instance_v8, "string_value", data_pointer);
        } else if (field_type == ftenum::FT_ABSOLUTE_TIME || field_type == ftenum::FT_RELATIVE_TIME) {
            double double_value = (double)(current_field_info->value.value.time.secs) + (double)(current_field_info->value.value.time.nsecs) / 1000000000.0;
            nodejs_set_number_attribute(isolate, context, field_instance_v8, "double_value", double_value);
        } else if (field_type == ftenum::FT_FRAMENUM) {
            uint64_t uint_value = fvalue_get_uinteger(&(current_field_info->value));
            nodejs_set_number_attribute(isolate, context, field_instance_v8, "uint_value", uint_value);
        } else // FieldType::NONE and potentially others
        {
            // Do nothing
        }
    }

    return scope.Escape(field_instance_v8);
}

void NodejsEventHandler::_add_buffer_to_packet_data(tvbuff_t* buffer, Local<Object>& packet_data_py) {
    int32_t buffer_length = tvb_captured_length(buffer);

    Isolate* isolate = environment_setup->isolate();
    HandleScope scope(isolate);
    Local<Context> context = isolate->GetCurrentContext();

    Local<Array> buffers_v8 = nodejs_get_attribute(isolate, context, packet_data_py, "buffers").As<Array>();

    Local<ArrayBuffer> buffer_v8 = ArrayBuffer::New(isolate, buffer_length);
    tvb_memcpy(buffer, buffer_v8->GetContents().Data(), 0, buffer_length);
    Local<Uint8Array> uint8_buffer_v8 = Uint8Array::New(buffer_v8, 0, buffer_length);
    buffers_v8->Set(context, buffers_v8->Length(), uint8_buffer_v8);
}

} // namespace shark_x

// shark_x member
namespace shark_x {
bool NodejsEventHandler::is_initialized = false;
set<NodejsLateInitRegistrationInfo*> NodejsEventHandler::late_init_registration_infos;
set<NodejsShutdownRegistrationInfo*> NodejsEventHandler::shutdown_registration_infos;
set<NodejsDissectionStartRegistrationInfo*> NodejsEventHandler::dissection_start_registration_infos;
set<PacketListenerRegistrationInfo*> NodejsEventHandler::packet_listener_registration_infos;

unique_ptr<CommonEnvironmentSetup> NodejsEventHandler::environment_setup;
unique_ptr<MultiIsolatePlatform> NodejsEventHandler::platform;

} // namespace shark_x

// init functions
namespace shark_x {

void NodejsEventHandler::Init() {
    const char* executable_directory = get_progfile_dir();
    char* executable_path = g_strdup_printf("%s%sshark_x", executable_directory, G_DIR_SEPARATOR_S);

    vector<string> args;
    args.push_back(executable_path);
    g_free(executable_path);
    vector<string> exec_args;
    vector<string> errors;

    int exit_code = node::InitializeNodeWithArgs(&args, &exec_args, &errors);
    platform = MultiIsolatePlatform::Create(16);
    V8::InitializePlatform(platform.get());
    V8::Initialize();
    environment_setup = CommonEnvironmentSetup::Create(platform.get(), &errors, args, exec_args);

    Isolate* isolate = environment_setup->isolate();

    Locker locker(isolate);
    Isolate::Scope isolate_scope(isolate);
    HandleScope scope(isolate);
    Context::Scope context_scope(environment_setup->context());

    InitModule();

    is_initialized = true;

    ExecuteFiles();
}

void NodejsEventHandler::InitModule() {
    Isolate* isolate = environment_setup->isolate();
    HandleScope scope(isolate);
    Local<Context> context = environment_setup->context();

    Local<Object> global = context->Global();

    Local<ObjectTemplate> shark_x_object_template = ObjectTemplate::New(isolate);
    Local<Object> shark_x_object = shark_x_object_template->NewInstance(context).ToLocalChecked();
    global->Set(context, nodejs_new_string(isolate, "shark_x"), shark_x_object);

    nodejs_set_number_attribute(isolate, context, shark_x_object, "SHARK_X_MAJOR_VERSION", SHARK_X_MAJOR_VERSION);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "SHARK_X_MINOR_VERSION", SHARK_X_MINOR_VERSION);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "SHARK_X_PATCH_VERSION", SHARK_X_PATCH_VERSION);

    int wireshark_major_version = 0;
    int wireshark_minor_version = 0;
    int wireshark_patch_version = 0;
    epan_get_version_number(&wireshark_major_version, &wireshark_minor_version, &wireshark_patch_version);

    nodejs_set_number_attribute(isolate, context, shark_x_object, "WIRESHARK_MAJOR_VERSION", wireshark_major_version);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "WIRESHARK_MINOR_VERSION", wireshark_minor_version);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "WIRESHARK_PATCH_VERSION", wireshark_patch_version);

    nodejs_set_string_attribute(isolate, context, shark_x_object, "PERSONAL_PLUGIN_DIRECTORY_WITH_VERSION", get_plugins_pers_dir_with_version());
    nodejs_set_string_attribute(isolate, context, shark_x_object, "PERSONAL_PLUGIN_DIRECTORY", get_plugins_pers_dir());
    nodejs_set_string_attribute(isolate, context, shark_x_object, "GLOBAL_PLUGIN_DIRECTORY_WITH_VERSION", get_plugins_dir_with_version());
    nodejs_set_string_attribute(isolate, context, shark_x_object, "GLOBAL_PLUGIN_DIRECTORY", get_plugins_dir());
    nodejs_set_string_attribute(isolate, context, shark_x_object, "EXECUTABLE_DIRECTORY", get_progfile_dir());

    nodejs_set_number_attribute(isolate, context, shark_x_object, "PHASE_REGISTRATION", (int)Phase::REGISTRATION);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "PHASE_NORMAL", (int)Phase::NORMAL_OPERATION);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "PHASE_SHUTDOWN", (int)Phase::SHUTDOWN);

    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_INT8", (int)ftenum::FT_INT8);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_INT16", (int)ftenum::FT_INT16);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_INT32", (int)ftenum::FT_INT32);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_INT64", (int)ftenum::FT_INT64);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_UINT8", (int)ftenum::FT_UINT8);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_UINT16", (int)ftenum::FT_UINT16);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_UINT32", (int)ftenum::FT_UINT32);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_UINT64", (int)ftenum::FT_UINT64);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_FLOAT", (int)ftenum::FT_FLOAT);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_DOUBLE", (int)ftenum::FT_DOUBLE);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_CHAR", (int)ftenum::FT_CHAR);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_BOOL", (int)ftenum::FT_BOOLEAN);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_BYTES", (int)ftenum::FT_BYTES);

    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_IPV4_ADDRESS", (int)ftenum::FT_IPv4);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_IPV6_ADDRESS", (int)ftenum::FT_IPv6);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_ETHERNET_ADDRESS", (int)ftenum::FT_ETHER);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_EUI64", (int)ftenum::FT_EUI64);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_STRING", (int)ftenum::FT_STRING);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_STRINGZ", (int)ftenum::FT_STRINGZ);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_ABSOLUTE_TIME", (int)ftenum::FT_ABSOLUTE_TIME);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_RELATIVE_TIME", (int)ftenum::FT_RELATIVE_TIME);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_FRAME_NUMBER", (int)ftenum::FT_FRAMENUM);
    nodejs_set_number_attribute(isolate, context, shark_x_object, "FT_NONE", (int)ftenum::FT_NONE);

    nodejs_set_function_attribute(isolate, context, shark_x_object, "report_failure", _report_failure);
    nodejs_set_function_attribute(isolate, context, shark_x_object, "report_warning", _report_warning);
    nodejs_set_function_attribute(isolate, context, shark_x_object, "register_late_init_callback", _register_late_init_callback);
    nodejs_set_function_attribute(isolate, context, shark_x_object, "deregister_late_init_callback", _deregister_late_init_callback);
    nodejs_set_function_attribute(isolate, context, shark_x_object, "register_shutdown_callback", _register_shutdown_callback);
    nodejs_set_function_attribute(isolate, context, shark_x_object, "deregister_shutdown_callback", _deregister_shutdown_callback);
    nodejs_set_function_attribute(isolate, context, shark_x_object, "register_dissection_start_callback", _register_dissection_start_callback);
    nodejs_set_function_attribute(isolate, context, shark_x_object, "deregister_dissection_start_callback", _deregister_dissection_start_callback);
    nodejs_set_function_attribute(isolate, context, shark_x_object, "register_packet_listener", _register_packet_listener);
    nodejs_set_function_attribute(isolate, context, shark_x_object, "deregister_packet_listener", _deregister_packet_listener);
    nodejs_set_function_attribute(isolate, context, shark_x_object, "get_field_descriptions", _get_field_descriptions);
}

void NodejsEventHandler::ExecuteFiles() {
    if (!is_initialized) {
        return;
    }

    Isolate* isolate = environment_setup->isolate();

    vector<string> file_paths = get_file_paths_of_plugin_directories(".js");

    for (auto&& file_path : file_paths) {
        HandleScope scope(isolate);
        Local<Context> context = environment_setup->context();
        TryCatch try_catch(isolate);

        string script_content = read_file(file_path);

        Local<String> source_v8 = nodejs_new_string_with_length(isolate, script_content.c_str(), (int)script_content.size());
        ScriptOrigin origin(nodejs_new_string(isolate, file_path.c_str()));

        Local<Script> script_v8;
        if (!Script::Compile(context, source_v8, &origin).ToLocal(&script_v8)) {
            nodejs_handle_exception(&try_catch, isolate);
            continue;
        }

        Local<Value> result_v8;
        if (!script_v8->Run(context).ToLocal(&result_v8)) {
            if (try_catch.HasCaught()) {
                nodejs_handle_exception(&try_catch, isolate);
            }
            continue;
        }
    }
}
} // namespace shark_x

// protected functions
namespace shark_x {
void NodejsEventHandler::OnLateInit() {
    for (auto&& late_init_registration_info : late_init_registration_infos) {
        Isolate* isolate = environment_setup->isolate();
        Locker locker(isolate);
        Isolate::Scope isolate_scope(isolate);
        HandleScope scope(isolate);
        Context::Scope context_scope(environment_setup->context());
        Local<Context> context = environment_setup->context();

        TryCatch try_catch(isolate);

        Local<Function> late_init_callback_v8 = late_init_registration_info->late_init_callback_v8.Get(isolate);
        if (!late_init_callback_v8->IsFunction()) {
            isolate->ThrowException(Exception::Error(nodejs_new_string(isolate, "late_init_callback is not a function")));
            if (try_catch.HasCaught()) {
                nodejs_handle_exception(&try_catch, isolate);
            }
            continue;
        }

        Local<Value> user_data_v8 = late_init_registration_info->user_data_v8.Get(isolate);

        Local<Value> result_v8;
        if (!late_init_callback_v8->Call(context, Undefined(isolate), 1, {&user_data_v8}).ToLocal(&result_v8)) {
            if (try_catch.HasCaught()) {
                nodejs_handle_exception(&try_catch, isolate);
            }
            continue;
        }
    }
}

void NodejsEventHandler::OnShutdown() {
    if (!is_initialized) {
        return;
    }

    Isolate* isolate = environment_setup->isolate();

    // clear late init registrations
    for (auto&& late_init_registration_info : late_init_registration_infos) {
        delete late_init_registration_info;
    }
    late_init_registration_infos.clear();

    // clear dissection start registrations
    for (auto&& dissection_start_registration_info : dissection_start_registration_infos) {
        delete dissection_start_registration_info;
    }
    dissection_start_registration_infos.clear();

    // clear packet listener registrations (this will trigger a packet listener
    // finish events)
    vector<PacketListenerRegistrationInfo*> temp_packet_listener_registration_infos;
    for (auto&& packet_listener_registration_info : packet_listener_registration_infos) {
        temp_packet_listener_registration_infos.push_back(packet_listener_registration_info);
    }
    for (auto&& packet_listener_registration_info : temp_packet_listener_registration_infos) {
        EventHandler::DeregisterPacketListener(packet_listener_registration_info, packet_listener_registration_infos);
    }
    temp_packet_listener_registration_infos.clear();

    // execute registered on_shutdown callbacks
    for (auto&& shutdown_registration_info : shutdown_registration_infos) {
        Locker locker(isolate);
        Isolate::Scope isolate_scope(isolate);
        HandleScope scope(isolate);
        Context::Scope context_scope(environment_setup->context());
        Local<Context> context = environment_setup->context();

        TryCatch try_catch(isolate);

        Local<Function> shutdown_callback_v8 = shutdown_registration_info->shutdown_callback_v8.Get(isolate);
        if (!shutdown_callback_v8->IsFunction()) {
            isolate->ThrowException(Exception::Error(nodejs_new_string(isolate, "shutdown_callback is not a function")));
            if (try_catch.HasCaught()) {
                nodejs_handle_exception(&try_catch, isolate);
            }
            continue;
        }

        Local<Value> user_data_v8 = shutdown_registration_info->user_data_v8.Get(isolate);

        Local<Value> result_v8;
        if (!shutdown_callback_v8->Call(context, Undefined(isolate), 1, {&user_data_v8}).ToLocal(&result_v8)) {
            if (try_catch.HasCaught()) {
                nodejs_handle_exception(&try_catch, isolate);
            }
            continue;
        }
    }

    // clear shutdown registrations
    for (auto&& shutdown_registration_info : shutdown_registration_infos) {
        delete shutdown_registration_info;
    }
    shutdown_registration_infos.clear();

    node::Stop(environment_setup->env());

    environment_setup.release();
    platform.release();

    isolate->Dispose();
    V8::Dispose();
    V8::ShutdownPlatform();

    is_initialized = false;
}

void NodejsEventHandler::OnDissectionStart() {
    for (auto&& dissection_start_registration_info : dissection_start_registration_infos) {
        Isolate* isolate = environment_setup->isolate();
        Locker locker(isolate);
        Isolate::Scope isolate_scope(isolate);
        HandleScope scope(isolate);
        Context::Scope context_scope(environment_setup->context());
        Local<Context> context = environment_setup->context();

        TryCatch try_catch(isolate);

        Local<Function> dissection_start_callback_v8 = dissection_start_registration_info->dissection_start_callback_v8.Get(isolate);
        if (!dissection_start_callback_v8->IsFunction()) {
            isolate->ThrowException(Exception::Error(nodejs_new_string(isolate, "dissection_start_callback is not a function")));
            if (try_catch.HasCaught()) {
                nodejs_handle_exception(&try_catch, isolate);
            }
            continue;
        }

        Local<Value> user_data_v8 = dissection_start_registration_info->user_data_v8.Get(isolate);

        Local<Value> result_v8;
        if (!dissection_start_callback_v8->Call(context, Undefined(isolate), 1, {&user_data_v8}).ToLocal(&result_v8)) {
            if (try_catch.HasCaught()) {
                nodejs_handle_exception(&try_catch, isolate);
            }
            continue;
        }
    }
}

tap_packet_status NodejsEventHandler::OnPacketListenerPacket(void* user_data, packet_info* packet_info, epan_dissect_t* epan_dissect, const void* _data) {
    PacketListenerRegistrationInfo* packet_listener_registration_info = (PacketListenerRegistrationInfo*)user_data;
    NodejsPacketListenerRegistrationInfo* nodejs_packet_listener_registration_info = (NodejsPacketListenerRegistrationInfo*)packet_listener_registration_info->user_data;

    Isolate* isolate = environment_setup->isolate();
    Locker locker(isolate);
    Isolate::Scope isolate_scope(isolate);
    HandleScope scope(isolate);
    Context::Scope context_scope(environment_setup->context());
    Local<Context> context = environment_setup->context();

    TryCatch try_catch(isolate);

    Local<Function> packet_listener_packet_callback_v8 = nodejs_packet_listener_registration_info->packet_listener_packet_callback_v8.Get(isolate);

    if (!packet_listener_packet_callback_v8->IsFunction()) {
        isolate->ThrowException(Exception::Error(nodejs_new_string(isolate, "packet_listener_packet_callback is not a function")));
        if (try_catch.HasCaught()) {
            nodejs_handle_exception(&try_catch, isolate);
        }
        return tap_packet_status::TAP_PACKET_DONT_REDRAW;
    }

    Local<Object> packet_data_v8 = _create_packet_data(packet_info, epan_dissect->tree, epan_dissect->tvb);

    Local<Value> user_data_v8 = nodejs_packet_listener_registration_info->user_data_v8.Get(isolate);

    Local<Value> args_v8[] = {packet_data_v8, user_data_v8};

    Local<Value> result_v8;
    if (!packet_listener_packet_callback_v8->Call(context, Undefined(isolate), 2, args_v8).ToLocal(&result_v8)) {
        if (try_catch.HasCaught()) {
            nodejs_handle_exception(&try_catch, isolate);
        }
        return tap_packet_status::TAP_PACKET_DONT_REDRAW;
    }
    return tap_packet_status::TAP_PACKET_DONT_REDRAW;
}

void NodejsEventHandler::OnPacketListenerFinish(void* user_data) {
    PacketListenerRegistrationInfo* packet_listener_registration_info = (PacketListenerRegistrationInfo*)user_data;
    NodejsPacketListenerRegistrationInfo* nodejs_packet_listener_registration_info = (NodejsPacketListenerRegistrationInfo*)packet_listener_registration_info->user_data;

    Isolate* isolate = environment_setup->isolate();
    Locker locker(isolate);
    Isolate::Scope isolate_scope(isolate);
    HandleScope scope(isolate);
    Context::Scope context_scope(environment_setup->context());
    Local<Context> context = environment_setup->context();

    TryCatch try_catch(isolate);

    Local<Value> packet_listener_finish_callback_v8 = nodejs_packet_listener_registration_info->packet_listener_finish_callback_v8.Get(isolate);
    if (!packet_listener_finish_callback_v8->IsFunction()) { // packet_listener_finish_callback is optional
        return;
    }
    Local<Value> user_data_v8 = nodejs_packet_listener_registration_info->user_data_v8.Get(isolate);

    Local<Value> result_v8;
    if (!packet_listener_finish_callback_v8.As<Function>()->Call(context, Undefined(isolate), 1, {&user_data_v8}).ToLocal(&result_v8)) {
        if (try_catch.HasCaught()) {
            nodejs_handle_exception(&try_catch, isolate);
        }
        return;
    }
    return;
}

void NodejsEventHandler::OnPacketListenerCleanup(void* user_data) {
    PacketListenerRegistrationInfo* packet_listener_registration_info = (PacketListenerRegistrationInfo*)user_data;
    NodejsPacketListenerRegistrationInfo* nodejs_packet_listener_registration_info = (NodejsPacketListenerRegistrationInfo*)packet_listener_registration_info->user_data;

    Isolate* isolate = environment_setup->isolate();
    Locker locker(isolate);
    Isolate::Scope isolate_scope(isolate);
    HandleScope scope(isolate);
    Context::Scope context_scope(environment_setup->context());
    Local<Context> context = environment_setup->context();

    TryCatch try_catch(isolate);

    Local<Value> packet_listener_cleanup_callback_v8 = nodejs_packet_listener_registration_info->packet_listener_cleanup_callback_v8.Get(isolate);
    if (!packet_listener_cleanup_callback_v8->IsFunction()) { // packet_listener_cleanup_callback is optional
        return;
    }
    Local<Value> user_data_v8 = nodejs_packet_listener_registration_info->user_data_v8.Get(isolate);

    Local<Value> result_v8;
    if (!packet_listener_cleanup_callback_v8.As<Function>()->Call(context, Undefined(isolate), 1, {&user_data_v8}).ToLocal(&result_v8)) {
        if (try_catch.HasCaught()) {
            nodejs_handle_exception(&try_catch, isolate);
        }
        return;
    }
    return;
}

} // namespace shark_x

// public functions
namespace shark_x {

void NodejsEventHandler::Register() {
    Init();

    register_final_registration_routine(OnLateInit);

    register_shutdown_routine(OnShutdown);

    register_init_routine(OnDissectionStart);
}

void NodejsEventHandler::RegisterHandoff() {}

} // namespace shark_x

#endif // SHARK_X_NODEJS_ENABLED