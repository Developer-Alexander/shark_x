/*
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR
A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SPDX-License-Identifier: MIT
*/

#ifndef __SHARK_X_NODEJS_TYPES__
#define __SHARK_X_NODEJS_TYPES__

#ifdef SHARK_X_NODEJS_ENABLED

// nodejs includes
#define BUILDING_NODE_EXTENSION
#include "node.h"
#include "uv.h"

namespace shark_x {
class NodejsLateInitRegistrationInfo {
  public:
    v8::Persistent<v8::Function> late_init_callback_v8;
    v8::Persistent<v8::Value> user_data_v8;
};

class NodejsShutdownRegistrationInfo {
  public:
    v8::Persistent<v8::Function> shutdown_callback_v8;
    v8::Persistent<v8::Value> user_data_v8;
};

class NodejsDissectionStartRegistrationInfo {
  public:
    v8::Persistent<v8::Function> dissection_start_callback_v8;
    v8::Persistent<v8::Value> user_data_v8;
};

class NodejsPacketListenerRegistrationInfo {
  public:
    v8::Persistent<v8::Function> packet_listener_packet_callback_v8;
    v8::Persistent<v8::Value> packet_listener_finish_callback_v8;
    v8::Persistent<v8::Value> packet_listener_cleanup_callback_v8;
    v8::Persistent<v8::Value> user_data_v8;
};
} // namespace shark_x

#endif // SHARK_X_NODEJS_ENABLED

#endif // __SHARK_X_NODEJS_TYPES__