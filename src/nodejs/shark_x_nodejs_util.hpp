/*
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR
A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SPDX-License-Identifier: MIT
*/

#ifndef __SHARK_X_NODEJS_UTIL__
#define __SHARK_X_NODEJS_UTIL__

#ifdef SHARK_X_NODEJS_ENABLED

// shark_x includes
#include "shark_x_util.hpp"

// wireshark includes
#include "wsutil/report_message.h"

// nodejs includes
#define BUILDING_NODE_EXTENSION
#include "node.h"

// std includes
#include <string>

namespace shark_x {

static void nodejs_handle_exception(v8::TryCatch* try_catch, v8::Isolate* isolate) {
    ws_assert(try_catch);
    ws_assert(isolate);

    v8::HandleScope handle_scope(isolate);
    v8::Local<v8::Context> context = isolate->GetCurrentContext();

    v8::Local<v8::Value> exception_v8 = try_catch->Exception();
    v8::String::Utf8Value exception_value(isolate, exception_v8);

    v8::Local<v8::Message> message_v8 = try_catch->Message();
    v8::Local<v8::Value> stack_trace_v8;

    std::string error_message = "NODEJS ERROR: ";

    if (message_v8.IsEmpty()) {
        error_message += *exception_value;
    } else {
        v8::String::Utf8Value file_name(isolate, message_v8->GetScriptOrigin().ResourceName());
        int line_number = 0;
        if (!message_v8->GetLineNumber(context).To(&line_number)) {
            line_number = -1;
        }

        string_append_format(error_message, "%s:%i: %s", *file_name, line_number, *exception_value);
    }

    if (try_catch->StackTrace(context).ToLocal(&stack_trace_v8) && stack_trace_v8->IsString() && v8::Local<v8::String>::Cast(stack_trace_v8)->Length() > 0) {
        v8::String::Utf8Value stack_trace(isolate, stack_trace_v8);

        string_append_format(error_message, "\nTraceback:\n%s", *stack_trace);
    }

    report_failure("%s", error_message.c_str());
}

#define nodejs_new_string(isolate, value) String::NewFromUtf8(isolate, value).ToLocalChecked()

#define nodejs_new_string_with_length(isolate, value, length) String::NewFromUtf8(isolate, value, NewStringType::kNormal, length).ToLocalChecked()

#define nodejs_get_attribute(isolate, context, object, attribute_name) object->Get(context, nodejs_new_string(isolate, attribute_name)).ToLocalChecked()

#define nodejs_set_attribute(isolate, context, object, attribute_name, value) object->Set(context, nodejs_new_string(isolate, attribute_name), value).ToLocalChecked()

#define nodejs_set_string_attribute(isolate, context, object, attribute_name, value)                                                                                               \
    object->Set(context, nodejs_new_string(isolate, attribute_name), nodejs_new_string(isolate, value))

#define nodejs_set_number_attribute(isolate, context, object, attribute_name, value) object->Set(context, nodejs_new_string(isolate, attribute_name), Number::New(isolate, value))

#define nodejs_set_bool_attribute(isolate, context, object, attribute_name, value)                                                                                                 \
    object->Set(context, nodejs_new_string(isolate, attribute_name), (value) ? True(isolate) : False(isolate))

#define nodejs_set_undefined_attribute(isolate, context, object, attribute_name) object->Set(context, nodejs_new_string(isolate, attribute_name), Undefined(isolate))

#define nodejs_set_function_attribute(isolate, context, object, attribute_name, value)                                                                                             \
    object->Set(context, nodejs_new_string(isolate, attribute_name), FunctionTemplate::New(isolate, value)->GetFunction(context).ToLocalChecked())

} // namespace shark_x

#endif // SHARK_X_NODEJS_ENABLED

#endif // __SHARK_X_NODEJS_UTIL__