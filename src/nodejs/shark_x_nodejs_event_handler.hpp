/*
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR
A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SPDX-License-Identifier: MIT
*/

#ifndef __SHARK_X_NODEJS_EVENT_HANDLER__
#define __SHARK_X_NODEJS_EVENT_HANDLER__

#ifdef SHARK_X_NODEJS_ENABLED

// shark_x includes
#include "shark_x.h"
#include "shark_x_types.hpp"

// std includes
#include <memory>
#include <set>

namespace node {
class CommonEnvironmentSetup;
class MultiIsolatePlatform;
} // namespace node

namespace v8 {
class HandleScope;
class TryCatch;
class Value;
class Object;
template <class T> class FunctionCallbackInfo;
template <class T> class Local;
} // namespace v8

namespace shark_x {

class NodejsLateInitRegistrationInfo;
class NodejsShutdownRegistrationInfo;
class NodejsDissectionStartRegistrationInfo;
class NodejsPacketListenerRegistrationInfo;

class NodejsEventHandler {
  protected:
    // api implementation

    static void _get_current_phase(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void _report_failure(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void _report_warning(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void _register_late_init_callback(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void _deregister_late_init_callback(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void _register_shutdown_callback(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void _deregister_shutdown_callback(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void _register_dissection_start_callback(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void _deregister_dissection_start_callback(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void _register_packet_listener(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void _deregister_packet_listener(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void _get_field_descriptions(const v8::FunctionCallbackInfo<v8::Value>& args);

    static v8::Local<v8::Object> _create_packet_data(packet_info* packet_info, proto_tree* tree, tvbuff_t* packet_buffer);
    static void _create_packet_data_process_node(proto_node* node, gpointer data);
    static v8::Local<v8::Object> _create_field_instance(header_field_info* current_header_field_info, field_info* current_field_info);
    static void _add_buffer_to_packet_data(tvbuff_t* buffer, v8::Local<v8::Object>& packet_data_v8);

    // shark_x member

    static bool is_initialized;
    static std::set<NodejsLateInitRegistrationInfo*> late_init_registration_infos;
    static std::set<NodejsShutdownRegistrationInfo*> shutdown_registration_infos;
    static std::set<NodejsDissectionStartRegistrationInfo*> dissection_start_registration_infos;
    static std::set<PacketListenerRegistrationInfo*> packet_listener_registration_infos;

    static std::unique_ptr<node::MultiIsolatePlatform> platform;
    static std::unique_ptr<node::CommonEnvironmentSetup> environment_setup;

    // init functions

    static void Init();
    static void InitModule();
    static void ExecuteFiles();

    // protected functions

    static void OnLateInit();
    static void OnShutdown();
    static void OnDissectionStart();
    static tap_packet_status OnPacketListenerPacket(void* user_data, packet_info* packet_info, epan_dissect_t* epan_dissect, const void* _data);
    static void OnPacketListenerFinish(void* user_data);
    static void OnPacketListenerCleanup(void* user_data);

  public:
    // public functions

    static void Register();
    static void RegisterHandoff();
};
} // namespace shark_x

#endif // SHARK_X_NODEJS_ENABLED

#endif // __SHARK_X_NODEJS_EVENT_HANDLER__