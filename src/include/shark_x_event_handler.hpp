/*
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR
A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SPDX-License-Identifier: MIT
*/

#ifndef __SHARK_X_EVENT_HANDLER__
#define __SHARK_X_EVENT_HANDLER__

// shark_x includes
#include "shark_x_types.hpp"

// std includes
#include <set>

namespace shark_x {
class EventHandler {
  public:
    // shark_x member
    static Phase current_phase;

    static bool CheckPacketListenerRegistrationInfo(PacketListenerRegistrationInfo* const packet_listener_registration_info,
                                                    std::set<PacketListenerRegistrationInfo*>& packet_listener_registration_infos, std::string& error_message);
    static bool RegisterPacketListener(PacketListenerRegistrationInfo* const packet_listener_registration_info,
                                       std::set<PacketListenerRegistrationInfo*>& packet_listener_registration_infos, std::string& error_message);
    static bool DeregisterPacketListener(PacketListenerRegistrationInfo* const packet_listener_registration_info,
                                         std::set<PacketListenerRegistrationInfo*>& packet_listener_registration_infos);

    static void CreatePacketData(PacketData& packet_data, std::vector<tvbuff_t*>& buffers, packet_info* packet_info, proto_tree* tree);
};
} // namespace shark_x

#endif // __SHARK_X_EVENT_HANDLER__