/*
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR
A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SPDX-License-Identifier: MIT
*/

#ifndef __SHARK_X_UTIL_EVENT__
#define __SHARK_X_UTIL_EVENT__

// wireshark includes
#include "wsutil/file_util.h"
#include "wsutil/filesystem.h"

// std includes
#include <algorithm>
#include <fstream>
#include <string>
#include <vector>

namespace shark_x {
static bool string_ends_with(const std::string& value, const std::string& ending) {
    if (ending.size() > value.size()) {
        return false;
    }
    bool result = std::equal(ending.rbegin(), ending.rend(), value.rbegin());
    return result;
}

static std::vector<std::string> get_file_paths_of_directory(const std::string& directory, const std::string& file_extension) {
    std::vector<std::string> file_paths;

    GDir* current_directory;
    const char* current_file_name;

    if ((current_directory = g_dir_open(directory.c_str(), 0, NULL)) != NULL) {
        while ((current_file_name = g_dir_read_name(current_directory)) != NULL) {
            std::string current_file_path = directory;
            current_file_path += G_DIR_SEPARATOR;
            current_file_path += current_file_name;

            if (file_extension != "") {
                if (!string_ends_with(current_file_path, file_extension)) {
                    continue;
                }
            }

            file_paths.push_back(current_file_path);
        }

        g_dir_close(current_directory);
    }

    std::sort(file_paths.begin(), file_paths.end());
    return file_paths;
}

static std::vector<std::string> get_file_paths_of_directories(const std::vector<std::string>& directories, const std::string& file_extension) {
    std::vector<std::string> file_paths;
    for (auto&& directory : directories) {
        std::vector<std::string> sub_file_paths = get_file_paths_of_directory(directory, file_extension);
        file_paths.insert(file_paths.end(), sub_file_paths.begin(), sub_file_paths.end());
    }
    return file_paths;
}

static std::vector<std::string> get_file_paths_of_plugin_directories(const std::string& file_extension) {
    std::vector<std::string> directories;
    directories.push_back(get_plugins_pers_dir_with_version());
    directories.push_back(get_plugins_pers_dir());
    directories.push_back(get_plugins_dir_with_version());
    directories.push_back(get_plugins_dir());

    std::vector<std::string> file_paths = get_file_paths_of_directories(directories, file_extension);

    return file_paths;
}

static std::string read_file(const std::string file_path) {
    std::string result;

    std::ifstream file;
    file.open(file_path);

    if (file.is_open()) {
        std::streampos file_size = file.tellg();
        result.reserve(file_size);

        std::string line;
        while (std::getline(file, line)) {
            result += line;
            result += "\n";
        }
        file.close();
    }

    return result;
}

static std::string string_format(const char* msg_format, ...) {
    va_list ap;

    va_start(ap, msg_format);
    char* formatted_string = g_strdup_vprintf(msg_format, ap);
    std::string result(formatted_string);
    g_free(formatted_string);
    va_end(ap);

    return result;
}

static void string_append_format(std::string& input, const char* msg_format, ...) {
    va_list ap;

    va_start(ap, msg_format);
    char* formatted_string = g_strdup_vprintf(msg_format, ap);
    input += formatted_string;
    g_free(formatted_string);
    va_end(ap);
}

} // namespace shark_x
#endif // __SHARK_X_UTIL_EVENT__