/*
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR
A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SPDX-License-Identifier: MIT
*/

#ifndef __SHARK_X_TYPES__
#define __SHARK_X_TYPES__

// wireshark includes
#include "epan/epan.h"
#include "epan/packet.h"
#include "epan/proto.h"
#include "epan/tap.h"
#include "epan/tvbuff.h"

// std includes
#include <map>
#include <set>
#include <string>
#include <vector>

namespace shark_x {

enum class Phase { REGISTRATION = 10, NORMAL_OPERATION = 20, SHUTDOWN = 30 };

class PacketListenerRegistrationInfo {
  public:
    std::string filter_string;
    std::string tap_name;
    tap_packet_cb packet_listener_packet_callback = NULL;
    tap_reset_cb packet_listener_finish_callback = NULL;   // wireshark's reset is more like a on finish event
    tap_finish_cb packet_listener_cleanup_callback = NULL; // wireshark's finish is more like a on shutdown or cleanup event
    bool all_fields_requested = false;
    std::set<std::string> wanted_field_names;
    void* user_data = NULL;
};

class FieldValue {
  public:
    ftenum field_type = FT_NONE;
    int64_t int_value = 0;
    uint64_t uint_value = 0;
    double double_value = 0.0;
    std::string string_value;
};

class FieldInstance {
  public:
    std::string name;
    FieldValue field_value;
    int64_t start_position = -1;
    uint64_t length = 0;
    int64_t buffer_index = -1;
    bool big_endian = true;
    bool hidden = false;
    bool generated = false;
};

class ProtocolTreeNode {
  public:
    FieldInstance field_instance;
    std::string representation;
    std::vector<ProtocolTreeNode> children;
};

class PacketInfo {
  public:
    uint32_t number = 0;
    uint32_t length = 0;
    uint64_t absolute_timestamp_seconds = 0;
    uint64_t absolute_timestamp_nanoseconds = 0;
    uint64_t relative_timestamp_seconds = 0;
    uint64_t relative_timestamp_nanoseconds = 0;
    bool visible = false;
    bool visited = false;
};

class PacketData {
  public:
    PacketInfo packet_info;
    ProtocolTreeNode protocol_tree;
    std::vector<std::string> buffers;
    int64_t packet_buffer_index = 0;
    std::string info_column;
    std::string protocol_column;
};
} // namespace shark_x

#endif // __SHARK_X_TYPES__