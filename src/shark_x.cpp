/*
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR
A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SPDX-License-Identifier: MIT
*/

// shark_x includes
#define WS_BUILD_DLL
#include "shark_x.h"
#include "shark_x_event_handler.hpp"

// wireshark includes
#include "epan/epan.h"
#include "epan/packet.h"
#include "epan/proto.h"
#include "epan/tap.h"
#include "epan/tvbuff.h"
#include "wsutil/report_message.h"

// python includes
#ifdef SHARK_X_PYTHON_ENABLED
#include "python/shark_x_python_event_handler.hpp"
#endif // SHARK_X_PYTHON_ENABLED

// nodejs includes
#ifdef SHARK_X_NODEJS_ENABLED
#include "nodejs/shark_x_nodejs_event_handler.hpp"
#endif // SHARK_X_NODEJS_ENABLED

// lusjit includes
#ifdef SHARK_X_LUAJIT_ENABLED
#include "luajit/shark_x_luajit_event_handler.hpp"
#endif // SHARK_X_LUAJIT_ENABLED

void shark_x_register(void) {

#ifdef SHARK_X_PYTHON_ENABLED
    shark_x::PythonEventHandler::Register();
#endif // SHARK_X_PYTHON_ENABLED

#ifdef SHARK_X_NODEJS_ENABLED
    shark_x::NodejsEventHandler::Register();
#endif // SHARK_X_NODEJS_ENABLED

#ifdef SHARK_X_LUAJIT_ENABLED
    shark_x::LuajitEventHandler::Register();
#endif // SHARK_X_LUAJIT_ENABLED
}

void shark_x_register_handoff(void) {
    shark_x::EventHandler::current_phase = shark_x::Phase::NORMAL_OPERATION;

#ifdef SHARK_X_PYTHON_ENABLED
    shark_x::PythonEventHandler::RegisterHandoff();
#endif // SHARK_X_PYTHON_ENABLED

#ifdef SHARK_X_NODEJS_ENABLED
    shark_x::NodejsEventHandler::RegisterHandoff();
#endif // SHARK_X_NODEJS_ENABLED

#ifdef SHARK_X_LUAJIT_ENABLED
    shark_x::LuajitEventHandler::RegisterHandoff();
#endif // SHARK_X_LUAJIT_ENABLED
}