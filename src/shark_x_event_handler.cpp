/*
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR
A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SPDX-License-Identifier: MIT
*/

// shark_x includes
#include "shark_x_event_handler.hpp"
#include "shark_x_types.hpp"

// wireshark includes
#include "epan/dfilter/dfilter.h"
#include "epan/epan.h"
#include "epan/packet.h"
#include "epan/proto.h"

using namespace std;

namespace shark_x {
Phase EventHandler::current_phase = Phase::REGISTRATION;

bool EventHandler::CheckPacketListenerRegistrationInfo(PacketListenerRegistrationInfo* const packet_listener_registration_info,
                                                       set<PacketListenerRegistrationInfo*>& packet_listener_registration_infos, std::string& error_message) {
    for (auto&& wanted_field_name : packet_listener_registration_info->wanted_field_names) {
        header_field_info* header_field_info = proto_registrar_get_byname(wanted_field_name.c_str());
        if (header_field_info == NULL) {
            error_message = "Field '" + wanted_field_name + "' does not exist.";
            return false;
        }

        dfilter_t* wanted_field_name_test_filter;
        char* wanted_field_name_test_error_message = NULL;
        gboolean compile_filter_result = dfilter_compile(wanted_field_name.c_str(), &wanted_field_name_test_filter, &wanted_field_name_test_error_message);
        if (compile_filter_result == FALSE) {
            if (wanted_field_name_test_error_message != NULL) {
                error_message = wanted_field_name_test_error_message;
                g_free(wanted_field_name_test_error_message);
            }
            return false;
        }
        dfilter_free(wanted_field_name_test_filter);
    }

    dfilter_t* filter_string_test_filter;
    char* filter_string_test_error_message = NULL;
    gboolean compile_filter_result = dfilter_compile(packet_listener_registration_info->filter_string.c_str(), &filter_string_test_filter, &filter_string_test_error_message);
    if (compile_filter_result == FALSE) {
        if (filter_string_test_error_message != NULL) {
            error_message = filter_string_test_error_message;
            g_free(filter_string_test_error_message);
        }
        return false;
    }
    dfilter_free(filter_string_test_filter);

    bool is_valid_tap_name = false;
    GList* tap_names = get_tap_names();
    while (tap_names != NULL) {
        string tap_name = (const char*)tap_names->data;
        if (packet_listener_registration_info->tap_name == tap_name) {
            is_valid_tap_name = true;
            break;
        }
        tap_names = tap_names->next;
    }

    if (!is_valid_tap_name) {
        error_message = "Tap '" + packet_listener_registration_info->tap_name + "' does not exist.";
        return false;
    }

    return true;
}

bool EventHandler::RegisterPacketListener(PacketListenerRegistrationInfo* const packet_listener_registration_info,
                                          set<PacketListenerRegistrationInfo*>& packet_listener_registration_infos, std::string& error_message) {
    if (packet_listener_registration_info == NULL) {
        error_message = "Reference to packet_listener_registration_info is not valid.";
        return false;
    }

    set<PacketListenerRegistrationInfo*>::iterator packet_listener_registration_info_found = packet_listener_registration_infos.find(packet_listener_registration_info);

    if (packet_listener_registration_info_found != packet_listener_registration_infos.end()) {
        error_message = "Reference to packet_listener_registration_info does already exist.";
        return false;
    }

    if (!CheckPacketListenerRegistrationInfo(packet_listener_registration_info, packet_listener_registration_infos, error_message)) {
        return false;
    }

    int flags = TL_REQUIRES_PROTO_TREE | TL_REQUIRES_COLUMNS;

    std::string effective_filter_string;
    if (packet_listener_registration_info->all_fields_requested) {
        effective_filter_string = packet_listener_registration_info->filter_string;
    } else if (packet_listener_registration_info->wanted_field_names.size() > 0) {
        size_t effective_filter_string_size = 8;
        for (auto&& wanted_field_name : packet_listener_registration_info->wanted_field_names) {
            effective_filter_string_size += wanted_field_name.size();
            effective_filter_string_size += 4;
        }

        effective_filter_string.reserve(effective_filter_string_size);
        int current_wanted_field_name_count = 0;

        effective_filter_string.append("(");
        for (auto&& wanted_field_name : packet_listener_registration_info->wanted_field_names) {
            effective_filter_string.append(wanted_field_name);
            if (current_wanted_field_name_count < packet_listener_registration_info->wanted_field_names.size() - 1) {
                effective_filter_string.append(" || ");
            }
            current_wanted_field_name_count++;
        }
        effective_filter_string.append(")");
        if (packet_listener_registration_info->filter_string.size() > 0) {
            effective_filter_string.append(" && (");
            effective_filter_string.append(packet_listener_registration_info->filter_string);
            effective_filter_string.append(")");
        }
    } else {
        effective_filter_string = packet_listener_registration_info->filter_string;
    }

    GString* error_string =
        register_tap_listener(packet_listener_registration_info->tap_name.c_str(), (void*)packet_listener_registration_info, effective_filter_string.c_str(), flags,
                              packet_listener_registration_info->packet_listener_finish_callback, packet_listener_registration_info->packet_listener_packet_callback, NULL,
                              packet_listener_registration_info->packet_listener_cleanup_callback);

    if (error_string != NULL) {
        error_message = error_string->str;
        g_string_free(error_string, TRUE);
        return false;
    }

    if (packet_listener_registration_info->all_fields_requested) {
        epan_set_always_visible(TRUE);
    }

    packet_listener_registration_infos.insert(packet_listener_registration_info);

    return true;
}

bool EventHandler::DeregisterPacketListener(PacketListenerRegistrationInfo* const packet_listener_registration_info,
                                            set<PacketListenerRegistrationInfo*>& packet_listener_registration_infos) {
    if (packet_listener_registration_info == NULL) {
        return false;
    }

    set<PacketListenerRegistrationInfo*>::iterator packet_listener_registration_info_found = packet_listener_registration_infos.find(packet_listener_registration_info);

    if (packet_listener_registration_info_found == packet_listener_registration_infos.end()) {
        return false;
    }

    remove_tap_listener(packet_listener_registration_info);

    if (packet_listener_registration_info->all_fields_requested) {
        epan_set_always_visible(FALSE);
    }

    if (packet_listener_registration_info->user_data != NULL) {
        delete packet_listener_registration_info->user_data;
    }
    delete packet_listener_registration_info;
    packet_listener_registration_infos.erase(packet_listener_registration_info);

    return true;
}

void _set_field_description(header_field_info* current_header_field_info, FieldInstance& field_instance, field_info* current_field_info) {
    ws_assert(current_header_field_info);

    field_instance.name = current_header_field_info->abbrev;

    if (current_field_info != NULL) {
        FieldValue& field_value = field_instance.field_value;
        ftenum& field_type = field_value.field_type;
        field_type = current_header_field_info->type;

        if (field_type == ftenum::FT_INT8 || field_type == ftenum::FT_INT16 || field_type == ftenum::FT_INT32) {
            field_value.int_value = fvalue_get_sinteger(&(current_field_info->value));
        } else if (field_type == ftenum::FT_INT64) {
            field_value.int_value = fvalue_get_sinteger64(&(current_field_info->value));
        } else if (field_type == ftenum::FT_UINT8 || field_type == ftenum::FT_UINT16 || field_type == ftenum::FT_UINT32) {
            field_value.uint_value = fvalue_get_uinteger(&(current_field_info->value));
        } else if (field_type == ftenum::FT_UINT64) {
            field_value.uint_value = fvalue_get_uinteger64(&(current_field_info->value));
        } else if (field_type == ftenum::FT_FLOAT || field_type == ftenum::FT_DOUBLE) {
            field_value.double_value = fvalue_get_floating(&(current_field_info->value));
        } else if (field_type == ftenum::FT_CHAR) {
            field_value.uint_value = fvalue_get_uinteger(&(current_field_info->value));
        } else if (field_type == ftenum::FT_BOOLEAN) {
            field_value.uint_value = fvalue_get_uinteger64(&(current_field_info->value));
        } else if (field_type == ftenum::FT_BYTES) {
            size_t buffer_length = (size_t)current_field_info->value.value.bytes->len;
            const char* data_pointer = (const char*)current_field_info->value.value.bytes->data;

            field_value.string_value = string(data_pointer, buffer_length);
        } else if (field_type == ftenum::FT_IPv4) {
            size_t buffer_length = 4;
            uint32_t int_value = fvalue_get_uinteger(&(current_field_info->value));

            field_value.string_value.reserve(buffer_length);

            field_value.string_value.push_back((char)((int_value >> 24) & 0xFF));
            field_value.string_value.push_back((char)((int_value >> 16) & 0xFF));
            field_value.string_value.push_back((char)((int_value >> 8) & 0xFF));
            field_value.string_value.push_back((char)((int_value >> 0) & 0xFF));
        } else if (field_type == ftenum::FT_IPv6) {
            size_t buffer_length = 16;
            const char* data_pointer = (const char*)current_field_info->value.value.ipv6.addr.bytes;

            field_value.string_value = string(data_pointer, buffer_length);
        } else if (field_type == ftenum::FT_ETHER) {
            size_t buffer_length = 6;
            const char* data_pointer = (const char*)current_field_info->value.value.bytes->data;

            field_value.string_value = string(data_pointer, buffer_length);
        } else if (field_type == ftenum::FT_GUID) {
            uint32_t buffer_length = 16;
            e_guid_t guid = current_field_info->value.value.guid;

            field_value.string_value.reserve(buffer_length);

            field_value.string_value.push_back((char)((guid.data1 >> 24) & 0xFF));
            field_value.string_value.push_back((char)((guid.data1 >> 16) & 0xFF));
            field_value.string_value.push_back((char)((guid.data1 >> 8) & 0xFF));
            field_value.string_value.push_back((char)((guid.data1 >> 0) & 0xFF));

            field_value.string_value.push_back((char)((guid.data2 >> 8) & 0xFF));
            field_value.string_value.push_back((char)((guid.data2 >> 0) & 0xFF));
            field_value.string_value.push_back((char)((guid.data3 >> 8) & 0xFF));
            field_value.string_value.push_back((char)((guid.data3 >> 0) & 0xFF));

            field_value.string_value.push_back((char)guid.data4[0]);
            field_value.string_value.push_back((char)guid.data4[1]);
            field_value.string_value.push_back((char)guid.data4[2]);
            field_value.string_value.push_back((char)guid.data4[3]);
            field_value.string_value.push_back((char)guid.data4[4]);
            field_value.string_value.push_back((char)guid.data4[5]);
            field_value.string_value.push_back((char)guid.data4[6]);
            field_value.string_value.push_back((char)guid.data4[7]);
        } else if (field_type == ftenum::FT_EUI64) {
            uint32_t buffer_length = 8U;
            uint64_t uint_value = fvalue_get_uinteger64(&(current_field_info->value));

            field_value.string_value.reserve(buffer_length);

            field_value.string_value.push_back((char)((uint_value >> 56) & 0xFFLL));
            field_value.string_value.push_back((char)((uint_value >> 48) & 0xFFLL));
            field_value.string_value.push_back((char)((uint_value >> 40) & 0xFFLL));
            field_value.string_value.push_back((char)((uint_value >> 32) & 0xFFLL));
            field_value.string_value.push_back((char)((uint_value >> 24) & 0xFFLL));
            field_value.string_value.push_back((char)((uint_value >> 16) & 0xFFLL));
            field_value.string_value.push_back((char)((uint_value >> 8) & 0xFFLL));
            field_value.string_value.push_back((char)((uint_value >> 0) & 0xFFLL));
        } else if (field_type == ftenum::FT_STRING || field_type == ftenum::FT_STRINGZ) {
            field_value.string_value = current_field_info->value.value.string;
            ;
        } else if (field_type == ftenum::FT_PROTOCOL) {
            field_value.string_value = current_field_info->value.value.protocol.proto_string;
        } else if (field_type == ftenum::FT_ABSOLUTE_TIME || field_type == ftenum::FT_RELATIVE_TIME) {
            double value = (double)(current_field_info->value.value.time.secs) + (double)(current_field_info->value.value.time.nsecs) / 1000000000.0;
            field_value.double_value = value;
        } else if (field_type == ftenum::FT_FRAMENUM) {
            field_value.uint_value = fvalue_get_uinteger(&(current_field_info->value));
        } else // FieldType::NONE and potentially others
        {
            field_type = ftenum::FT_NONE;
            // Do nothing
        }
    }
}

class CreatePacketDataProcessNodeData {
  public:
    vector<tvbuff_t*>* buffers = NULL;
    ProtocolTreeNode* parent_protocol_tree_node = NULL;
};

static void _create_packet_data_process_node(proto_node* node, gpointer data) {
    ws_assert(node);
    ws_assert(data);
    ws_assert(node->finfo);

    CreatePacketDataProcessNodeData* create_packet_data_process_node_data = (CreatePacketDataProcessNodeData*)data;

    header_field_info* current_header_field_info = node->finfo->hfinfo;

    ProtocolTreeNode* parent_protocol_tree_node = create_packet_data_process_node_data->parent_protocol_tree_node;
    parent_protocol_tree_node->children.push_back(ProtocolTreeNode());
    ProtocolTreeNode& protocol_tree_node = parent_protocol_tree_node->children.back();

    _set_field_description(current_header_field_info, protocol_tree_node.field_instance, node->finfo);

    tvbuff_t* current_buffer = node->finfo->ds_tvb;
    int64_t buffer_index = -1;

    if (current_buffer != NULL) {
        vector<tvbuff_t*>* buffers = create_packet_data_process_node_data->buffers;
        for (size_t i = 0; i < buffers->size(); i++) {
            if (buffers->at(i) == current_buffer) {
                buffer_index = (int64_t)i;
                break;
            }
        }
        if (buffer_index == -1) {
            buffer_index = (int64_t)buffers->size();
            buffers->push_back(current_buffer);
        }
    }

    if (current_buffer != NULL) {
        protocol_tree_node.field_instance.start_position = node->finfo->start;
        protocol_tree_node.field_instance.length = node->finfo->length;
    } else {
        protocol_tree_node.field_instance.start_position = -1;
        protocol_tree_node.field_instance.length = 0;
    }

    protocol_tree_node.field_instance.buffer_index = buffer_index;

    protocol_tree_node.field_instance.hidden = (node->finfo->flags & FI_HIDDEN) > 0 ? true : false;
    protocol_tree_node.field_instance.generated = (node->finfo->flags & FI_GENERATED) > 0 ? true : false;
    protocol_tree_node.field_instance.big_endian = (node->finfo->flags & FI_BIG_ENDIAN) > 0 ? true : false;

    if (node->finfo->rep->representation != NULL) {
        protocol_tree_node.representation = node->finfo->rep->representation;
    }

    create_packet_data_process_node_data->parent_protocol_tree_node = &protocol_tree_node;
    proto_tree_children_foreach(node, _create_packet_data_process_node, create_packet_data_process_node_data);
    create_packet_data_process_node_data->parent_protocol_tree_node = parent_protocol_tree_node;
}

static void _add_buffer_to_packet_data(tvbuff_t* buffer, PacketData& packet_data) {
    packet_data.buffers.push_back(string());
    string& current_buffer = packet_data.buffers.back();
    int32_t buffer_length = tvb_captured_length(buffer);

    current_buffer.reserve(buffer_length);

    for (int32_t i = 0; i < buffer_length; i++) {
        uint8_t current_byte = tvb_get_guint8(buffer, (gint)i);
        current_buffer.push_back(current_byte);
    }
}

void EventHandler::CreatePacketData(PacketData& packet_data, std::vector<tvbuff_t*>& buffers, packet_info* packet_info, proto_tree* tree) {
    packet_data.packet_buffer_index = 0;

    packet_data.packet_info.number = packet_info->num;
    packet_data.packet_info.absolute_timestamp_seconds = packet_info->abs_ts.secs;
    packet_data.packet_info.absolute_timestamp_nanoseconds = packet_info->abs_ts.nsecs;
    packet_data.packet_info.relative_timestamp_seconds = packet_info->rel_ts.secs;
    packet_data.packet_info.relative_timestamp_seconds = packet_info->rel_ts.nsecs;
    packet_data.packet_info.length = packet_info->fd->pkt_len;
    packet_data.packet_info.visited = packet_info->fd->visited > 0 ? true : false;
    packet_data.packet_info.visible = (tree != NULL && tree->tree_data->visible == TRUE) ? true : false;

    if (packet_info->cinfo != NULL) {
        const char* protocol_column_text = col_get_text(packet_info->cinfo, COL_PROTOCOL);
        if (protocol_column_text != NULL) {
            packet_data.protocol_column = protocol_column_text;
        }

        const char* info_column_text = col_get_text(packet_info->cinfo, COL_INFO);
        if (info_column_text != NULL) {
            packet_data.info_column = info_column_text;
        }
    }

    CreatePacketDataProcessNodeData create_packet_data_process_node_data;
    create_packet_data_process_node_data.buffers = &buffers;
    create_packet_data_process_node_data.parent_protocol_tree_node = &packet_data.protocol_tree;

    proto_tree_children_foreach(tree, _create_packet_data_process_node, &create_packet_data_process_node_data);

    for (auto&& current_buffer : buffers) {
        _add_buffer_to_packet_data(current_buffer, packet_data);
    }
}
} // namespace shark_x