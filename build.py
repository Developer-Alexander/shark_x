"""
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SPDX-License-Identifier: MIT
"""

# %%
import subprocess
import sys
import os
import shutil
import traceback
import platform
import requests
from io import BytesIO
from zipfile import ZipFile
from time import time
from datetime import timedelta
from typing import Any, Callable, List, Union

# %%
system: str = platform.system().lower()
sep: str = os.sep

# %%

wireshark_git_url: str = "https://github.com/wireshark/wireshark.git"
python_git_url: str = "https://github.com/python/cpython"
python_pip_url: str = "https://bootstrap.pypa.io/get-pip.py"
nodejs_git_url: str = "https://github.com/nodejs/node.git"
luajit_git_url: str = "https://github.com/LuaJIT/LuaJIT.git"

shark_x_major_version: int = 0
shark_x_minor_version: int = 2
shark_x_patch_version: int = 0

wireshark_major_version: int = 3
wireshark_minor_version: int = 6
wireshark_patch_version: int = 3

python_major_version: int = 3
python_minor_version: int = 10
python_patch_version: int = 4
python_version_appendix: str = ""

nodejs_major_version: int = 16
nodejs_minor_version: int = 14
nodejs_patch_version: int = 2

luajit_major_version: int = 2
luajit_minor_version: int = 1
luajit_patch_version: int = 0

builds: List[str] = ["Release"]
debug_enabled: bool = True

if debug_enabled:
    builds.append("Debug")

python_enabled: bool = True
nodejs_enabled: bool = True
luajit_enabled: bool = True

build_enabled: bool = True
get_pip: bool = True

output_prefix: str = f"""{os.getcwd()}"""
libs_directory: str = f"""{output_prefix}{sep}libs{sep}{system}"""

shark_x_src_directory: str = f"""{os.getcwd()}{sep}src"""
shark_x_build_directory: str = f"""{libs_directory}{sep}shark_x{sep}build"""
shark_x_binary_base_directory: str = f"""{output_prefix}{sep}bin{sep}{system}"""

wireshark_base_directory: str = f"""{libs_directory}{sep}wireshark"""
wireshark_build_directory: str = f"""{wireshark_base_directory}{sep}build"""
wireshark_binary_base_directory: str = f"""{wireshark_build_directory}{sep}run"""
wireshark_libs_directory: str = f"""{libs_directory}{sep}wireshark-win64-libs-{wireshark_major_version}.{wireshark_minor_version}"""

python_base_directory: str = f"""{libs_directory}{sep}python"""

nodejs_base_directory: str = f"""{libs_directory}{sep}nodejs"""
nodejs_binary_base_directory: str = f"""{nodejs_base_directory}{sep}out"""

luajit_base_directory: str = f"""{libs_directory}{sep}luajit"""
luajit_src_directory: str = f"""{luajit_base_directory}{sep}src"""

win_flex_bison_url: str = "https://github.com/lexxmark/winflexbison/releases/download/v2.5.25/win_flex_bison-2.5.25.zip"
strawberry_perl_url: str = (
    "https://strawberryperl.com/download/5.32.1.1/strawberry-perl-5.32.1.1-64bit.zip"
)

# %%


def get_msbuild_path():
    path: str = ""
    if system == "windows":
        command: str = f"""\"%ProgramFiles(x86)%\\Microsoft Visual Studio\\Installer\\vswhere.exe" -latest -requires Microsoft.Component.MSBuild -find MSBuild\\**\\Bin\\MSBuild.exe"""
        print(f"""call '{command}'""")
        raw_path: bytes = subprocess.check_output(command, shell=True)
        path: str = raw_path.decode("UTF-8")
        path = path.replace("\n", "").replace("\r", "")
        if " " in path:
            path = f"""\"{path}\""""

    return path


# %%


def copy_file(source_path: str, target_path: str):
    try:
        if os.path.exists(source_path):
            print(f"""Copy '{source_path}' to '{target_path}'""")
            with open(source_path, "r+b") as input_file:
                content: bytes = input_file.read()
                input_file.close()
                os.makedirs(os.path.dirname(target_path), exist_ok=True)
                with open(target_path, "w+b") as output_file:
                    output_file.write(content)
                    output_file.flush()
                    output_file.close()
    except Exception:
        print(f"""### Error: {traceback.format_exc()}""")
        abort: str = input(f"Continue anyway? (y/n)")
        if abort.lower() != "y":
            sys.exit()


def copy_directory(
    source_directory: str,
    target_directories: Union[List[str], str],
    allowed_file_extensions: Union[List[str], None] = None,
    recursively=True,
):
    if isinstance(target_directories, str):
        target_directories = [target_directories]

    if os.path.exists(source_directory):
        for base_directory, sub_directories, file_names in os.walk(source_directory):
            if not recursively:
                if base_directory != source_directory:
                    continue
            for file_name in file_names:
                if allowed_file_extensions is None or any(
                    [
                        file_name.endswith(extension)
                        for extension in allowed_file_extensions
                    ]
                ):
                    relative_base_directory_path = os.path.relpath(
                        base_directory, source_directory
                    )
                    source_path: str = os.path.join(base_directory, file_name)
                    for target_directory in target_directories:
                        target_path: str = os.path.join(
                            target_directory, relative_base_directory_path, file_name
                        )
                        copy_file(source_path, target_path)


def clear_directory(directory: str):
    if os.path.exists(directory):
        shutil.rmtree(directory, ignore_errors=True)


# %%


def clone_git(git_url: str, target_directory: str, version: Union[str, None]):
    start_time: float = time()
    currend_working_directory: str = os.getcwd()

    print(f"""### Get {git_url} source""")
    try:
        os.makedirs(target_directory, exist_ok=True)
        command: str = f"""git clone {git_url} {target_directory}"""
        print(f"""call '{command}'""")
        os.system(command)
        if version is not None:
            os.chdir(target_directory)
            os.system(f"""git fetch --all --tags""")
            os.system(f"""git checkout tags/{version}""")
            os.system(f"""git submodule update --init --recursive""")
    except Exception:
        print(f"""### Error: {traceback.format_exc()}""")
        abort: str = input(f"Continue anyway? (y/n)")
        if abort.lower() != "y":
            sys.exit()

    os.chdir(currend_working_directory)

    duration: float = time() - start_time
    print(f"""### Done in {timedelta(seconds=duration)} s""")


# %%


def build_python():
    start_time: float = time()
    currend_working_directory: str = os.getcwd()

    print(f"""### Build python""")
    try:
        if system == "windows":
            os.chdir(f"""{python_base_directory}{sep}PCbuild""")

            python_binary_directory: str = (
                f"""{python_base_directory}{sep}PCbuild{sep}amd64"""
            )
            clear_directory(python_binary_directory)

            command: str = f"""build.bat -c Debug -p x64"""
            print(f"""call '{command}'""")
            os.system(command)

            copy_directory(
                f"""{python_binary_directory}""",
                [
                    f"""{shark_x_binary_base_directory}{sep}Debug""",
                    f"""{wireshark_binary_base_directory}{sep}Debug""",
                ],
                [".exe", ".dll", ".pyd", ".lib", ".pdb"],
                False,
            )

            clear_directory(python_binary_directory)

            command = f"""build.bat -c Release -p x64"""
            print(f"""call '{command}'""")
            os.system(command)

            copy_directory(
                f"""{python_binary_directory}""",
                [
                    f"""{shark_x_binary_base_directory}{sep}Release""",
                    f"""{wireshark_binary_base_directory}{sep}Release""",
                ],
                [".exe", ".dll", ".pyd", ".lib", ".pdb"],
                False,
            )

            # get pip
            if get_pip:
                # between value of PYTHONHOME and && must not be a whitespace (same for PYTHONPATH)
                command = f"""SET PYTHONHOME={python_binary_directory}&&
                SET PYTHONPATH={python_base_directory};{python_base_directory}{sep}Lib;{python_base_directory}{sep}Lib{sep}site-packages&&
                {python_binary_directory}{sep}python -m ensurepip --altinstall &&
                {python_binary_directory}{sep}python -m pip install --upgrade -t {python_base_directory}{sep}Lib{sep}site-packages pip setuptools web-pdb --no-warn-script-location"""
                print(f"""call '{command}'""")
                command = command.replace("\n", "").replace("    ", "")
                os.system(command)

            copy_directory(
                f"""{python_base_directory}{sep}Lib""",
                [
                    f"""{shark_x_binary_base_directory}{sep}Debug{sep}lib{sep}python{python_major_version}.{python_minor_version}""",
                    f"""{wireshark_binary_base_directory}{sep}Debug{sep}lib{sep}python{python_major_version}.{python_minor_version}""",
                    f"""{shark_x_binary_base_directory}{sep}Release{sep}lib{sep}python{python_major_version}.{python_minor_version}""",
                    f"""{wireshark_binary_base_directory}{sep}Release{sep}lib{sep}python{python_major_version}.{python_minor_version}""",
                ],
            )

        elif system == "linux":
            os.chdir(f"""{python_base_directory}""")

            python_binary_directory: str = f"""{python_base_directory}{sep}bin"""
            clear_directory(python_binary_directory)

            command = f"""./configure --prefix={python_binary_directory} --enable-ipv6 --enable-shared --enable-optimizations --with-lto --enable-loadable-sqlite-extensions"""
            print(f"""call '{command}'""")
            os.system(command)

            command = f"""make -j{os.cpu_count()} && make install"""
            print(f"""call '{command}'""")
            os.system(command)

            # get pip
            if get_pip:
                command = f"""export PYTHONHOME={python_binary_directory} &&
                export LD_LIBRARY_PATH={python_binary_directory}{sep}lib:$LD_LIBRARY_PATH &&
                {python_binary_directory}{sep}bin{sep}python3 -m ensurepip --altinstall &&
                {python_binary_directory}{sep}bin{sep}python3 -m pip install --upgrade -t {python_binary_directory}{sep}lib{sep}python{python_major_version}.{python_minor_version}{sep}site-packages pip setuptools web-pdb --no-warn-script-location"""
                print(f"""call '{command}'""")
                os.system(command)

            copy_directory(
                f"""{python_binary_directory}{sep}bin""",
                [
                    f"""{shark_x_binary_base_directory}""",
                    f"""{wireshark_binary_base_directory}""",
                ],
            )

            copy_directory(
                f"""{python_binary_directory}{sep}lib""",
                [
                    f"""{shark_x_binary_base_directory}""",
                    f"""{wireshark_binary_base_directory}""",
                ],
                [".so", ".so.1.0"],
                recursively=False,
            )

            copy_directory(
                f"""{python_binary_directory}{sep}lib{sep}python{python_major_version}.{python_minor_version}""",
                [
                    f"""{shark_x_binary_base_directory}{sep}lib{sep}python{python_major_version}.{python_minor_version}""",
                    f"""{wireshark_binary_base_directory}{sep}lib{sep}python{python_major_version}.{python_minor_version}""",
                ],
            )

            copy_directory(
                f"""{python_binary_directory}{sep}lib{sep}pkgconfig""",
                [
                    f"""{shark_x_binary_base_directory}{sep}lib{sep}pkgconfig""",
                    f"""{wireshark_binary_base_directory}{sep}lib{sep}pkgconfig""",
                ],
            )

    except Exception:
        print(f"""### Error: {traceback.format_exc()}""")
        abort: str = input(f"Continue anyway? (y/n)")
        if abort.lower() != "y":
            sys.exit()

    os.chdir(currend_working_directory)

    duration: float = time() - start_time
    print(f"""### Done in {timedelta(seconds=duration)} s""")


# %%


def build_nodejs():
    start_time: float = time()
    currend_working_directory: str = os.getcwd()

    print(f"""### Build nodejs""")
    try:
        os.chdir(nodejs_base_directory)
        if system == "windows":
            # msbuild_path: str = get_msbuild_path()

            # os.environ["_CL_"] = """/MD"""
            os.environ["_LINK_"] = """\"winmm.lib\""""

            command = f"""vcbuild.bat debug vs2019 x64 dll openssl-no-asm no-cctest"""
            print(f"""call '{command}'""")
            os.system(command)

            copy_file(
                f"""{nodejs_binary_base_directory}{sep}Debug{sep}libnode.dll""",
                f"""{shark_x_binary_base_directory}{sep}Debug{sep}libnode.dll""",
            )

            copy_file(
                f"""{nodejs_binary_base_directory}{sep}Debug{sep}libnode.dll""",
                f"""{wireshark_binary_base_directory}{sep}Debug{sep}libnode.dll""",
            )

            copy_file(
                f"""{nodejs_binary_base_directory}{sep}Debug{sep}libnode.lib""",
                f"""{wireshark_binary_base_directory}{sep}Debug{sep}libnode.lib""",
            )

            copy_file(
                f"""{nodejs_binary_base_directory}{sep}Debug{sep}libnode.pdb""",
                f"""{wireshark_binary_base_directory}{sep}Debug{sep}libnode.pdb""",
            )

            command = f"""vcbuild.bat release vs2019 x64 dll openssl-no-asm no-cctest"""
            print(f"""call '{command}'""")
            os.system(command)

            copy_file(
                f"""{nodejs_binary_base_directory}{sep}Release{sep}libnode.dll""",
                f"""{shark_x_binary_base_directory}{sep}Release{sep}libnode.dll""",
            )

            copy_file(
                f"""{nodejs_binary_base_directory}{sep}Release{sep}libnode.dll""",
                f"""{wireshark_binary_base_directory}{sep}Release{sep}libnode.dll""",
            )

            copy_file(
                f"""{nodejs_binary_base_directory}{sep}Release{sep}libnode.lib""",
                f"""{wireshark_binary_base_directory}{sep}Release{sep}libnode.lib""",
            )

            copy_file(
                f"""{nodejs_binary_base_directory}{sep}Release{sep}libnode.pdb""",
                f"""{wireshark_binary_base_directory}{sep}Release{sep}libnode.pdb""",
            )

            # os.environ["_CL_"] = ""
            os.environ["_LINK_"] = ""

        elif system == "linux":
            command: str = f"""./configure --shared"""
            print(f"""call '{command}'""")
            os.system(command)
            command = f"""make -j{os.cpu_count()}"""
            print(f"""call '{command}'""")
            os.system(command)

            copy_file(
                f"""{nodejs_binary_base_directory}{sep}Release{sep}libnode.so.93""",
                f"""{shark_x_binary_base_directory}{sep}libnode.so.93""",
            )
            command = f"""ln -s {shark_x_binary_base_directory}{sep}libnode.so.93 {shark_x_binary_base_directory}{sep}libnode.so"""
            print(f"""call '{command}'""")
            os.system(command)

            copy_file(
                f"""{nodejs_binary_base_directory}{sep}Release{sep}libnode.so.93""",
                f"""{wireshark_binary_base_directory}{sep}libnode.so.93""",
            )
            command = f"""ln -s {wireshark_binary_base_directory}{sep}libnode.so.93 {wireshark_binary_base_directory}{sep}libnode.so"""
            print(f"""call '{command}'""")
            os.system(command)

    except Exception:
        print(f"""### Error: {traceback.format_exc()}""")
        abort: str = input(f"Continue anyway? (y/n)")
        if abort.lower() != "y":
            sys.exit()

    os.chdir(currend_working_directory)

    duration: float = time() - start_time
    print(f"""### Done in {timedelta(seconds=duration)} s""")


# %%


def build_luajit():
    start_time: float = time()
    currend_working_directory: str = os.getcwd()

    print(f"""### Build luajit""")
    try:
        os.chdir(luajit_src_directory)

        # Since luajit is designed as a dropin replacement for lua the exported functions needs to be renamed to prevent name conflicts
        for base_directory, sub_directories, file_names in os.walk(
            luajit_src_directory
        ):
            for file_name in file_names:
                file_path: str = os.path.join(base_directory, file_name)
                with open(file_path, "r+b") as input_file:
                    content: bytes = input_file.read()
                    input_file.close()

                    content = content.replace(b"lua_", b"lsx_")
                    content = content.replace(b"luaL_", b"lsxL_")
                    content = content.replace(b"luaopen_", b"lsxopen_")

                    with open(file_path, "w+b") as output_file:
                        output_file.write(content)
                        output_file.flush()
                        output_file.close()

        if system == "windows":
            command: str = f"""\"%ProgramFiles(x86)%\\Microsoft Visual Studio\\Installer\\vswhere.exe" -latest -property installationPath"""
            print(f"""call '{command}'""")
            raw_vcvars64_path: bytes = subprocess.check_output(command, shell=True)
            vcvars64_path: str = raw_vcvars64_path.decode("UTF-8")
            vcvars64_path = vcvars64_path.replace("\n", "")
            vcvars64_path = vcvars64_path.replace("\r", "")

            vcvars64_path += f"""\\VC\\Auxiliary\\Build\\vcvars64.bat"""
            if " " in vcvars64_path:
                vcvars64_path = f"""\"{vcvars64_path}\""""

            command = vcvars64_path + f"""&& msvcbuild.bat"""
            print(f"""call '{command}'""")
            os.system(command)

            copy_directory(
                luajit_src_directory,
                [
                    f"""{shark_x_binary_base_directory}{sep}Debug""",
                    f"""{wireshark_binary_base_directory}{sep}Debug""",
                    f"""{shark_x_binary_base_directory}{sep}Release""",
                    f"""{wireshark_binary_base_directory}{sep}Release""",
                ],
                [".dll", ".lib", ".pdb"],
            )

        elif system == "linux":
            command: str = f"""make clean"""
            print(f"""call '{command}'""")
            os.system(command)
            command = f"""make -j{os.cpu_count()}"""
            print(f"""call '{command}'""")
            os.system(command)

            copy_file(
                f"""{luajit_base_directory}{sep}src{sep}libluajit.so""",
                f"""{shark_x_binary_base_directory}{sep}libluajit-5.1.so.2""",
            )
            command = f"""ln -s {shark_x_binary_base_directory}{sep}libluajit-5.1.so.2 {shark_x_binary_base_directory}{sep}libluajit.so"""
            print(f"""call '{command}'""")
            os.system(command)

            copy_file(
                f"""{luajit_base_directory}{sep}src{sep}libluajit.so""",
                f"""{wireshark_binary_base_directory}{sep}libluajit-5.1.so.2""",
            )
            command = f"""ln -s {wireshark_binary_base_directory}{sep}libluajit-5.1.so.2 {wireshark_binary_base_directory}{sep}libluajit.so"""
            print(f"""call '{command}'""")
            os.system(command)

    except Exception:
        print(f"""### Error: {traceback.format_exc()}""")
        abort: str = input(f"Continue anyway? (y/n)")
        if abort.lower() != "y":
            sys.exit()

    os.chdir(currend_working_directory)

    duration: float = time() - start_time
    print(f"""### Done in {timedelta(seconds=duration)} s""")


# %%
def get_wireshark_dependencies():
    start_time: float = time()
    currend_working_directory: str = os.getcwd()

    print(f"""### Get wireshark dependencies""")
    if system == "windows":
        try:
            os.environ["PLATFORM"] = "Win64"
            os.environ["WIRESHARK_BASE_DIR"] = wireshark_base_directory
            os.environ["WIRESHARK_LIB_DIR"] = wireshark_libs_directory

            # win_flex_bison
            if not os.path.exists(f"""{wireshark_libs_directory}{sep}win_flex_bison"""):
                print(f"""### Get win flex bison""")
                with requests.get(
                    win_flex_bison_url
                ) as win_flex_bison_request_response:
                    with ZipFile(
                        BytesIO(win_flex_bison_request_response.content)
                    ) as win_flex_bison_zip_file:
                        win_flex_bison_zip_file.extractall(
                            f"""{wireshark_libs_directory}{sep}win_flex_bison"""
                        )

            os.environ["PATH"] += f""";{wireshark_libs_directory}{sep}win_flex_bison"""

            # strawberry_perl
            if not os.path.exists(
                f"""{wireshark_libs_directory}{sep}strawberry_perl"""
            ):
                print(f"""### Get strawberry perl""")
                with requests.get(
                    strawberry_perl_url
                ) as strawberry_perl_request_response:
                    with ZipFile(
                        BytesIO(strawberry_perl_request_response.content)
                    ) as strawberry_perl_zip_file:
                        strawberry_perl_zip_file.extractall(
                            f"""{wireshark_libs_directory}{sep}strawberry_perl"""
                        )

            os.environ[
                "PATH"
            ] += f""";{wireshark_libs_directory}{sep}strawberry_perl{sep}perl{sep}bin"""

        except Exception:
            print(f"""### Error: {traceback.format_exc()}""")
            abort: str = input(f"Continue anyway? (y/n)")
            if abort.lower() != "y":
                sys.exit()

    os.chdir(currend_working_directory)

    duration: float = time() - start_time
    print(f"""### Done in {timedelta(seconds=duration)} s""")


# %%


def integrate_shark_x():
    start_time: float = time()
    currend_working_directory: str = os.getcwd()

    print(f"""### Integrate shark_x""")
    try:
        copy_file(
            f"""{shark_x_src_directory}{sep}plugin{sep}shark_x_plugin.c""",
            f"""{wireshark_base_directory}{sep}plugins{sep}epan{sep}shark_x_plugin{sep}shark_x_plugin.c""",
        )
        copy_file(
            f"""{shark_x_src_directory}{sep}plugin{sep}CMakeLists.txt""",
            f"""{wireshark_base_directory}{sep}plugins{sep}epan{sep}shark_x_plugin{sep}CMakeLists.txt""",
        )

        cmake_file_content: str = ""
        cmake_file_content += f"""
            # BEGIN SHARK_X
            set(SHARK_X_MAJOR_VERSION {shark_x_major_version})
            set(SHARK_X_MINOR_VERSION {shark_x_minor_version})
            set(SHARK_X_PATCH_VERSION {shark_x_patch_version})
            set(SHARK_X_SRC_DIRECTORY {shark_x_src_directory})
            set(SHARK_X_BUILD_DIRECTORY {shark_x_build_directory})
            set(SHARK_X_BINARY_BASE_DIRECTORY {shark_x_binary_base_directory})

            set(SHARK_X_PYTHON_ENABLED {"ON" if python_enabled else "OFF"})
            set(PYTHON_MAJOR_VERSION {python_major_version})
            set(PYTHON_MINOR_VERSION {python_minor_version})
            set(PYTHON_PATCH_VERSION {python_patch_version})
            set(PYTHON_BASE_DIRECTORY {python_base_directory})

            set(SHARK_X_NODEJS_ENABLED {"ON" if nodejs_enabled else "OFF"})
            set(NODEJS_BASE_DIRECTORY {nodejs_base_directory})
            set(NODEJS_MAJOR_VERSION {nodejs_major_version})
            set(NODEJS_MINOR_VERSION {nodejs_minor_version})
            set(NODEJS_PATCH_VERSION {nodejs_patch_version})

            set(SHARK_X_LUAJIT_ENABLED {"ON" if luajit_enabled else "OFF"})
            set(LUAJIT_BASE_DIRECTORY {luajit_base_directory})
            set(LUAJIT_MAJOR_VERSION {luajit_major_version})
            set(LUAJIT_MINOR_VERSION {luajit_minor_version})
            set(LUAJIT_PATCH_VERSION {luajit_patch_version})

            set(WIRESHARK_MAJOR_VERSION {wireshark_major_version})
            set(WIRESHARK_MINOR_VERSION {wireshark_minor_version})
            set(WIRESHARK_PATCH_VERSION {wireshark_patch_version})
            set(WIRESHARK_BASE_DIRECTORY {wireshark_base_directory})
            set(WIRESHARK_BINARY_BASE_DIRECTORY {wireshark_binary_base_directory})
            """

        if system == "windows":
            cmake_file_content += f"""
                set(ENV{{PLATFORM}} "Win64")
                set(ENV{{WIRESHARK_BASE_DIR}} {wireshark_base_directory})
                set(ENV{{WIRESHARK_LIB_DIR}} {wireshark_libs_directory})
                """

            if "QT5_BASE_DIR" in os.environ and os.environ["QT5_BASE_DIR"] != "":
                cmake_file_content += f"""
                    set(BUILD_wireshark ON)
                    set(ENV{{QT5_BASE_DIR}} {os.environ["QT5_BASE_DIR"]})
                    """
            else:
                cmake_file_content += f"""set(BUILD_wireshark OFF)\n"""

            cmake_file_content = cmake_file_content.replace("\\", "/")

        cmake_file_content = cmake_file_content.replace("    ", "")
        cmake_file_content += f"""# END SHARK_X\n"""

        if os.path.exists(f"""{wireshark_base_directory}{sep}CMakeLists.backup"""):
            with open(
                f"""{wireshark_base_directory}{sep}CMakeLists.backup""",
                "r",
                encoding="UTF-8",
            ) as cmake_file:
                cmake_file_content += cmake_file.read()
                cmake_file.close()
        else:
            with open(
                f"""{wireshark_base_directory}{sep}CMakeLists.txt""",
                "r",
                encoding="UTF-8",
            ) as cmake_file:
                cmake_file_content_backup: str = cmake_file.read()
                cmake_file.close()

            with open(
                f"""{wireshark_base_directory}{sep}CMakeLists.backup""",
                "w",
                encoding="UTF-8",
            ) as cmake_file:
                cmake_file.write(cmake_file_content_backup)
                cmake_file.flush()
                cmake_file.close()

            cmake_file_content += cmake_file_content_backup

        cmake_file_content += """
            # BEGIN SHARK_X
            add_subdirectory(${SHARK_X_SRC_DIRECTORY} ${SHARK_X_BUILD_DIRECTORY}/libshark_x)
            add_subdirectory(${WIRESHARK_BASE_DIRECTORY}/plugins/epan/shark_x_plugin)
            if(BUILD_wireshark)
                add_dependencies(wireshark shark_x_plugin)
            endif()
            add_dependencies(tshark shark_x_plugin)
            add_dependencies(shark_x_plugin epan)
            add_dependencies(shark_x_plugin libshark_x)
            add_dependencies(libshark_x epan)
            # END SHARK_X
            """.replace(
            "    ", ""
        )

        with open(
            f"""{wireshark_base_directory}{sep}CMakeLists.txt""", "w", encoding="UTF-8"
        ) as cmake_file:
            cmake_file.write(cmake_file_content)
            cmake_file.flush()
            cmake_file.close()

    except Exception:
        print(f"""### Error: {traceback.format_exc()}""")
        abort: str = input(f"Continue anyway? (y/n)")
        if abort.lower() != "y":
            sys.exit()

    os.chdir(currend_working_directory)

    duration: float = time() - start_time
    print(f"""### Done in {timedelta(seconds=duration)} s""")


# %%


def generate_wireshark():
    start_time: float = time()
    currend_working_directory: str = os.getcwd()

    print(f"""### Generate wireshark""")
    try:

        os.makedirs(wireshark_build_directory, exist_ok=True)
        os.chdir(wireshark_build_directory)

        cmake_command: str = f"""cmake {wireshark_base_directory} """
        if system == "windows":
            cmake_command += """ -G "Visual Studio 16 2019" -A x64"""
            cmake_command = cmake_command.replace("\\", "/")

        print(f"""call '{cmake_command}'""")
        os.system(cmake_command)

    except Exception:
        print(f"""### Error: {traceback.format_exc()}""")
        abort: str = input(f"Continue anyway? (y/n)")
        if abort.lower() != "y":
            sys.exit()

    os.chdir(currend_working_directory)

    duration: float = time() - start_time
    print(f"""### Done in {timedelta(seconds=duration)} s""")


# %%


def build_wireshark():
    start_time: float = time()
    currend_working_directory: str = os.getcwd()

    print(f"""### Build wireshark""")
    try:
        os.chdir(wireshark_build_directory)
        if system == "windows":
            msbuild_path: str = get_msbuild_path()
            for build in builds:
                command: str = f"""{msbuild_path} /m /p:Configuration={build} /p:Platform=x64 Wireshark.sln"""
                print(f"""call '{command}'""")
                os.system(command)

        elif system == "linux":
            command: str = f"""make -j{os.cpu_count()} CXXFLAGS='-fPIC'"""
            print(f"""call '{command}'""")
            os.system(command)

    except Exception:
        print(f"""### Error: {traceback.format_exc()}""")
        abort: str = input(f"Continue anyway? (y/n)")
        if abort.lower() != "y":
            sys.exit()

    os.chdir(currend_working_directory)

    duration: float = time() - start_time
    print(f"""### Done in {timedelta(seconds=duration)} s""")


# %%


def main():
    if " " in os.getcwd():
        raise Exception(
            f"Path of current working directory must not contain spaces. ({os.getcwd()})"
        )

    start_time: float = time()

    currend_working_directory: str = os.getcwd()

    wireshark_tag: str = f"v{wireshark_major_version}.{wireshark_minor_version}.{wireshark_patch_version}"

    clone_git(wireshark_git_url, wireshark_base_directory, wireshark_tag)

    if python_enabled:
        tag: str = f"v{python_major_version}.{python_minor_version}.{python_patch_version}{python_version_appendix}"
        clone_git(python_git_url, python_base_directory, tag)
        build_python()
        pass

    if nodejs_enabled:
        tag: str = (
            f"v{nodejs_major_version}.{nodejs_minor_version}.{nodejs_patch_version}"
        )
        clone_git(nodejs_git_url, nodejs_base_directory, tag)
        build_nodejs()
        pass

    if luajit_enabled:
        clone_git(luajit_git_url, luajit_base_directory, None)
        build_luajit()
        pass

    get_wireshark_dependencies()

    integrate_shark_x()

    generate_wireshark()

    if build_enabled:
        build_wireshark()
        pass

    os.chdir(currend_working_directory)

    duration: float = time() - start_time
    print(f"""### Done in {timedelta(seconds=duration)} s""")


# %%
if __name__ == "__main__":
    main()
