"""
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SPDX-License-Identifier: MIT
"""

import shark_x
import pprint
from typing import Any

shark_x.report_failure("Hello from shark_x python script")

shark_x.report_failure(
    f"shark_x version: {shark_x.SHARK_X_MAJOR_VERSION}.{shark_x.SHARK_X_MINOR_VERSION}.{shark_x.SHARK_X_PATCH_VERSION}"
)


def on_packet_listener_packet(packet_data: Any, user_data: Any) -> None:
    shark_x.report_failure("Hello from shark_x python packet listener packet")
    shark_x.report_failure(pprint.pformat(packet_data) + "\n")


def on_packet_listener_finish(user_data: Any) -> None:
    shark_x.report_failure("Hello from shark_x python packet listener finish")


def on_packet_listener_cleanup(user_data: Any) -> None:
    shark_x.report_failure("Hello from shark_x python packet listener cleanup")


def on_late_init(user_data: Any) -> None:
    shark_x.report_failure("Hello from shark_x python late init")
    packet_listener_id: str = shark_x.register_packet_listener(
        "frame",
        "udp",
        ["udp.port", "data"],
        False,
        on_packet_listener_packet,
        on_packet_listener_finish,
        on_packet_listener_cleanup,
        user_data,
    )


def on_dissection_start(user_data: Any) -> None:
    shark_x.report_failure("Hello from shark_x python dissection start")
    # for field_description in shark_x.get_field_descriptions():
    #    shark_x.report_failure(pprint.pformat(field_description))


def on_shutdown(user_data: Any) -> None:
    shark_x.report_failure("Hello from shark_x python shutdown")


user_data = object()

late_init_callback_id: str = shark_x.register_late_init_callback(
    on_late_init, user_data
)
dissection_start_callback_id: str = shark_x.register_dissection_start_callback(
    on_dissection_start, user_data
)
shutdown_callback_id: str = shark_x.register_shutdown_callback(on_shutdown, user_data)
