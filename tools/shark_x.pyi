
from typing import Any, Callable, List


class shark_x_base_type:
    pass


def is_registration_phase() -> bool: ...


def report_failure(message: str) -> None: ...


def report_warning(message: str) -> None: ...


def register_late_init_callback(
    late_init_callback: Callable[[Any], None], user_data: Any) -> str: ...


def deregister_late_init_callback(late_init_callback_id: str) -> bool: ...


def register_shutdown_callback(
    shutdown_callback: Callable[[Any], None], user_data: Any) -> str: ...


def deregister_shutdown_callback(shutdown_callback_id: str) -> bool: ...


def register_dissection_start_callback(
    dissection_start_callback: Callable[[Any], None], user_data: Any) -> str: ...


def deregister_dissection_start_callback(
    dissection_start_callback_id: str) -> bool: ...


def register_packet_listener(tap_name: str,
                             filter_string: str,
                             wanted_field_names: List[str],
                             all_fields_requested: bool,
                             packet_listener_packet_callback: Callable[[shark_x_base_type, Any], None],
                             packet_listener_finish_callback: Callable[[Any], None],
                             packet_listener_cleanup_callback: Callable[[Any], None],
                             user_data: Any) -> str: ...


def deregister_packet_listener(
    packet_listener_id: str) -> bool: ...


def get_field_descriptions() -> List[shark_x_base_type]: ...
