sudo add-apt-repository universe
sudo apt-get update
sudo apt-get install build-essential git cmake python3 gdb python3-pip
sudo apt-get install flex bison perl libglib2.0-dev libc-ares-dev libpcap-dev qttools5-dev qttools5-dev-tools libqt5svg5-dev qtmultimedia5-dev libgcrypt20-dev liblua5.2-dev libnghttp2-dev libpcre2-dev
sudo apt-get install libssl-dev zlib1g-dev libncurses5-dev libncursesw5-dev libreadline-dev libsqlite3-dev libgdbm-dev libdb5.3-dev libbz2-dev libexpat1-dev liblzma-dev libffi-dev uuid-dev
