# General

The API is designed in a simple C-like style, so it can be implemented in different environments. The documentation for the API is generic so that it can be easily applied to the specific environment. In general, the API tries to use basic data types wherever possible.

The following table lists the equivalent data types for the specific environment:

| C / C++                         | python | nodejs     | luajit                      |
| ------------------------------- | ------ | ---------- | --------------------------- |
| NULL or void                    | None   | undefined  | nil                         |
| string, char[]                  | str    | string     | string                      |
| int8                            | int    | number     | number                      |
| int16                           | int    | number     | number                      |
| int32                           | int    | number     | number                      |
| int64                           | int    | BigInt     | string (is going to change) |
| uint8                           | int    | number     | number                      |
| uint16                          | int    | number     | number                      |
| uint32                          | int    | number     | number                      |
| uint64                          | int    | BigInt     | string (is going to change) |
| float                           | float  | number     | number                      |
| double                          | float  | number     | number                      |
| byte array (uint8_t[] / string) | bytes  | Uint8Array | string (is going to change) |
| bool                            | bool   | bool       | bool                        |
| array, vector                   | list   | array      | array like table            |

# Constants

### `SHARK_X_MAJOR_VERSION`

An integer constant for the major version of shark_x.

### `SHARK_X_MINOR_VERSION`

An integer constant for the minor version of shark_x.

### `SHARK_X_PATCH_VERSION`

An integer constant for the patch version of shark_x.

### `WIRESHARK_MAJOR_VERSION`

An integer constant for the major version of wireshark.

### `WIRESHARK_MINOR_VERSION`

An integer constant for the minor version of wireshark.

### `WIRESHARK_PATCH_VERSION`

An integer constant for the patch version of wireshark.

### `PERSONAL_PLUGIN_DIRECTORY_WITH_VERSION`

A string constant with the path to the folder where wireshark is looking for version specific and user specific plugins/scripts.

### `PERSONAL_PLUGIN_DIRECTORY`

A string constant with the path to the folder where wireshark is looking for user specific plugins/scripts. `PERSONAL_PLUGIN_DIRECTORY` is not version specific.

### `GLOBAL_PLUGIN_DIRECTORY_WITH_VERSION`

A string constant with the path to the folder where wireshark is looking for version specific plugins/scripts. `GLOBAL_PLUGIN_DIRECTORY_WITH_VERSION` is not user specific.

### `GLOBAL_PLUGIN_DIRECTORY`

A string constant with the path to the folder where wireshark is looking for plugins/scripts. `GLOBAL_PLUGIN_DIRECTORY` is not version specific and user specific.

### `EXECUTABLE_DIRECTORY`

A string constant with the path to the folder where the wireshark executable is located. This can be different to the current working directory.

### `PHASE_REGISTRATION`

An integer constant representing registration phase.

Note: Current phase can be obtained via `get_current_phase()`. Please see description of `get_current_phase()` for an explanation of the phase model and its different phases.

### `PHASE_NORMAL_OPERATION`

An integer constant representing normal operation phase.

Note: Current phase can be obtained via `get_current_phase()`. Please see description of `get_current_phase()` for an explanation of the phase model and its different phases.

### `PHASE_SHUTDOWN`

An integer constant representing shutdown phase.

Note: Current phase can be obtained via `get_current_phase()`. Please see description of `get_current_phase()` for an explanation of the phase model and its different phases.

### `FT_INT8`

An integer constant for the field type `FT_INT8` of wireshark. A `FT_INT8` field is an 8 bit signed integer.

The value of a `FT_INT8` field instance is stored in `int_value`.

### `FT_INT16`

An integer constant for the field type `FT_INT16` of wireshark. A `FT_INT16` field is an 16 bit signed integer.

The value of a `FT_INT16` field instance is stored in `int_value`.

### `FT_INT32`

An integer constant for the field type `FT_INT32` of wireshark. A `FT_INT32` field is an 32 bit signed integer.

The value of a `FT_INT32` field instance is stored in `int_value`.

### `FT_INT64`

An integer constant for the field type `FT_INT64` of wireshark. A `FT_INT64` field is an 64 bit signed integer.

The value of a `FT_INT64` field instance is stored in `int_value`.

### `FT_UINT8`

An integer constant for the field type `FT_UINT8` of wireshark. A `FT_UINT8` field is an 8 bit unsigned integer.

The value of a `FT_UINT8` field instance is stored in `uint_value`.

### `FT_UINT16`

An integer constant for the field type `FT_UINT16` of wireshark. A `FT_UINT16` field is an 16 bit unsigned integer.

The value of a `FT_UINT816` field instance is stored in `uint_value`.

### `FT_UINT32`

An integer constant for the field type `FT_UINT32` of wireshark. A `FT_UINT32` field is an 32 bit unsigned integer.

The value of a `FT_UINT32` field instance is stored in `uint_value`.

### `FT_UINT64`

An integer constant for the field type `FT_UINT64` of wireshark. A `FT_UINT64` field is an 64 bit unsigned integer.

The value of a `FT_UINT64` field instance is stored in `uint_value`.

### `FT_FLOAT`

An integer constant for the field type `FT_FLOAT` of wireshark. A `FT_FLOAT` field is an 32 bit float.

The value of a `FT_FLOAT` field instance is stored in `double_value`.

### `FT_DOUBLE`

An integer constant for the field type `FT_DOUBLE` of wireshark. A `FT_DOUBLE` field is an 64 bit double.

The value of a `FT_DOUBLE` field instance is stored in `double_value`.

### `FT_CHAR`

An integer constant for the field type `FT_CHAR` of wireshark. A `FT_CHAR` field is an 8 bit unsigned integer that represents a single character.

The value of a `FT_CHAR` field instance is stored in `uint_value`.

### `FT_BOOL`

An integer constant for the field type `FT_BOOL` of wireshark. A `FT_BOOL` field is an 8 bit unsigned integer that represents a boolean value.

The value of a `FT_BOOL` field instance is stored in `uint_value`.

### `FT_BYTES`

An integer constant for the field type `FT_BYTES` of wireshark. A `FT_BYTES` field is an array of arbitrary length of 8 bit unsigned integers.

The value of a `FT_BYTES` field instance is stored in `bytes_value`.

### `FT_IPV4_ADDRESS`

An integer constant for the field type `FT_IPV4_ADDRESS` of wireshark. A `FT_IPV4_ADDRESS` field is an array of four 8 bit unsigned integers.

The value of a `FT_IPV4_ADDRESS` field instance is stored in `bytes_value`.

### `FT_IPV6_ADDRESS`

An integer constant for the field type `FT_IPV6_ADDRESS` of wireshark. A `FT_IPV6_ADDRESS` field is an array of sixteen 8 bit unsigned integers.

The value of a `FT_IPV6_ADDRESS` field instance is stored in `bytes_value`.

### `FT_ETHERNET_ADDRESS`

An integer constant for the field type `FT_ETHERNET_ADDRESS` of wireshark. A `FT_ETHERNET_ADDRESS` field is an array of six 8 bit unsigned integers.

The value of a `FT_ETHERNET_ADDRESS` field instance is stored in `bytes_value`.

### `FT_GUID`

An integer constant for the field type `FT_GUID` of wireshark. A `FT_GUID` field is an array of sixteen 8 bit unsigned integers.

The value of a `FT_GUID` field instance is stored in `bytes_value`.

### `FT_EUI64`

An integer constant for the field type `FT_EUI64` of wireshark. A `FT_EUI64` field is an array of eight 8 bit unsigned integers.

The value of a `FT_EUI64` field instance is stored in `bytes_value`.

### `FT_STRING`

An integer constant for the field type `FT_STRING` of wireshark. A `FT_STRING` field is an UTF-8 encoded string.

The value of a `FT_STRING` field instance is stored in `string_value`.

### `FT_STRINGZ`

An integer constant for the field type `FT_STRINGZ` of wireshark. A `FT_STRINGZ` field is an UTF-8 encoded string with an additional zero-termination byte at the end. The additional zero-termination byte is not covered by the actual string value.

The value of a `FT_STRINGZ` field instance is stored in `string_value`.

### `FT_PROTOCOL`

An integer constant for the field type `FT_PROTOCOL` of wireshark. A `FT_PROTOCOL` field can have a textual representation but does not have a specific data type.

The value of a `FT_PROTOCOL` field instance is stored in `string_value`.

### `FT_ABSOLUTE_TIME`

An integer constant for the field type `FT_ABSOLUTE_TIME` of wireshark. A `FT_ABSOLUTE_TIME` field is an 64 bit double that represents an UTC timestamp in seconds since 01/01/1970.

The value of a `FT_ABSOLUTE_TIME` field instance is stored in `double_value`.

### `FT_RELATIVE_TIME`

An integer constant for the field type `FT_RELATIVE_TIME` of wireshark. A `FT_RELATIVE_TIME` field is an 64 bit double that represents an amount of seconds.

The value of a `FT_RELATIVE_TIME` field instance is stored in `double_value`.

### `FT_FRAME_NUMBER`

An integer constant for the field type `FT_FRAME_NUMBER` of wireshark. A `FT_FRAME_NUMBER` field is an 64 bit unsinged integer that represents the packet number of another packet. Can be used for references to related packets.

The value of a `FT_FRAME_NUMBER` field instance is stored in `uint_value`.

### `FT_NONE`

An integer constant for the field type `FT_NONE` of wireshark. A `FT_NONE` field can have a textual representation but does not have a specific data type. Can be used for raw text nodes in the dissection tree.

Wireshark does use other field types that are not supported by shark_x (yet). These field types are mapped to `FT_NONE`.

# Functions

### `get_current_phase()`

wireshark's lifetime is divided into three phases. The first phase is the `PHASE_REGISTRATION` phase. In this phase, elements such as dissectors, fields or preferences are registered. Accordingly, not all resources are available at this time. For this reason some functions of shark_x are not executable during this phase.

The second phase `PHASE_NORMAL_OPERATION` begins as soon as the `PHASE_REGISTRATION` phase is over. During this phase, all Wireshark resources such as dissectors, fields or preferences are available and the filter engine is initialized. Event callbacks won't be called be this phase is reached.

The final phase is the `PHASE_SHUTDOWN` phase. The `PHASE_SHUTDOWN` phase starts when wireshark is closed and accordingly all used resources are cleaned up. During this phase, parts of the resources used may no longer be available, which means that some shark_x functions can no longer be executed reliably.

**Return value:** (`integer`) return the current phase. See `PHASE_*` constants for possible values.

### `report_failure( message )`

Prints a message to the user. Wireshark will show it as a message box with a failure sounde while tshark will print it to the console.

Note: Wireshark might limit the number of messages that are shown to the user per second.

**Return value:** (`void`)

**Parameter:**

- `message`: (`string`) The message that shall be shown to the user

### `report_warning( message )`

Prints a message to the user. Wireshark will show it as a message box with a failure sounde while tshark will print it to the console.

Note: Wireshark might limit the number of messages that are shown to the user per second.

**Return value:** (`void`)

**Parameter:**

- `message`: (`string`) The message that shall be shown to the user

### `register_late_init_callback( late_init_callback, user_data )`

Registers a function callback that will be called when `late init` event occurs. The `late init` event occurs once during wireshark's lifetime when `PHASE_REGISTRATION` is left and `PHASE_NORMAL_OPERATION` is entered.

`late init` event is a good time to register listeners.

**Return value:** (`string`) An id of the late init callback that can be used to deregister it via `deregister_late_init_callback()`.

**Parameter:**

- `late_init_callback`: (`function`) The callback function that will be called when `late init` event occurs. It must have the following signature `late_init_callback(userdata)` while `user_data` is the `user_data` that is given at registration.

- `user_data`: (`any`) user_data that shall be given by the callback as argument. It can be of any type.

### `deregister_late_init_callback( late_init_callback_id )`

Removes a previously registered dissection start callback.

**Return value:** (`bool`) `true` if deregistration was successful otherwise `false`

**Parameter:**

- `late_init_callback_id`: (`string`) The id of the late init callback that was returned by `register_late_init_callback()`

### `register_shutdown_callback( shutdown_callback, user_data )`

Registers a function callback that will be called when `shutdown` event occurs. The `shutdown` event occurs once during wireshark's lifetime right before wireshark is terminated. When this event occurs the `PHASE_SHUTDOWN` is entered.

`shutdown` event is a good time to free allocated resources.

**Return value:** (`string`) An id of the shutdown callback that can be used to deregister it via `deregister_shutdown_callback()`.

**Parameter:**

- `shutdown_callback`: (`function`) The callback function that will be called when `shutdown` event occurs. It must have the following signature `shutdown_callback(userdata)` while `user_data` is the `user_data` that is given at registration.

- `user_data`: (`any`) user_data that shall be given by the callback as argument. It can be of any type.

### `deregister_shutdown_callback( shutdown_callback_id )`

Removes a previously registered shutdown callback.

**Return value:** (`bool`) `true` if deregistration was successful otherwise `false`

**Parameter:**

- `shutdown_callback_id`: (`string`) The id of the shutdown callback that was returned by `register_shutdown_callback()`

### `register_dissection_start_callback( dissection_start_callback, user_data )`

Registers a function callback that will be called when `dissection start` event occurs. The `dissection start` event occurs when wireshark starts a full (re)dissection run. This can happen multiple times during wireshark's lifetime.

`dissection start` event is a good time initialize or reset resources that are used per capture file.

**Return value:** (`string`) An id of the dissection start callback that can be used to deregister it via `deregister_dissection_start_callback()`.

**Parameter:**

- `dissection_start_callback`: (`function`) The callback function that will be called when `dissection start` event occurs. It must have the following signature `dissection_start_callback(userdata)` while `user_data` is the `user_data` that is given at registration.

- `user_data`: (`any`) user_data that shall be given by the callback as argument. It can be of any type.

### `deregister_dissection_start_callback( dissection_start_callback_id )`

Removes a previously registered dissection start callback.

**Return value:** (`bool`) `true` if deregistration was successful otherwise `false`

**Parameter:**

- `dissection_start_callback_id`: (`string`) The id of the dissection start callback that was returned by `register_dissection_start_callback()`

### `register_packet_listener( tap_name, filter_string, wanted_field_names, all_fields_requested, packet_listener_packet_callback, packet_listener_finish_callback, packet_listener_cleanup_callback, user_data )`

Registers a packet listener that has 3 callbacks. The packet callback will be called for every packet that matched to the given filter string. The finish callback will be called after wireshark has gone through all packets. The cleanup callback will be called when the packet listener becomes deregistered. (including deregistration at shutdown)

Packet listeners a powerful utility for analysis scripts (post dissection analysis).

Packet listeners can have a huge impact on the performance and can slow down the dissection process significantly. As a rule of thumb: Only request the fields you really need.

Note: For more information check out wireshark's documentation about taps and listeners.

**Return value:** (`string`) An id of the packet listener that can be used to deregister it via `deregister_packet_listener()`.

**Parameter:**

- `tap_name`: (`string`) A valid name of a tap. (e.g. `frame`)

- `filter_string`: (`string`) A valid filter string. The packet callback will only be called for packets that matches to this filter. (e.g. `udp` if all packets that do not have a udp layer shall be skiped.)

- `wanted_field_names`: (`array(string)`) An array of valid names of wireshark fields. These fields are included in the packet data of the packet callbacks if they occur in the respective packet. However, the packet data can contain additional fields. (e.g. fields requested by other packet listeners.)

- `all_fields_requested`: (`bool`) If `true` packet data of the packet callbacks will contain all fields of that packet. But beaware: This slows down the dissection process significantly. As a rule of thumb: Only request the fields you really need.

- `packet_listener_packet_callback`: (`function`) The callback function that will be called after dissection of every packet that did match the given filter string.It must have the following signature `packet_listener_finish_callback(packet_data, userdata)` while `user_data` is the `user_data` that is given at registration. `packet_data` is of type `PacketData`.

- `packet_listener_finish_callback`: (`function`) The callback function that will be called when has gone through all packets. It must have the following signature `packet_listener_finish_callback(userdata)` while `user_data` is the `user_data` that is given at registration. The `finish` event is a good time to prepare used resources for the next dissection run.

- `packet_listener_cleanup_callback`: (`function`) The callback function that will be called when the packet listener becomes deregistered. It must have the following signature `packet_listener_finish_callback(userdata)` while `user_data` is the `user_data` that is given at registration. The `cleanup` event is a good time free used resources.

- `user_data`: (`any`) user_data that shall be given by the callback as argument. It can be of any type.

### `deregister_packet_listener( packet_listener_id )`

Removes a previously registered packet listener with its callbacks.

**Return value:** (`bool`) `true` if deregistration was successful otherwise `false`

**Parameter:**

- `packet_listener_id`: (`string`) The id of the packet listener that was returned by `register_packet_listener()`

# Types

### `PacketData`

- `packet_buffer_index`: (`integer`) The index of the packet buffer within the `buffers` atribute

- `packet_info`: (`PacketInfo`) Useful meta information about the packet

- `protocol_tree`: (`ProtocolTreeNode`) The root of the dissection tree.

- `buffers`: (`array(byte arrays)`) The buffers that belong to the packet. This is typically the packet buffer itself. But there might be more buffers that were created by some dissectors. (e.g. reassembled or desegmented data of a transport protocol)

### `PacketInfo`

- `number`: (`integer`) The number of the packet. Packet numbers are starting at 1.

- `absolute_timestamp_seconds`: (`integer`) The seconds part of the UTC based timestamp of the packet,

- `absolute_timestamp_nanoseconds`: (`integer`) The nanoseconds part of the UTC based timestamp of the packet.

- `relative_timestamp_seconds`: (`integer`) The seconds part of the time since the first packet (#1).

- `relative_timestamp_nanoseconds`: (`integer`) The nanoseconds part of the time since the first packet (#1).

- `length`: (`integer`) The number bytes that belong to the packet.

- `visited`: (`bool`) Wireshark implements a 2 pass dissection. That means that every packet is visited twice during its dissection. At the first pass `visited` is `false`. At the second pass it is `true`.

- `visible`: (`bool`) Packet can be hidden so that they are not displayed to the user. `visible` indicates if they are visible to the user.

- `protocol_column`: (`string`) The content of the protocol column.

- `info_column`: (`string`) The content of the info column that is typically something like a packet summary.

### `ProtocolTreeNode`

- `children`: (`array(ProtocolTreeNode)`) The child nodes of this node.

- `field_instance`: (`FieldInstance`) The instance of a field. For example wireshark does provide a field `ipv6.hlim` that describes this field in general but it does not have an actual value. An instance of this field does additionally have a actual value like `255`. Fields of the same type can occur multiple times in the same packet.

- `representation`: (`string`) A node within the dissection tree can have a special textual representation instead of field value or in addition to a field value. (e.g. field with resolved enumeration values)

### `FieldInstance`

- `name`: (`string`) The name of the field that can be used in filter expressions. (e.g. "vlan")

- `display_name`: (`string`) The name of the field that is shown to the user. (e.d. "802.1Q Virtual LAN")

- `field_id`: (`integer`) The internal id that uses wireshark for this field type.

- `field_type`: (`integer`) The type of this field. If not set the default is `FT_NONE`. See `FT_*` constants for possible values. If the field is of a type that is not supported by shark_x the `field_type` will be set to `FT_NONE`.

- `int_value`: (`integer`) The actual value of this field instance but only if `field_type` is indicating an field type with an integer value. Otherwise it might have a default value or may not be present. See `FT_*` constants for which field types does have an integer value.

- `uint_value`: (`unsigned integer`) The actual value of this field instance but only if `field_type` is indicating an field type with an insigned integer value. Otherwise it might have a default value or may not be present. See `FT_*` constants for which field types does have an unsinged integer value.

- `double_value`: (`double`) The actual value of this field instance but only if `field_type` is indicating an field type with an double value. Otherwise it might have a default value or may not be present. See `FT_*` constants for which field types does have an double value.

- `string_value`: (`string`) The actual value of this field instance but only if `field_type` is indicating an field type with an string value. Otherwise it might have a default value or may not be present. See `FT_*` constants for which field types does have an string value.

- `bytes_value`: (`byte array`) The actual value of this field instance but only if `field_type` is indicating an field type with an bytes array value. Otherwise it might have a default value or may not be present. See `FT_*` constants for which field types does have an byte array value.

- `start_position`: (`integer`) The index where this field instance is located in the referenced buffer (`buffer_index`). `-1` is indicating that this field instance is not referencing a buffer.

- `length`: (`integer`) The number of bytes this field instance is covering within the referenced buffer (`buffer_index`) beginning at index `start_position`. `0` is indicating that this field instance is not referencing a buffer.

- `buffer_index`: (`integer`) The index of the buffer within the `buffers` array of the `packet_data` that this field instances is referencing.

- `hidden`: (`bool`) `false` if the field is not visible to the user otherwise `true`. Note: hidden fields can be made visible explicitly by the user and hidden fields an still be filtered for.

- `generated`: (`bool`) Fields may be marked as generated to indicate that they are not parsed directly from the packet data but somehow generated.

- `big_endian`: (`bool`) `true` if the field is encoded as big endian (most significant byte first) otherwise `false`.
